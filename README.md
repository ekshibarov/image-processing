# Image Processing Server
Limited version of Icons8 Tools used as a dedicated server for processing images. Have a test environment and installation scripts.
Server contains image tools, like an imagemagick, cairosvg, etc.
Server use RabbitMQ, nodejs, PHP, Python for receive tasks and process images.

## Installation for vagrant
1. git clone
2. cd to repository folder
3. git submodule init
4. git submodule update
5. cp Vagrantfile.dist Vagrantfile
6. vagrant up
7. vagrant ssh
8. cd /vagrant/install
9. Run bash scripts step by step, exclude tools folder. Tools scripts runs automatically. Use --prod or --<something> for deploy on different environments. --prod is for production server, another is vagrant

## Run tests
1. cd to tests folder
2. vendor/bin/behat
	

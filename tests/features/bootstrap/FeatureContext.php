<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;

/**
 * Defines application features from the feature context.
 */
class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given :version, when I run `aptitude --version`
     */
    public function whenIRunAptitudeVersion($version)
    {
        $execResult = shell_exec("aptitude --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `php --version`
     */
    public function whenIRunPhpVersion($version)
    {
        $execResult = shell_exec("php --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `pip --version`
     */
    public function whenIRunPipVersion($version)
    {
        $execResult = shell_exec("pip --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `git --version`
     */
    public function whenIRunGitVersion($version)
    {
        $execResult = shell_exec("git --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `gcc --version`
     */
    public function whenIRunGccVersion($version)
    {
        $execResult = shell_exec("gcc --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `make --version`
     */
    public function whenIRunMakeVersion($version)
    {
        $execResult = shell_exec("make --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `composer.phar --version`
     */
    public function whenIRunComposerPharVersion($version)
    {
        $execResult = shell_exec("composer.phar --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `node --version`
     */
    public function whenIRunNodeVersion($version)
    {
        $execResult = shell_exec("node --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `npm --version`
     */
    public function whenIRunNpmVersion($version)
    {
        $execResult = shell_exec("npm --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `sudo pm2 --version`
     */
    public function whenIRunSudoPmVersion($version)
    {
        $execResult = shell_exec("sudo pm2 --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `rabbitmqadmin --version`
     */
    public function whenIRunRabbitmqadminVersion($version)
    {
        $execResult = shell_exec("rabbitmqadmin --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `inkscape --version`
     */
    public function whenIRunInkscapeVersion($version)
    {
        $execResult = shell_exec("inkscape --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :version, when I run `ps2eps --version`
     */
    public function whenIRunPsepsVersion($version)
    {
        $execResult = shell_exec("ps2eps --version");
        $isContain = (mb_strpos($execResult, $version) > 0);
        PHPUnit_Framework_Assert::assertTrue($isContain);
    }

    /**
     * @Given :version, when I run `cairosvg --version`
     */
    public function whenIRunCairosvgVersion($version)
    {
        $execResult = shell_exec("cairosvg --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :path, when I run `which svg-path-bounding-box`
     */
    public function whenIRunWhichSvgPathBoundingBox($path)
    {
        $execResult = shell_exec("which svg-path-bounding-box");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :path, when I run `which svgo`
     */
    public function whenIRunWhichSvgo($path)
    {
        $execResult = shell_exec("which svgo");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :path, when I run `which cryopng`
     */
    public function whenIRunWhichCryopng($path)
    {
        $execResult = shell_exec("which cryopng");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :path, when I run `which pngout`
     */
    public function whenIRunWhichPngout($path)
    {
        $execResult = shell_exec("which pngout");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :version, when I run `convert --version`
     */
    public function whenIRunConvertVersion($version)
    {
        $execResult = shell_exec("convert --version");
        PHPUnit_Framework_Assert::assertStringStartsWith($version, $execResult);
    }

    /**
     * @Given :path, when I run `which rabbitmq-server`
     */
    public function whenIRunWhichRabbitmqServer($path)
    {
        $execResult = shell_exec("which rabbitmq-server");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :path, when I run `which defluff`
     */
    public function whenIRunWhichDefluff($path)
    {
        $execResult = shell_exec("which defluff");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :path, when I run `which pngrewrite`
     */
    public function whenIRunWhichPngrewrite($path)
    {
        $execResult = shell_exec("which pngrewrite");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :path, when I run `which python2.7`
     */
    public function whenIRunWhichPython($path)
    {
        $execResult = shell_exec("which python2.7");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }

    /**
     * @Given :arg1, when I run `which java`
     */
    public function whenIRunWhichJava($path)
    {
        $execResult = shell_exec("which java");
        PHPUnit_Framework_Assert::assertStringStartsWith($path, $execResult);
    }
}

Feature: Test server environment
    On install tools to different servers
    As need a developer and owner
    I need a stable and similar configuration

    Scenario: Test Aptitude
        Given "aptitude", when I run `aptitude --version`

    Scenario: Test PHP 7.0
        Given "PHP 7.0", when I run `php --version`

    Scenario: Test Python 2.7
        Given "/usr/bin/python2.7", when I run `which python2.7`

    Scenario: Test pip
        Given "pip 9", when I run `pip --version`

    Scenario: Test Git
        Given "git version 2", when I run `git --version`

    Scenario: Test gcc
        Given "gcc", when I run `gcc --version`

    Scenario: Test make
        Given "GNU Make", when I run `make --version`

    Scenario: Test composer
        Given "Composer version 1", when I run `composer.phar --version`

    Scenario: Test nodejs
        Given "v7", when I run `node --version`

    Scenario: Test npm
        Given "4", when I run `npm --version`

    Scenario: Test pm2
        Given "2", when I run `sudo pm2 --version`

    Scenario: Test RabbitMQ
        Given "/usr/sbin/rabbitmq-server", when I run `which rabbitmq-server`

    Scenario: Test RabbitMQ Admin
        Given "rabbitmqadmin", when I run `rabbitmqadmin --version`

    Scenario: Test Inkscape
        Given "Inkscape 0.9", when I run `inkscape --version`

    Scenario: Test ps2eps
        Given "Version: 1", when I run `ps2eps --version`

    Scenario: Test CairoSVG
        Given "1", when I run `cairosvg --version`

    Scenario: Test svg-path-bounding-box
        Given "/usr/bin/svg-path-bounding-box", when I run `which svg-path-bounding-box`

    Scenario: Test svgo
        Given "/usr/bin/svgo", when I run `which svgo`

    Scenario: Test cryopng
        Given "/usr/local/bin/cryopng", when I run `which cryopng`

    Scenario: Test pngout
        Given "/usr/local/bin/pngout", when I run `which pngout`

    Scenario: Test defluff
        Given "/usr/local/bin/defluff", when I run `which defluff`

    Scenario: Test pngrewrite
        Given "/usr/local/bin/pngrewrite", when I run `which pngrewrite`

    Scenario: Test ImageMagick
        Given "Version: ImageMagick 6", when I run `convert --version`

    Scenario: Test java
        Given "/usr/bin/java", when I run `which java`

#!/usr/bin/env bash

PROJECT_DIR='/vagrant'
HOME_DIR='/home/ubuntu'
TOOLS_USER='ubuntu'
TOOLS_GROUP='ubuntu'
RABBITMQ_USER='test'
RABBITMQ_PASSWORD='test'

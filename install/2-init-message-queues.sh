#!/bin/bash

# Load environment variables depends on parameter
if [ "$1" == "--prod" ]; then
    source prod_env.sh
else
    source vagrant_env.sh
fi

# очереди сообщений RabbitMQ

# дополнительные пакеты
sudo aptitude install -y rabbitmq-server

# менеджер RabbitMQ https://www.rabbitmq.com/management-cli.html
wget https://raw.githubusercontent.com/rabbitmq/rabbitmq-management/rabbitmq_v3_5_4/bin/rabbitmqadmin -O /tmp/rabbitmqadmin
sudo mv /tmp/rabbitmqadmin /usr/local/bin/rabbitmqadmin
sudo chmod +x /usr/local/bin/rabbitmqadmin
sudo sh -c 'rabbitmqadmin --bash-completion > /etc/bash_completion.d/rabbitmqadmin'

sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server restart

sudo rabbitmqctl add_user $RABBITMQ_USER $RABBITMQ_PASSWORD
sudo rabbitmqctl set_user_tags $RABBITMQ_USER administrator
sudo rabbitmqctl set_permissions -p / $RABBITMQ_USER ".*" ".*" ".*"

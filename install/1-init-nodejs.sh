#!/usr/bin/env bash

# Load environment variables depends on parameter
if [ "$1" == "--prod" ]; then
    source prod_env.sh
else
    source vagrant_env.sh
fi

curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y npm
sudo npm update -g npm


# настройка pm2 - менеджера процессов node
sudo npm install -g pm2@latest
sudo npm update -g pm2
sudo pm2 update
sudo pm2 startup $TOOLS_USER -u $TOOLS_USER --hp /home/ubuntu
sudo chown -R $TOOLS_USER:$TOOLS_GROUP $HOME_DIR/.pm2

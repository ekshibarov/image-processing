#!/usr/bin/env bash

# Load environment variables depends on parameter
if [ "$1" == "--prod" ]; then
    source prod_env.sh
else
    source vagrant_env.sh
fi

# слушатель очередей

# остановка слушателя очередей
if [ -d $PROJECT_DIR/amqp-consumer ];
then
    cd $PROJECT_DIR/amqp-consumer
    pm2 stop amqp-consumer-daemon.js
fi

# дополнительные пакеты

# установка/обновление приложения
cd $PROJECT_DIR/amqp-consumer
npm install
npm update

# запуск слушателя
pm2 start amqp-consumer-daemon.js
pm2 save

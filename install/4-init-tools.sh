#!/usr/bin/env bash

# Load environment variables depends on parameter
if [ "$1" == "--prod" ]; then
    source prod_env.sh
else
    source vagrant_env.sh
fi

# фронтенд приложения

# дополнительные пакеты
sudo add-apt-repository ppa:inkscape.dev/stable
sudo apt-get update
sudo aptitude install -y inkscape ps2eps
sudo npm install -g svg-path-bounding-box

# директория под логи приложения
mkdir -p $HOME_DIR/logs/tools.icons8.com

# права доступа
sudo chown -R $TOOLS_USER:www-data $PROJECT_DIR/tools.icons8.com

# сжатие PNG
bash tools/imgo-install.sh

# сжатие и упрощение SVG
bash tools/svgo-install.sh
bash tools/xvfb-install.sh

# генерация PDF
bash tools/cairosvg-install.sh

# права доступа
sudo chown -R $TOOLS_USER:www-data $PROJECT_DIR/tools.icons8.com

echo "Icons8 tools.icons8.com installed"

#!/usr/bin/env bash
# Load environment variables depends on parameter
if [ "$1" == "--prod" ]; then
    source prod_env.sh
else
    source vagrant_env.sh
fi

# Install languages and tools, like PHP, Python and gcc
sudo apt-get install -y aptitude
sudo apt-get install -y php7.0-cli php7.0-mbstring php7.0-curl php7.0-xml php7.0-bcmath php7.0-zip
sudo apt-get install -y git
sudo apt-get install -y gcc
sudo apt-get install -y make
sudo apt-get install -y python python-pip python-dev libffi-dev
pip install --upgrade pip
sudo pip install --upgrade setuptools
sudo apt install -y openjdk-9-jdk
sudo apt install -y openjdk-9-jre

# Install composer
EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
RESULT=$?
rm composer-setup.php
sudo mv composer.phar /usr/local/bin/

# Install composer packages for different subprojects
cd ../tests
composer.phar install
cd ../tools.icons8.com/protected
composer.phar install

# For tools, create local config file from dist
cd $PROJECT_DIR/tools.icons8.com/protected/config
cp params.local.php-vagrant-dist params.local.php

# For amqp consumer, create local config file from dist
cd $PROJECT_DIR/amqp-consumer
cp config.json-vagrant-dist config.json

# Create runtime directore and set permissions
mkdir -p $HOME_DIR/runtime/tools.icons8.com
chmod 777 $HOME_DIR/runtime/tools.icons8.com

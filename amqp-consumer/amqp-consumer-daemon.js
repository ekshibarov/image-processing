var amqp = require('amqp'),
    spawn = require('child_process').spawn,
    config = require('./config');

var conn = amqp.createConnection({ url: config.url }, {
    reconnect: true, // Enable reconnection
    reconnectBackoffStrategy: 'linear',
    reconnectBackoffTime: 1000 // Try reconnect once a second
});
conn.on('error', function(err){
    // выйти при ошибках соединения с сервером
    console.log('Consumer:',err);
    process.exit(1);
});

// @see https://nodejs.org/docs/latest/api/events.html#events_emitter_setmaxlisteners_n Количество слушателей событий
// @see http://stackoverflow.com/a/5527037/3544196 Как посчитать количество свойств объекта
var queuesCount = Object.keys(config.queues).map(function(key){ return config.queues.hasOwnProperty(key); }).length;
console.log('Starting ' + queuesCount + ' queue(s)');
conn.setMaxListeners(2+queuesCount);

conn.on('ready', function () {

    Object.keys(config.queues).forEach(function(queueName){

        var queueConfig = config.queues[queueName];
        conn.queue(queueName, { autoDelete: false, durable: true, exclusive: false, passive: false }, function(queue) {
            console.log('Consumer: Queue', queueName, 'ready');

            var exchange = conn.exchange('');

            queue.subscribe({ ack: true, prefetchCount: queueConfig.prefetchCount }, function(msg, headers, deliveryInfo, ack) {
                var data = ((msg || {}).data || {}).toString();

                try {
                    var child = spawn(queueConfig.cmd, queueConfig.args, { cwd: queueConfig.cwd });

                    child.stdin.write(data); // передать сообщение через STDIN (как бы echo "<message>" | cmd)
                    child.stdin.end();

                    child.stdout.on('data', function (data) {
                        console.log('Consumer: stdout: ' + data);
                    });

                    child.stderr.on('data', function (data) {
                        console.log('Consumer: stderr: ' + data);
                    });

                    child.on('close', function (code) { // успешное завершение вызова
                        console.log('Consumer: child process exited with code ' + code);
                        finish(code);
                    });

                    child.on('error', function (err) {
                        console.log('Consumer: child process error', err);
                    });
                }
                catch (err) {
                    finish(err);
                }

                function finish(err) {
                    if (err) {
                        console.log('Consumer: Queue', queueName, data, 'error', err.message||err);
                        setTimeout( // повторить обработку этого же сообщения через некоторое (случайное) время
                            function(){
                                exchange.publish(queueName, data);
                                ack.acknowledge();
                            },
                            queueConfig.retryTimeout + Math.random() * queueConfig.retryTimeout
                        );
                    } else {
                        console.log('Consumer: Queue', queueName, 'ok');
                        ack.acknowledge();
                    }

                }

            });
        });
    });
});

#!/usr/bin/env bash
# запуск транслятора Scalable Vector Graphics -> PostScript

scriptpath=(`dirname $0`)
javabin=$1
svgpath=$2
pspath=$3

pushd .
cd ${scriptpath}

cmd="${javabin} -jar fop.jar -imagein ${svgpath}  -ps ${pspath}"
echo ${cmd}
`${cmd}`

popd
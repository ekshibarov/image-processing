#!/usr/bin/env bash

log=/tmp/run-redraw-simp-svg.txt
fail=0

# подбор интерпретатора
php="/usr/bin/env php"
if [ -e /usr/local/php54/bin/php ];
then
   # это для пром сервера
   php="/usr/local/php54/bin/php"
fi

if [ -z $1 ];
then
    echo "Не указана директория"
    exit 1
fi
folder=$1

echo 'start' > $log
date "+%Y%m%d_%H%M%S" >> $log

proc=1

# Многопоточное упрощение всех SVG
echo "Упрощение всех SVG в папке $folder" >> $log
find $folder -iname '*.svg' -print0 | xargs --null --no-run-if-empty --replace=%% --max-args=1 --max-procs=$proc sh -c "$php yiic.php svg draw --svg='%%' --lazy --no_png --no_eps --no_pdf --no_cross"

echo 'simplified' >> $log

date "+%Y%m%d_%H%M%S" >> $log
if [ $fail -ne 0 ];
then
    cat $log;
fi
rm $log

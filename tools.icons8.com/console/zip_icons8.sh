#!/bin/bash

# if Dropbox/changer is changed
if [  -e "/tmp/refresh-zip" ]; then
    unlink /tmp/refresh-zip
    destination="/home/icons8/s.icons8.com/download"
    ver=`date +%N`

    # update icons
    bash ./generatezipshared/zip_icons8_all_sizes.sh
    bash ./generatezipshared/zip_icons8_all_sizes_all.sh
    bash ./generatezipshared/zip_icons8_basic.sh
    bash ./generatezipshared/zip_icons8_basic_all.sh
    bash ./generatezipshared/zip_icons8_extended.sh
    # большой сложный архив, упаковывается отдельной крон-задачей bash ./generatezipshared/zip_icons8_extended_all.sh

    # rename temp ZIP
    cd $destination/
    list=`find ./ -name "*-$ver.zip" `
    for i in $list ;
    do
        mv $i ${i%-$ver.zip}.zip
    done

    #chown -R icons8:www-data $destination/
fi
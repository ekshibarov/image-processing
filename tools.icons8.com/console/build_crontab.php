<?php

if ($argc < 2)
{
	echo "Usage: " . $argv[0] . ' <path-to-crontab>', PHP_EOL;
	exit(-1);
}

define('APP_PATH', dirname(dirname(__FILE__)));

ob_start();
include APP_PATH . '/protected/config/cron/crontab.php';
$crontab = ob_get_clean();

if ('--' == $argv[1])
{
	echo $crontab;
} else
{
	file_put_contents($argv[1], $crontab);
}
#!/usr/bin/env bash

export PATH="/usr/local/rvm/gems/ruby-2.1.4/bin:/usr/local/rvm/gems/ruby-2.1.4@global/bin:/usr/local/rvm/rubies/ruby-2.1.4/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/rvm/bin:/home/icons8/bin"

# test with report to email
# bash sync.sh | mail -s "tools.icons8.com command sync is done" pavel@visualpharm.com

PWD=`pwd`
DATE=`date +%Y%m%d.%H%M%S`
QUEUE_LIST="$PWD/../protected/runtime/sync-queue-$DATE.txt"
FAIL_LIST="$PWD/../protected/runtime/sync-queue-$DATE-error.log"
PHP="/usr/bin/env php"
if [ -e "./php.ini" ];
then
    PHP="$PHP -c ./php.ini"
fi

# `$php yiic.php updater sync` делает сравнение структуры файлов и запускает задания в очереди
# на удаление неннужных файлов и на отрисовку новых

if [ "$1" ];
then
    QUEUE_LIST=$1
    if [ ! -e $QUEUE_LIST ];
    then
        echo "File ${QUEUE_LIST} not found"
        exit
    fi
    echo "Use file list ${QUEUE_LIST}"
else
    echo "Find changed icons into file list ${QUEUE_LIST}"
    $PHP yiic.php updater sync --out="${QUEUE_LIST}"
fi

if [ -e $QUEUE_LIST ]; then
    echo "  Draw icons by list ${QUEUE_LIST}"
    ICONS=`cat $QUEUE_LIST`
    for ICON in $ICONS
    do
        $PHP yiic.php svg draw --svg="${ICON}" --faillog="${FAIL_LIST}"
    done
    if [ -e $FAIL_LIST ]; then
        echo "    Some icons not rendered. Report: ${FAIL_LIST}"
        cat $FAIL_LIST
    fi
else
   echo "  No changes in icons found"
fi

echo "Sync complete, everything is up to date"

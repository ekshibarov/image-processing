<?php

define('APP_PATH', dirname(dirname(__FILE__)));

$yiic = APP_PATH . '/../framework/yiic.php';
$config = APP_PATH . '/protected/config/console.php';

// how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);

ini_set('memory_limit', '2G');

require_once($yiic);

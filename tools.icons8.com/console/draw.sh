#!/usr/bin/env bash

export PATH="/usr/local/rvm/gems/ruby-2.1.4/bin:/usr/local/rvm/gems/ruby-2.1.4@global/bin:/usr/local/rvm/rubies/ruby-2.1.4/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/rvm/bin:/home/icons8/bin"

PHP="/usr/bin/env php"
if [ -e "./php.ini" ];
then
    PHP="$PHP -c ./php.ini"
fi

if [ -z $1 ];
then
    echo "Не указан файл"
    exit 1
fi

# TODO если много файлов
#find $folder -iname '*.svg' -print0 | xargs --null --no-run-if-empty --replace=%% --max-args=1 --max-procs=$proc sh -c "$php yiic.php svg draw --svg='%%'"

# Если один файл
ICON=$1
$PHP yiic.php svg draw --svg="${ICON}"

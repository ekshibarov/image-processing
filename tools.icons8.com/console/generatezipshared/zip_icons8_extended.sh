#!/bin/bash
source="/home/icons8/shared-icons"
destination="/home/icons8/s.icons8.com/download"
ver=`date +%N`

cd  $source/windows8
# all files
zip  -9  $destination/windows8_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r SVG_for_Font -i "*.svg"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r SVG_for_Font -i "*.SVG"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r EPS -i "*.eps"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r PSD -i "*.psd"
zip  -9  $destination/windows8_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/windows8/Extra
zip  -9  $destination/windows8_icons_extended-$ver.zip -r . -i "icons8.ai" -i "icons8.eps"  -i "icons8.pdf" -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd  $source/windows10
# all files
zip  -9  $destination/windows10_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r SVG_for_Font -i "*.svg"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r SVG_for_Font -i "*.SVG"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r EPS -i "*.eps"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r PSD -i "*.psd"
zip  -9  $destination/windows10_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/windows8/Extra
zip  -9  $destination/windows8_icons_extended-$ver.zip -r . -i "icons8.ai" -i "icons8.eps"  -i "icons8.pdf" -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/iOS7
#  all files
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r PSD -i "*.psd"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r EPS -i "*.eps"
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/iOS7/Extra
zip  -9  $destination/iOS7_icons_extended-$ver.zip -r . -i "iOS7.ai" -i "iOS7.eps" -i "iOS7.pdf"  -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/Android
#  all files
zip  -9  $destination/android_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/android_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/android_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/android_icons_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
zip  -9  $destination/android_icons_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/android_icons_extended-$ver.zip -r PSD -i "*.psd"
zip  -9  $destination/android_icons_extended-$ver.zip -r PDF -i "*.pdf"
zip  -9  $destination/android_icons_extended-$ver.zip -r EPS -i "*.eps"
cd  $source/Android/Extra
zip  -9  $destination/android_icons_extended-$ver.zip -r . -i "android.ai" -i "android.eps"  -i "android.pdf" -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/Android_L
#  all files
zip  -9  $destination/android_l_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r PSD -i "*.psd"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r PDF -i "*.pdf"
zip  -9  $destination/android_l_icons_extended-$ver.zip -r EPS -i "*.eps"
cd  $source/Android_L/Extra
zip  -9  $destination/android_l_icons_extended-$ver.zip -r . -i "android_lollipop.ai"  -i "android_lollipop.eps"  -i "android_lollipop.pdf" -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/MacOS
#  all files
zip  -9  $destination/macos_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/macos_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/macos_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/macos_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/MacOS/Extra
zip  -9  $destination/macos_icons_extended-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/Color
#  all files
zip  -9  $destination/color_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/color_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/color_icons_extended-$ver.zip -r SVG -i "*.SVG"
#TODO zip  -9  $destination/color_icons_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
#TODO zip  -9  $destination/color_icons_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/color_icons_extended-$ver.zip -r EPS -i "*.eps"
zip  -9  $destination/color_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/Color/Extra
zip  -9  $destination/color_icons_extended-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/office
#  all files
zip  -9  $destination/office_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/office_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/office_icons_extended-$ver.zip -r SVG -i "*.SVG"
#TODO zip  -9  $destination/office_icons_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
#TODO zip  -9  $destination/office_icons_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/office_icons_extended-$ver.zip -r EPS -i "*.eps"
zip  -9  $destination/office_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/office/Extra
zip  -9  $destination/office_icons_extended-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd  $source/Responsive
# all files
# TODO zip  -9  $destination/responsive_icons_extended-$ver.zip -r PNG -i "*.png"
zip  -9  $destination/responsive_icons_extended-$ver.zip -r SVG -i "*.svg"
zip  -9  $destination/responsive_icons_extended-$ver.zip -r SVG -i "*.SVG"
zip  -9  $destination/responsive_icons_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
zip  -9  $destination/responsive_icons_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/responsive_icons_extended-$ver.zip -r PDF -i "*.pdf"
cd  $source/Responsive/Extra
zip  -9  $destination/responsive_icons_extended-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"


# remove old and rename temp ZIP
cd $destination
unlink  iOS7_icons_extended.zip
unlink  windows8_icons_extended.zip 
unlink  windows10_icons_extended.zip
unlink  android_icons_extended.zip
unlink  android_l_icons_extended.zip
unlink  macos_icons_extended.zip
unlink  color_icons_extended.zip
unlink  office_icons_extended.zip
unlink  responsive_icons_extended.zip
list=`find ./ -name "*-$ver.zip" `
for i in $list ;
do
    mv $i ${i%-$ver.zip}.zip
done

sleep 10

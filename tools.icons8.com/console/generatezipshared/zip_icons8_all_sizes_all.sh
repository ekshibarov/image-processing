#!/bin/bash
source="/home/icons8/shared-icons"
destination="/home/icons8/s.icons8.com/download"
ver=`date +%N`


cd $source
# PNG all sizes
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r windows8/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r iOS7/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r Android/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r Android_L/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r Color/PNG -i "*.png"
#zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r MacOS/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r windows10/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r office/PNG -i "*.png"
# TODO zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r Responsive/PNG -i "*.png"

# license
cd $source/Extra
zip  -9  $destination/all_icons_by_Icons8-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"


# remove old and rename temp ZIP
cd $destination
unlink all_icons_by_Icons8.zip
mv all_icons_by_Icons8-$ver.zip all_icons_by_Icons8.zip
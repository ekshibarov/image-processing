#!/bin/bash
source="/home/icons8/shared-icons"
destination="/home/icons8/s.icons8.com/download"
tmp="/tmp/files_for_zip"
ver=`date +%N`

if [ -e $tmp ]; then rm -rf $tmp; fi
mkdir -p $tmp/PNG/100
mkdir -p $tmp/"SVG_EPS_PDF"
ln -s $source/Extra/"Promo Free Web App.pdf" $tmp/PNG/100/"How to get 100x100.pdf"
ln -s $source/Extra/"How to get SVG EPS and PDF.txt" $tmp/"SVG_EPS_PDF/How to get SVG EPS and PDF.txt"

cd  $source/windows8
# PNG < 50px
zip  -9  $destination/windows8_icons_free-$ver.zip -r PNG -i "*-26.png"
zip  -9  $destination/windows8_icons_free-$ver.zip -r PNG -i "*-32.png"
zip  -9  $destination/windows8_icons_free-$ver.zip -r PNG -i "*-48.png"
cd  $source/windows8/Extra
zip  -9  $destination/windows8_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/windows8_icons_free-$ver.zip -r . -i "*.*"

cd $source/iOS7
# PNG < 50px
zip  -9  $destination/iOS7_icons_free-$ver.zip -r PNG -i "*-25.png"
zip  -9  $destination/iOS7_icons_free-$ver.zip -r PNG -i "*-32.png"
zip  -9  $destination/iOS7_icons_free-$ver.zip -r PNG -i "*-50.png"
cd  $source/iOS7/Extra
zip  -9  $destination/iOS7_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/iOS7_icons_free-$ver.zip -r . -i "*.*"

cd $source/Android
# PNG < 50px
zip  -9  $destination/android_icons_free-$ver.zip -r PNG -i "*-24.png"
zip  -9  $destination/android_icons_free-$ver.zip -r PNG -i "*-32.png"
zip  -9  $destination/android_icons_free-$ver.zip -r PNG -i "*-36.png"
zip  -9  $destination/android_icons_free-$ver.zip -r PNG -i "*-48.png"
cd  $source/Android/Extra
zip  -9  $destination/android_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/android_icons_free-$ver.zip -r . -i "*.*"

cd $source/windows10
# PNG < 50px
zip  -9  $destination/windows10_icons_free-$ver.zip -r PNG -i "*-16.png"
zip  -9  $destination/windows10_icons_free-$ver.zip -r PNG -i "*-32.png"
zip  -9  $destination/windows10_icons_free-$ver.zip -r PNG -i "*-48.png"
cd  $source/windows10/Extra
zip  -9  $destination/windows10_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/windows10_icons_free-$ver.zip -r . -i "*.*"

cd $source/Android_L
# PNG < 50px
zip  -9  $destination/android_l_icons_free-$ver.zip -r PNG -i "*-24.png"
zip  -9  $destination/android_l_icons_free-$ver.zip -r PNG -i "*-32.png"
zip  -9  $destination/android_l_icons_free-$ver.zip -r PNG -i "*-36.png"
zip  -9  $destination/android_l_icons_free-$ver.zip -r PNG -i "*-48.png"
cd  $source/Android_L/Extra
zip  -9  $destination/android_l_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/android_l_icons_free-$ver.zip -r . -i "*.*"

#cd $source/MacOS
# PNG < 50px
#zip  -9  $destination/macos_icons_free-$ver.zip -r PNG -i "*-16.png"
#zip  -9  $destination/macos_icons_free-$ver.zip -r PNG -i "*-18.png"
#zip  -9  $destination/macos_icons_free-$ver.zip -r PNG -i "*-32.png"
#zip  -9  $destination/macos_icons_free-$ver.zip -r PNG -i "*-36.png"
#zip  -9  $destination/macos_icons_free-$ver.zip -r PNG -i "*-48.png"
#cd  $source/MacOS/Extra
#zip  -9  $destination/macos_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
#cd $tmp
#zip  -9  $destination/macos_icons_free-$ver.zip -r . -i "*.*"

cd $source/Color
# PNG < 50px
zip  -9  $destination/color_icons_free-$ver.zip -r PNG -i "*-24.png"
zip  -9  $destination/color_icons_free-$ver.zip -r PNG -i "*-48.png"
cd  $source/Color/Extra
zip  -9  $destination/color_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/color_icons_free-$ver.zip -r . -i "*.*"

cd $source/office
# PNG < 50px
zip  -9  $destination/office_icons_free-$ver.zip -r PNG -i "*-16.png"
zip  -9  $destination/office_icons_free-$ver.zip -r PNG -i "*-30.png"
zip  -9  $destination/office_icons_free-$ver.zip -r PNG -i "*-40.png"
zip  -9  $destination/office_icons_free-$ver.zip -r PNG -i "*-60.png"
zip  -9  $destination/office_icons_free-$ver.zip -r PNG -i "*-80.png"
cd  $source/office/Extra
zip  -9  $destination/office_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"
cd $tmp
zip  -9  $destination/office_icons_free-$ver.zip -r . -i "*.*"

# TODO cd $source/Responsive
# TODO PNG < 50px
# TODO zip  -9  $destination/responsive_icons_free-$ver.zip -r PNG -i "*-24.png"
# TODO zip  -9  $destination/responsive_icons_free-$ver.zip -r PNG -i "*-48.png"
# TODO cd  $source/Responsive/Extra
# TODO zip  -9  $destination/responsive_icons_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"

# remove old and rename temp ZIP
cd $destination
unlink  iOS7_icons_free.zip
unlink  windows8_icons_free.zip
unlink  windows10_icons_free.zip
unlink  android_icons_free.zip
unlink  android_l_icons_free.zip
#unlink  macos_icons_free.zip
unlink color_icons_free.zip
unlink office_icons_free.zip
# TODO unlink responsive_icons_free.zip
list=`find ./ -name "*-$ver.zip" `
for i in $list ;
do
    mv $i ${i%-$ver.zip}.zip # rename temp ZIP
done
rm -rf $tmp
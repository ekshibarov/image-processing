#!/bin/bash
source="/home/icons8/shared-icons"
destination="/home/icons8/s.icons8.com/download"
tmp="/tmp/files_for_zip_all"
ver=`date +%N`

if [ -e $tmp ]; then rm -rf $tmp; fi
mkdir -p $tmp/{windows8,iOS7,Android,Android_L,Color,windows10}/PNG/100
mkdir -p $tmp/{windows8,iOS7,Android,Android_L,Color,windows10}/"SVG_EPS_PDF"

# create promo-files structure for adding to archive
for d in $tmp/* ; do
    ln -s $source/Extra/"Promo Free Web App.pdf" $d/PNG/100/"How to get 100x100.pdf"
    ln -s $source/Extra/"How to get SVG EPS and PDF.txt" $d/"SVG_EPS_PDF/How to get SVG EPS and PDF.txt"
done

cd  $source
# PNG < 50px
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r windows8/PNG -i "*-26.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r windows8/PNG -i "*-32.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r windows8/PNG -i "*-48.png"

zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r windows10/PNG -i "*-16.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r windows10/PNG -i "*-32.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r windows10/PNG -i "*-48.png"

zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r iOS7/PNG -i "*-25.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r iOS7/PNG -i "*-32.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r iOS7/PNG -i "*-50.png"

zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android/PNG -i "*-24.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android/PNG -i "*-32.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android/PNG -i "*-36.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android/PNG -i "*-48.png"

zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android_L/PNG -i "*-24.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android_L/PNG -i "*-32.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android_L/PNG -i "*-36.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Android_L/PNG -i "*-48.png"

zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Color/PNG -i "*-24.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Color/PNG -i "*-48.png"

zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r office/PNG -i "*-16.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r office/PNG -i "*-30.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r office/PNG -i "*-40.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r office/PNG -i "*-60.png"
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r office/PNG -i "*-80.png"

#zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r MacOS/PNG -i "*-16.png"
#zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r MacOS/PNG -i "*-18.png"
#zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r MacOS/PNG -i "*-32.png"
#zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r MacOS/PNG -i "*-36.png"
#zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r MacOS/PNG -i "*-48.png"

# TODO zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Responsive/PNG -i "*-16.png"
# TODO zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Responsive/PNG -i "*-18.png"
# TODO zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r Responsive/PNG -i "*-32.png"

# license
cd  $source/Extra
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r . -i "Free License.pdf" -i "Promo App for Mac and Windows.pdf" -i "Promo Free Web App.pdf"

# promo-files about vectors and sizes
cd $tmp
zip  -9  $destination/all_icons_by_Icons8_free-$ver.zip -r . -i "*.*"

# remove old and rename temp ZIP
cd $destination
unlink all_icons_by_Icons8_free.zip
mv all_icons_by_Icons8_free-$ver.zip all_icons_by_Icons8_free.zip
rm -rf $tmp
sleep 10
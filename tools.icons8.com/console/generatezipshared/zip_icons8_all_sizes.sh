#!/bin/bash
source="/home/icons8/shared-icons"
destination="/home/icons8/s.icons8.com/download"
ver=`date +%N`


cd $source/windows8
# PNG all sizes
zip  -9  $destination/windows8_icons-$ver.zip -r PNG -i "*.png"
cd $source/windows8/Extra
zip  -9  $destination/windows8_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/windows10
# PNG all sizes
zip  -9  $destination/windows10_icons-$ver.zip -r PNG -i "*.png"
cd $source/windows8/Extra
zip  -9  $destination/windows10_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/iOS7
# PNG all sizes
zip  -9  $destination/iOS7_icons-$ver.zip -r PNG -i "*.png"
cd $source/iOS7/Extra
zip  -9  $destination/iOS7_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/Android
# PNG all sizes
zip  -9  $destination/android_icons-$ver.zip -r PNG -i "*.png"
cd $source/Android/Extra
zip  -9  $destination/android_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/Android_L
# PNG all sizes
zip  -9  $destination/android_l_icons-$ver.zip -r PNG -i "*.png"
cd $source/Android_L/Extra
zip  -9  $destination/android_l_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/Color
# PNG all sizes
zip  -9  $destination/color_icons-$ver.zip -r PNG -i "*.png"
cd $source/Color/Extra
zip  -9  $destination/color_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

cd $source/office
# PNG all sizes
zip  -9  $destination/office_icons-$ver.zip -r PNG -i "*.png"
cd $source/Color/Extra
zip  -9  $destination/office_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"


cd $source/MacOS
# PNG all sizes
zip  -9  $destination/macos_icons-$ver.zip -r PNG -i "*.png"
cd $source/MacOS/Extra
zip  -9  $destination/macos_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"

# TODO cd $source/Responsive
# TODO PNG all sizes
# TODO zip  -9  $destination/responsive_icons-$ver.zip -r PNG -i "*.png"
# TODO cd $source/Responsive/Extra
# TODO zip  -9  $destination/responsive_icons-$ver.zip -r . -i "Icons8_License.pdf" -i "Promo App for Mac and Windows Paid.pdf" -i "Promo Free Web App Paid.pdf"


cd $destination/
# remove old
unlink  iOS7_icons.zip
unlink  windows8_icons.zip
unlink  windows10_icons.zip
unlink  android_icons.zip
unlink  android_l_icons.zip
unlink  color_icons.zip
unlink  office_icons.zip
unlink  macos_icons.zip
# TODO unlink  responsive_icons.zip
# rename temp ZIP
list=`find ./ -name "*-$ver.zip" `
for i in $list ;
do
    mv $i ${i%-$ver.zip}.zip
done
sleep 10

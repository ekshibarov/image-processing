#!/bin/bash
source="/home/icons8/shared-icons"
destination="/home/icons8/s.icons8.com/download"
tmp="/tmp/zip_icons8"
ver=`date +%N`

if [ -e $tmp ]; then rm -rf $tmp; fi
mkdir -p $tmp
ln -s $source/windows8/Extra $tmp/windows8
ln -s $source/iOS7/Extra $tmp/iOS7
ln -s $source/Android/Extra $tmp/Android
ln -s $source/Android_L/Extra $tmp/Android_L
ln -s $source/MacOS/Extra $tmp/MacOS
ln -s $source/Color/Extra $tmp/Color
ln -s $source/Responsive/Extra $tmp/Responsive

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/SVG_for_Font -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/SVG_for_Font -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/PDF -i "*.pdf"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows8/PSD -i "*.psd"
cd $tmp
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r . -i "windows8/icons8.ai" -i "windows8/icons8.eps"  -i "windows8/icons8.pdf"

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/SVG_for_Font -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/SVG_for_Font -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/PDF -i "*.pdf"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r windows10/PSD -i "*.psd"
cd $tmp
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r . -i "windows8/icons8.ai" -i "windows8/icons8.eps"  -i "windows8/icons8.pdf"

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/SVG_for_Font  -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/SVG_for_Font  -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/PSD -i "*.psd"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r iOS7/PDF -i "*.pdf"
cd $tmp
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r . -i "iOS7/iOS7.ai" -i "iOS7/iOS7.eps" -i "iOS7/iOS7.pdf"

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/SVG_for_Font  -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/SVG_for_Font  -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/PSD -i "*.psd"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android/PDF -i "*.pdf"
cd $tmp
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r . -i "Android/android.ai" -i "Android/android.eps"  -i "Android/android.pdf"

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/SVG_for_Font  -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/SVG_for_Font  -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/PSD -i "*.psd"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Android_L/PDF -i "*.pdf"
cd $tmp
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r .  -i "Android_L/android_lollipop.ai"  -i "Android_L/android_lollipop.eps"  -i "Android_L/android_lollipop.pdf"

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r MacOS/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r MacOS/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r MacOS/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r MacOS/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r MacOS/PDF -i "*.pdf"
# TODO cd $tmp
# TODO add extra files

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Color/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Color/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Color/SVG -i "*.SVG"
#TODO zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
#TODO zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Color/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Color/PDF -i "*.pdf"
# TODO cd $tmp
# TODO add extra files

cd  $source/
# all files
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r office/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r office/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r office/SVG -i "*.SVG"
#TODO zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r SVG_for_Font  -i "*.svg"
#TODO zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r SVG_for_Font  -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r office/EPS -i "*.eps"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r office/PDF -i "*.pdf"
# TODO cd $tmp
# TODO add extra files

cd  $source/
# all files
# TODO zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Responsive/PNG -i "*.png"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Responsive/SVG -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Responsive/SVG -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Responsive/SVG_for_Font  -i "*.svg"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Responsive/SVG_for_Font  -i "*.SVG"
zip  -9  $destination/all_icons_by_Icons8_extended-$ver.zip -r Responsive/PDF -i "*.pdf"
# TODO cd $tmp
# TODO add extra files

# license
cd  $source/Extra
zip -9 $destination/all_icons_by_Icons8_extended-$ver.zip "Icons8_License.pdf"
zip -9 $destination/all_icons_by_Icons8_extended-$ver.zip "Promo App for Mac and Windows Paid.pdf"
zip -9 $destination/all_icons_by_Icons8_extended-$ver.zip "Promo Free Web App Paid.pdf"


# rename temp ZIP
cd $destination
unlink all_icons_by_Icons8_extended.zip
mv all_icons_by_Icons8_extended-$ver.zip  all_icons_by_Icons8_extended.zip


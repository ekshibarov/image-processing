#!/usr/bin/env bash

log=/tmp/run-recolor-svg.txt
fail=0

# подбор интерпретатора
php="/usr/bin/env php"
if [ -e /usr/local/php54/bin/php ];
then
   # это для пром сервера
   php="/usr/local/php54/bin/php"
fi

if [ -z $1 ];
then
    echo "Не указана директория"
    exit 1
fi
folder=$1

echo 'start' > $log
date "+%Y%m%d_%H%M%S" >> $log

proc=1

# Многопоточная упаковка всех файлов PNG в указанной директории рекурсивно
echo "Исправление тона в изображениях SVG в папке $folder" >> $log
find $folder -iname '*.svg' -print0 | xargs --null --no-run-if-empty  --replace=%% --max-args=1 --max-procs=$proc sh -c "$php yiic.php svg recolor --svg='%%' && echo '%% recolored' && $php yiic.php svg draw --svg='%%' && echo '%% redrawn'"

echo 'done' >> $log

date "+%Y%m%d_%H%M%S" >> $log
if [ $fail -ne 0 ];
then
    cat $log;
fi
rm $log

<?php

class Platform {

	const
		/** icons for iPhone/iOS 7 devices */
		API_CODE_IOS7 = 'ios7',

		/** icons for iPhone/iOS 7 devices */
		API_CODE_IOS7_FILLED = 'ios7_filled', // todo пока так надо для бандла, потом переделать

		/** icons for Windows 8/Metro devices */
		API_CODE_WINDOWS8 = 'win8',

		/** icons for Windows 10 devices */
		API_CODE_WINDOWS10 = 'win10',

		/** icons for Android devices */
		API_CODE_ANDROID = 'android',

		/** icons for Android L devices */
		API_CODE_ANDROID_L = 'androidL',

		/** color icons */
		API_CODE_COLOR = 'color',

		/** office icons */
		API_CODE_OFFICE = 'office',

		/** icons for MacOS X Yosemite */
		API_CODE_YOSEMITE = 'yosemite'
	;

	const
		/** storage subfolder for icons*/
		SUBFOLDER_IOS7 = 'iOS7',

		SUBFOLDER_WINDOWS8 = 'windows8',

		SUBFOLDER_WINDOWS10 = 'windows10',

		SUBFOLDER_ANDROID = 'Android',

		SUBFOLDER_ANDROID_L = 'Android_L',

		SUBFOLDER_COLOR = 'Color',

		SUBFOLDER_OFFICE = 'office',

		SUBFOLDER_MACOS = 'MacOS'
	;

	protected static $foldersByApiCode = array(
		self::API_CODE_IOS7 => self::SUBFOLDER_IOS7,
		self::API_CODE_WINDOWS8 => self::SUBFOLDER_WINDOWS8,
		self::API_CODE_WINDOWS10 => self::SUBFOLDER_WINDOWS10,
		self::API_CODE_ANDROID => self::SUBFOLDER_ANDROID,
		self::API_CODE_ANDROID_L => self::SUBFOLDER_ANDROID_L,
		self::API_CODE_COLOR => self::SUBFOLDER_COLOR,
		self::API_CODE_YOSEMITE => self::SUBFOLDER_MACOS
	);

	/**
	 * Возвращает массив платформ
	 * @param $onlyUsed bool true = вернуть только используемые платформы, false = вернуть все
	 * @return array массив платформ
	 */
	public static function findAllPlatforms() {
		return array (
			array (
				'platform_id' => '1',
				'name' => 'Windows 8/Metro',
				'api_code' => self::API_CODE_WINDOWS8,
			),
			array (
				'platform_id' => '2',
				'name' => 'iPhone/iOS 7',
				'api_code' => self::API_CODE_IOS7,
			),
			array (
				'platform_id' => '3',
				'name' => 'Android',
				'api_code' => self::API_CODE_ANDROID,
			),
			array (
				'platform_id' => '5',
				'name' => 'Android L',
				'api_code' => self::API_CODE_ANDROID_L,
			),
			array (
				'platform_id' => '6',
				'name' => 'Color',
				'api_code' => self::API_CODE_COLOR,
			),
            array (
                'platform_id' => '7',
                'name' => 'Windows 10/Threshold',
                'api_code' => self::API_CODE_WINDOWS10,
            ),
            array (
                'platform_id' => '8',
                'name' => 'Office',
                'api_code' => self::API_CODE_OFFICE,
            ),
		);
	}

	/**
	 * Возвращает платформу по указанному коду
	 * @param $platformApiCode string код платформы
	 * @param $onlyUsed bool true = вернуть только используемые платформы, false = вернуть все
	 * @return array|null
	 */
	public static function findPlatformByApiCode($platformApiCode, $onlyUsed = true) {
		$result = self::findAllPlatforms($onlyUsed);

		foreach($result as $row) {
			if ($platformApiCode == $row['api_code']) {
				return $row;
			}
		}

		return null;
	}

	/**
	 * Возвращает платформу по названию
	 * @param $platformName string название
	 * @param $onlyUsed bool true = вернуть только используемые платформы, false = вернуть все
	 * @return array|null
	 */
	public static function findPlatformByName($platformName, $onlyUsed = true) {
		$result = self::findAllPlatforms($onlyUsed);

		foreach($result as $row) {
			if ($platformName == $row['name']) {
				return $row;
			}
		}

		return null;
	}

	/**
	 * @param string $platformApiCode
	 * @return string|false
	 */
	public static function getSubfolderByApiCode($platformApiCode)
	{
		return isset(self::$foldersByApiCode[$platformApiCode]) ? self::$foldersByApiCode[$platformApiCode] : false;
	}

	/**
	 * @param string $subfolder
	 * @return string|false
	 */
	public static function getApiCodeBySubfolder($subfolder)
	{
		return array_search($subfolder, self::$foldersByApiCode);
	}
}

<?php
/**
 * @var array $indexingWarnings
 * @var array $manySvgImages
 */

$storagePath = rtrim(\Yii::app()->params['storage_path'], '/') . '/';
$finfo = finfo_open(FILEINFO_MIME_TYPE);
?>
<html>
<head>
	<title>Manual Fix Required</title>
	<style>
		table th { background: #999; color: #fff; font-weight: bold; text-align: center; }
		table th, table td { border: 1px solid #ccc;}
		img { margin: 5px; }
	</style>
</head>
<body>
<h1>Indexing problems</h1>

<?php if (!empty($indexingWarnings)) { ?>
<h2>Indexing warnings</h2>
<table>
    <thead>
    <tr>
        <td>File relative path</td>
        <td>File info</td>
        <td>Description</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($indexingWarnings as $r => $warnings) { ?>
        <?php
        list($description, $fileRelPath) = $warnings;
        $fileAbsPath = $storagePath . $fileRelPath;
        $mimeType = finfo_file($finfo, $fileAbsPath);
        ?>
        <tr>
            <td><?php echo $r+1, '. ', $fileRelPath ?></td>
            <td>Modified: <?php echo date('Y/m/d H:i:s', filemtime($fileAbsPath)) ?>
                <br/>
                Size: <?php echo filesize($fileAbsPath) ?>bytes
                <br/>
                SHA1: <?php echo sha1_file($fileAbsPath) ?></td>
            <td><img src="data:<?php echo $mimeType ?>;base64,<?php echo base64_encode(file_get_contents($fileAbsPath)) ?>" width="50" /></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php } ?>

<?php if (!empty($manySvgImages)) { ?>
<h2>Many SVG files with same name (name are case insensitive)</h2>
<table>
    <thead>
        <tr>
            <td>Icon relative path</td>
            <td>File info</td>
            <td>Icon image</td>
        </tr>
    </thead>
    <tbody>
    <?php foreach($manySvgImages as $r => $iconsList) { ?>
        <?php list($iconName, $platform, $fileAbsPaths) = $iconsList; ?>
        <?php if ($r > 0) ?><tr><td colspan="3">&nbsp;</td></tr>
        <tr><td colspan="3"><?php echo $r+1, '. ', $platform, '/', $iconName ?></td></tr>
        <?php foreach($fileAbsPaths as $fileRelPath => $dummy) { ?>
            <?php
                list(,,$category,) = explode('/', $fileRelPath);
                $fileAbsPath = $storagePath . $fileRelPath;
                $mimeType = finfo_file($finfo, $fileAbsPath);
            ?>
            <tr>
                <td><?php echo $fileRelPath ?></td>
                <td>Modified: <?php echo date('Y/m/d H:i:s', filemtime($fileAbsPath)) ?>
                    <br/>
                    Size: <?php echo filesize($fileAbsPath) ?>bytes
                    <br/>
                    SHA1: <?php echo sha1_file($fileAbsPath) ?>
                </td>
                <td><img src="data:<?php echo $mimeType ?>;base64,<?php echo base64_encode(file_get_contents($fileAbsPath)) ?>" width="50" /></td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<?php } ?>

</body>
</html>
<?php
/**
 * @var string $description
 * @var array $manySvgImages
 */

$storagePath = rtrim(\Yii::app()->params['storage_path'], '/') . '/';
$viewModelData = array();
?>
<html>
<head>
	<title>Manual Fix Required</title>
	<style>
		table th { background: #999; color: #fff; font-weight: bold; text-align: center; }
		table th, table td { border: 1px solid #ccc;}
		img { margin: 5px; }
		input[type=text] { border: 1px solid #999; padding: 3px; }
	</style>
</head>
<body>
<h1><?php echo $description ?></h1>
<table>
	<thead>
		<tr>
			<td>Icon relative path</td>
			<td>Icon image</td>
			<td>Remove?</td>
			<td>Move & rename?</td>
		</tr>
	</thead>
	<tbody>
	<?php foreach($manySvgImages as $r => $iconsList) { ?>
		<?php list($iconName, $platform, $iconRelPaths) = $iconsList; ?>
		<?php if ($r > 0) ?><tr><td colspan="4">&nbsp;</td></tr>
		<tr><td colspan="4"><?php echo $platform, '/', $iconName ?></td></tr>
		<?php foreach($iconRelPaths as $iconRelPath => $hash) { ?>
			<?php list(,,$category,) = explode('/', $iconRelPath); ?>
			<?php $iconAbsFile = $storagePath . $iconRelPath; ?>
			<tr>
				<td>
					<?php echo $iconRelPath ?>
					<br/>
					Modified: <?php echo date('Y/m/d H:i:s', filemtime($iconAbsFile)) ?>
					<br/>
					Size: <?php echo filesize($iconAbsFile) ?>bytes
					<br/>
					SHA1: <?php echo sha1_file($iconAbsFile) ?>
				</td>
				<td><img src="data:image/svg+xml;base64,<?php echo base64_encode(file_get_contents($iconAbsFile)) ?>" width="50" /></td>
				<td><label><input type="checkbox" id="">Remove</label></td>
				<td><input type="text" size="40" placeholder="Enter category name" value="<?php echo $category ?>"/>/<input type="text" size="40" placeholder="Enter icon name" value=""/></td>
				<?php
					$key = "{$platform}/{$iconName}";
					if (!isset($viewModelData[$key])) $viewModelData[$key] = array();
					$viewModelData[$key][] = array(
						'iconName' => $iconName,
						'platform' => $platform,
						'relPath' => $iconRelPath,
						'absPath' => $iconAbsFile,
						'sha1' => sha1_file($iconAbsFile),
						'size' => filesize($iconAbsFile),
						'time' => date('Y/m/d H:i:s', filemtime($iconAbsFile)),
						'image' => 'data:image/svg+xml;base64,' . base64_encode(file_get_contents($iconAbsFile)),
					);
				?>
			</tr>
		<?php } ?>
	<?php } ?>
	</tbody>
</table>
<script type="application/javascript">
var viewModelData = <?php echo json_encode($viewModelData); ?>
</script>
</body>
</html>
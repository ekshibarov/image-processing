<?php
/**
 * @var array $columns
 * @var array $rows
 * @var array $warnings
 * @var array $absentSvgOrig
*/

$subHeaders = '';
foreach($columns as $column)
	$subHeaders .= '<th class="' . preg_replace('/\:/', '_', $column) . '">' . preg_replace('/\:/', '<br/>', $column) . '</th>';
$subHeaders = '<tr><th>##</th>' . $subHeaders . '</tr>';

function iconFiles($iconFiles)
{
	$ret = [];
	foreach($iconFiles as $type => $listByType)
	{
		if ('png' == $type)
		{
			foreach($listByType as $size => $files){
				$ret = array_merge($ret, array_keys($files));
			}
		} else
		{
			$ret = array_merge($ret, array_keys($listByType));
		}
	}
	$ret = array_unique($ret);
	return $ret;
}
?>
<html>
<head>
	<title>SVG compare</title>
	<style>
		table { width: 80%; align: center; border-collapse: collapse; border-spacing: 0; }
		th { background: #999; color: #fff; font-weight: bold; text-align: center; }
		th, td { border: 1px solid #ccc; padding: 2px; }
		img { margin: 5px; }
		td.svg img { border: 3px solid #55bb55; background-color: #ff33ff; }
		td.svg_simp img { border: 3px solid #55bb55; background-color: #ff33ff; }
		td.svg_simp_old img { border: 3px solid #55bb55; background-color: #ff33ff; }
		td.svg_cross img { border: 3px solid #55bb55; background-color: #ff33ff; }
		td.svg_cross_old img { border: 3px solid #55bb55; background-color: #ff33ff; }
		td.svg, td.svg_simp, td.svg_simp_old, td.svg_cross, td.svg_cross_old, td.checkerboard { background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAB4CAYAAABb59j9AAAABmJLR0QA/wD/AP+gvaeTAAABnUlEQVR4nO3bsQ0DIRQFQc5yR/TfAdSEWyCxZK9m4tOLVj858ay1zrgw57z5bOy9r76zZ+8be6+rr+BPCJoUQZMiaFIETYqgSRE0KYImRdCkPOecqz+Fv/6HyJ69MVxoYgRNiqBJETQpgiZF0KQImhRBkyJoUt6VP0T27I3hQhMjaFIETYqgSRE0KYImRdCkCJoUQZPiTaG91J4LTYqgSRE0KYImRdCkCJoUQZMiaFIETYo3hfZSey40KYImRdCkCJoUQZMiaFIETYqgSRE0Kd4U2kvtudCkCJoUQZMiaFIETYqgSRE0KYImRdCkeFNoL7XnQpMiaFIETYqgSRE0KYImRdCkCJoUQZPiTaG91J4LTYqgSRE0KYImRdCkCJoUQZMiaFIETYo3hfZSey40KYImRdCkCJoUQZMiaFIETYqgSRE0Kd4U2kvtudCkCJoUQZMiaFIETYqgSRE0KYImRdCkeFNoL7XnQpMiaFIETYqgSRE0KYImRdCkCJoUQZPiTaG91J4LTYqgSRE0KYImRdCkCJoUQZMiaFIETcoHt3taUX1tnMMAAAAASUVORK5CYII='); }
		.no-category { display: block; width: 70px; height: 30px; background: #ccc; margin: 0 auto; }
		.no-svg { display: block; width: 70px; height: 30px; background: #ccc; margin: 0 auto; }
		.svg-not-found { display: block; width: 50px; height: 60px; background: pink; border: 3px solid red; margin: 0 auto; }

		#dialog {
			display:none;
			position: absolute;
			width: 200px;
			height: 130px;
			background: #FFF;
			border: 2px solid #eee;
			padding: 20px;
			top:0;

			-webkit-box-shadow: 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
			-moz-box-shadow: 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
			box-shadow: 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
		}
		#dialog input { display: block; border: 1px solid #aaa;  margin-bottom: 20px; width: 200px; line-height: 2em; }
		#dialog a { cursor: pointer; display: block; padding: 10px; background: #eee; width: 40px; margin: 0 auto; }
		table input { border: 1px solid #aaa; width: 120px; line-height: 2em; }
	</style>
	<?php /*<base href="<?php echo $domain?>" >*/ ?>
</head>
<body>
<table>
	<thead>
	<?php echo $subHeaders ?>
	</thead>
	<tbody>
	<?php foreach($rows as $r => $row) { ?>
		<tr>
			<td><?php echo ($r+1) ?></td>
			<?php foreach($columns as $column)
				echo '<td class="' . strtolower(preg_replace('/[\:\.]/', ' ', $column)) . '">', (isset($row[$column]) ? $row[$column] : ""), '</td>';
			?>
		</tr>
		<?php if ($r > 0 && 0 == (($r+1) % 10)) echo $subHeaders; ?>
	<?php } ?>
	<?php echo $subHeaders; ?>
	</tbody>
</table>

<div id="dialog">
	<input type="text" name="abs" />
	<input type="text" name="rel" />
	<a>close</a>
</div>

<?php if ($warnings) {?>
	<h2>Обнаружены следующие проблемы</h2>
	<ul>
		<li><?php echo implode("</li><li>", $warnings) ?></li>
	</ul>
<?php } ?>

<?php if ($absentSvgOrig) {?>
	<hr/>
	<h2>Не обнаружены следующие оригинальные изображения SVG, но найдены другие типы файлов</h2>
	<ol>
		<?php
		foreach($absentSvgOrig as $problem)
		{
			list($iconName, $iconFiles) = $problem;
			$files = iconFiles($iconFiles);
			echo '<li>', $iconName, '<ul><li>', implode("</li><li>", $files) ,'</li></ul></li>';
		}
		?>
	</ol>
<?php } ?>

<script type="application/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="application/javascript">
$(function(){
	var $d = $('#dialog'),
		$dia = $d.find('input[name=abs]'),
		$dir = $d.find('input[name=rel]');
	$d.find('a').click(function(){$d.fadeOut()});

	$('td.file').each(function(i,e){
		var $e = $(e),
			v = $e.text(),
			$i = $('<input type="text">');
		$e.html("").append($i.val(v));
	});
	$('td.category:not(:has(.no-category))').each(function(i,e){
		var $e = $(e),
			v = $e.text(),
			$i = $('<input type="text">');
		$e.html("").append($i.val(v));
	});
	$('img').each(function(i,e){
		var $e = $(e),
			_rel = $e.data('srcrel'),
			_abs = $e.data('srcabs');
		if (!_rel)
			return;
		$e.click(function(event){
			$d.fadeIn();
			$dia.val(_abs);
			$dir.val(_rel);
			$d.css({left: event.pageX-200, top: event.pageY});
		});
	});
	$('input').on('click',function(event){
		event.stopPropagation();
		event.preventDefault();
		$(this).select();
		return false;
	});
	$("body").keydown(function(event) {
		if(event.keyCode == 27) // ESC
			$d.fadeOut();
	});
});
</script>
</body>
</html>
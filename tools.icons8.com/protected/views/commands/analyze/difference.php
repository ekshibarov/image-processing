<?php
/**
 * @var array $columns
 * @var array $rows
 * @var array $warnings
 * @var array $fails
 * @var array $different
 * @var array $unsimplified
 * @var array $absentSvgOrig
 * @var array $absentSvgSimp
 * @var array $brokenViewBox
 * @var array $brokenSimViewBox
 * @var array $brokenBoundingBox
*/

$subHeaders = '';
foreach($columns as $column)
	$subHeaders .= '<th class="' . preg_replace('/\:|\./', '_', $column) . '">' . preg_replace('/\:/', '<br/>', $column) . '</th>';
$subHeaders = '<tr>' . $subHeaders . '</tr>';

function iconFiles($iconFiles)
{
	$ret = [];
	foreach($iconFiles as $type => $listByType)
	{
		if ('png' == $type)
		{
			foreach($listByType as $size => $files){
				$ret = array_merge($ret, array_keys($files));
			}
		} else
		{
			$ret = array_merge($ret, array_keys($listByType));
		}
	}
	$ret = array_unique($ret);
	return $ret;
}

function formatBoundingValue($value1, $value2) // форматирует значение $value1 - $value2
{
    return floor(((float)$value1 - (float)$value2)*10000)/10000;
}

function getAlertClassByValue($value)
{
    if($value > 100)
        return 'alertBounding1';
    elseif($value > 10)
        return 'alertBounding2';
    elseif($value > 1)
        return 'alertBounding3';
    elseif($value > 0.1)
        return 'alertBounding4';
    else
        return 'alertBounding5';
}
?>
<html>
<head>
    <meta charset="utf-8">
	<title>SVG difference report</title>
	<style>
		table { width: 80%; text-align: center; border-collapse: collapse; border-spacing: 0; font-family: Arial, "Helvetica Neue", Helvetica, sans-serif}
		th { background: #7C7878; color: #fff; font-weight: normal; text-align: center; }
		th, td { border: 1px solid #ccc; padding: 2px; }
		img { margin: 5px; }
		th.strokes, td.strokes { background: lightpink; }
		th.figures, td.figures { background: lightgreen; } /* Паша, а можно я тут поменяла оформление немного? */
		td.png_100 img, td.svg img, td.svg_simp img, td.svg_simp_old img, td.svg_cross img, td.svg_cross_old img {  border: 3px solid #FFFFFF;background-color: #DA9D9D;-webkit-box-shadow: 1px 1px 2px 2px rgba(0,0,0,0.4);-moz-box-shadow: 1px 1px 2px 2px rgba(0,0,0,0.4);box-shadow: 1px 1px 2px 2px rgba(0,0,0,0.4);}
		td.svg, td.svg_simp, td.svg_simp_old, td.svg_cross, td.svg_cross_old, td.png_100, td.checkerboard { background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAB4CAYAAABb59j9AAAABmJLR0QA/wD/AP+gvaeTAAABnUlEQVR4nO3bsQ0DIRQFQc5yR/TfAdSEWyCxZK9m4tOLVj858ay1zrgw57z5bOy9r76zZ+8be6+rr+BPCJoUQZMiaFIETYqgSRE0KYImRdCkPOecqz+Fv/6HyJ69MVxoYgRNiqBJETQpgiZF0KQImhRBkyJoUt6VP0T27I3hQhMjaFIETYqgSRE0KYImRdCkCJoUQZPiTaG91J4LTYqgSRE0KYImRdCkCJoUQZMiaFIETYo3hfZSey40KYImRdCkCJoUQZMiaFIETYqgSRE0Kd4U2kvtudCkCJoUQZMiaFIETYqgSRE0KYImRdCkeFNoL7XnQpMiaFIETYqgSRE0KYImRdCkCJoUQZPiTaG91J4LTYqgSRE0KYImRdCkCJoUQZMiaFIETYo3hfZSey40KYImRdCkCJoUQZMiaFIETYqgSRE0Kd4U2kvtudCkCJoUQZMiaFIETYqgSRE0KYImRdCkeFNoL7XnQpMiaFIETYqgSRE0KYImRdCkCJoUQZPiTaG91J4LTYqgSRE0KYImRdCkCJoUQZMiaFIETcoHt3taUX1tnMMAAAAASUVORK5CYII='); }

        li.alertBounding1 {background-color: #E80B0B; color:#ffffff;}
        li.alertBounding1 a {color: #afeaef;}
        li.alertBounding2 {background-color: #FD9E9E}
        li.alertBounding3 {background-color: #FCCACA}
        li.alertBounding4 {background-color: #FFEAEA}
        li.bounding span {font-weight: bold;}
        li.bounding {border-bottom: 1px solid white;}
        li.bounding:hover {border-bottom: 1px dashed #484848;}
        li.bounding img {position: absolute; right: 20px; margin-top: -81px; border: 1px dashed; display: none; background-color: white;}
        li.bounding:hover img {display: block}

		.no-category { display: block; width: 70px; height: 30px; background: #ccc; margin: 0 auto; }
		.no-svg { display: block; width: 70px; height: 30px; background: #ccc; margin: 0 auto; }
		.svg-not-found, .wrong-svg {display: block; width: 50px; background: rgb(255, 244, 246); border: 3px solid red; margin: 0 auto; padding: 5px; color: red; font-weight: bold;}
		/*.wrong-svg { display: block; width: 50px; height: 60px; background: pink; border: 3px solid red; margin: 0 auto; }*/

		#dialog {
			display:none;
			position: absolute;
			width: 200px;
			height: 130px;
			background: #FFF;
			border: 2px solid #eee;
			padding: 20px;
			top:0;

			-webkit-box-shadow: 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
			-moz-box-shadow: 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
			box-shadow: 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
		}
		#dialog input { display: block; border: 1px solid #aaa;  margin-bottom: 20px; width: 200px; line-height: 2em; }
		#dialog a { cursor: pointer; display: block; padding: 10px; background: #eee; width: 40px; margin: 0 auto; }
		table input { border: 1px solid #aaa; width: 120px; line-height: 2em; }
	</style>
	<?php /*<base href="<?php echo $domain?>" >*/ ?>
</head>
<body>
<h1>SVG Report by <?=date('d.m.Y h:m'); ?></h1>
<table>
	<thead>
	<?php echo $subHeaders ?>
	</thead>
	<tbody>
	<?php foreach($rows as $r => $row) { ?>
		<tr>
			<?php foreach($columns as $column)
				echo '<td class="' . preg_replace('/\:|\./', '_', $column) . '">', (isset($row[$column]) ? $row[$column] : ""), '</td>';
			?>
		</tr>
		<?php if ($r > 0 && 0 == ($r % 10)) echo $subHeaders; ?>
	<?php } ?>
	<?php echo $subHeaders; ?>
	</tbody>
</table>
<?php if ($fails) {?>
	<hr/>
	<h2>Обнаружены следующие проблемы</h2>
	<ol>
		<li><?php echo implode("</li><li>", $fails) ?></li>
	</ol>
<?php } ?>

<?php if ($warnings) {?>
	<h2>Обнаружены следующие проблемы</h2>
	<ul>
		<li><?php echo implode("</li><li>", $warnings) ?></li>
	</ul>
<?php } ?>

<?php if ($absentSvgOrig) {?>
	<hr/>
	<h2>Не обнаружены следующие оригинальные изображения SVG, но найдены другие типы файлов</h2>
	<ol>
		<?php
		foreach($absentSvgOrig as $problem)
		{
			list($iconName, $iconFiles) = $problem;
			$files = iconFiles($iconFiles);
			echo '<li>', $iconName, '<ul><li>', implode("</li><li>", $files) ,'</li></ul></li>';
		}
		?>
	</ol>
<?php } ?>

<?php if ($absentSvgSimp) {?>
	<hr/>
	<h2>Не обнаружены следующие упрощённые изображения SVG</h2>
	<ol>
		<?php
		foreach($absentSvgSimp as $problem)
		{
			list($iconKey, $svgRelPath) = $problem;
			echo '<li><a href="#', $iconKey, '">', $svgRelPath, '</a></li>';
		}
		?>
	</ol>
<?php } ?>

<?php if ($different) {?>
	<hr/>
	<h2>Обнаружены следующие различающиеся изображения SVG</h2>
	<ol>
		<?php
		foreach($different as $problem)
		{
			list($iconKey, $svgRelPath, $metrik) = $problem;
			echo '<li><a href="#', $iconKey, '">', $svgRelPath, '</a> ', $metrik, '</li>';
		}
		?>
	</ol>
<?php } ?>

<?php if ($unsimplified) {?>
	<hr/>
	<h2>Обнаружены следующие недостаточно упрощённые изображения SVG</h2>
	<ol>
		<?php
		foreach($unsimplified as $problem)
		{
			list($iconKey, $svgRelPath) = $problem;
			echo '<li><a href="#', $iconKey, '">', $svgRelPath, '</a></li>';
		}
		?>
	</ol>
<?php } ?>

<?php if ($brokenViewBox) {?>
	<hr/>
	<h2>Обнаружены следующие изображения SVG c неверными значениями viewBox</h2>
	<ol>
		<?php
		foreach($brokenViewBox as $problem)
		{
            list($iconKey, $svgRelPath, $viewBox, $platformStandardViewBox) = $problem;
            echo '<li><a href="#', $iconKey, '">', $svgRelPath, '</a> (viewBox="' . implode(' ', $viewBox) . '"
			вместо "0 0 ' . $platformStandardViewBox . ' ' . $platformStandardViewBox .'")</li>';
		}
		?>
	</ol>
<?php } ?>

<?php if ($brokenSimViewBox) {?>
	<hr/>
	<h2>Обнаружены следующие упрощенные изображения SVG c неверными значениями viewBox</h2>
	<ol>
		<?php
		foreach($brokenSimViewBox as $problem)
		{
			list($iconKey, $svgRelPath, $viewBox, $platformStandardViewBox) = $problem;
			echo '<li><a href="#', $iconKey, '">', $svgRelPath, '</a> (viewBox="' . implode(' ', $viewBox) . '"
			вместо "0 0 ' . $platformStandardViewBox . ' ' . $platformStandardViewBox .'")</li>';
		}
		?>
	</ol>
<?php } ?>

<?php if ($brokenBoundingBox) {?>
	<hr/>
	<h2>Обнаружены следующие изображения SVG, вышедшие за пределы viewBox</h2>
	<ol>
		<?php
		foreach($brokenBoundingBox as $problem)
		{
			list($iconKey, $svgRelPath, $boundingBox, $viewBox) = $problem;
            if(isset($boundingBox['startX']) &&
                isset($boundingBox['startY']) &&
                isset($boundingBox['width']) &&
                isset($boundingBox['height']))
            {
                $boundingProblemString = '';
                $boundingValues = [];
                if(((float)$viewBox['startX'] - (float)$boundingBox['startX']) > AnalyzeCommand::BOUNDING_BOX_CHECK_THRESHOLD)
                {
                    $value = formatBoundingValue($viewBox['startX'], $boundingBox['startX']);
                    $boundingProblemString .= 'слева на <span>' . $value . '</span>; ';
                    $boundingValues[] = $value;
                }
                if(((float)$viewBox['startY'] - (float)$boundingBox['startY']) > AnalyzeCommand::BOUNDING_BOX_CHECK_THRESHOLD)
                {
                    $value = formatBoundingValue($viewBox['startY'], $boundingBox['startY']);
                    $boundingProblemString .= 'сверху на <span>' . $value . '</span>; ';
                    $boundingValues[] = $value;
                }
                if(((float)$boundingBox['width'] - (float)$viewBox['width']) > AnalyzeCommand::BOUNDING_BOX_CHECK_THRESHOLD)
                {
                    $value = formatBoundingValue($boundingBox['width'], $viewBox['width']);
                    $boundingProblemString .= 'справа на <span>' . $value . '</span>; ';
                    $boundingValues[] = $value;
                }
                if(((float)$boundingBox['height'] - (float)$viewBox['height']) > AnalyzeCommand::BOUNDING_BOX_CHECK_THRESHOLD)
                {
                    $value = formatBoundingValue($boundingBox['height'], $viewBox['height']);
                    $boundingProblemString .= 'снизу на <span>' . $value . '</span>; ';
                    $boundingValues[] = $value;
                }

                echo '<li class="bounding ' . getAlertClassByValue(max($boundingValues)) . '"><a href="#', $iconKey, '">',
                $svgRelPath, '</a> - превышает viewBox ' . $boundingProblemString . '<img src="http://st.icons8.com/' . $svgRelPath . '" height="80" width="80"></li>';
            }
            else
                echo '<li><a>', $svgRelPath, '</a> - не определен boundingBox, т.к.
                иконка недостаточно упрощена</li>';
        }
		?>
	</ol>

<?php } ?>

<div id="dialog">
	<input type="text" name="abs" />
	<input type="text" name="rel" />
	<a>close</a>
</div>

<script type="application/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="application/javascript">
$(function(){
	var $d = $('#dialog'),
		$dia = $d.find('input[name=abs]'),
		$dir = $d.find('input[name=rel]');
	$d.find('a').click(function(){$d.fadeOut()});

	/*
	$('td.file').each(function(i,e){
		var $e = $(e),
			v = $e.text(),
			$i = $('<input type="text">');
		$e.html("").append($i.val(v));
	});
	*/
	/*
	$('td.category:not(:has(.no-category))').each(function(i,e){
		var $e = $(e),
			v = $e.text(),
			$i = $('<input type="text">');
		$e.html("").append($i.val(v));
	});
	*/
	$('img').each(function(i,e){
		var $e = $(e),
			_rel = $e.data('srcrel'),
			_abs = $e.data('srcabs');
		if (!_rel)
			return;
		$e.click(function(event){
			$d.fadeIn();
			$dia.val(_abs);
			$dir.val(_rel);
			$d.css({left: event.pageX-200, top: event.pageY});
		});
	});
	/*
	$('input').on('click',function(event){
		event.stopPropagation();
		event.preventDefault();
		$(this).select();
		return false;
	});
	*/
	$("body").keydown(function(event) {
		if(event.keyCode == 27) // ESC
			$d.fadeOut();
	});
});
</script>
</body>
</html>
<?php

class QueueTaskCommand extends \CConsoleCommand
{

    public $config;

    public $storagePath;

    public $backupPath;

    public $debug = false;

    const TOOLS_AUTH_HASH = 'tools';

    /* Получаем данные из stdin */
    public $stdInContent;

    public function run($args)
    {
        if(!function_exists('posix_isatty'))
        {
            Yii::log('posix_isatty is not defined', CLogger::LEVEL_ERROR, 'queue_task');
            return false;
        }
        if(!posix_isatty(STDIN))
            $this->stdInContent = file_get_contents("php://stdin");
        return parent::run($args);
    }

    public function init()
    {
        parent::init();
        $this->config = \Yii::app()->params;
        $this->storagePath = rtrim($this->config['storage_path'], '/') . '/';
        $this->backupPath = rtrim($this->config['backup_path'], '/') . '/' . date('Ymd') . '/' . date('H') . '/';
    }

    public function actionDrawEps($debug = false)
    {
        $this->debug = $debug;
        if (false !== ($queueMessageJsonContent = base64_decode($this->stdInContent))) {
            if (false !== $queueMessage = json_decode($queueMessageJsonContent, true)) {
                $context = isset($queueMessage['context']) ? $queueMessage['context'] : null;
                $jobId = isset($queueMessage['queue_job_id']) ? $queueMessage['queue_job_id'] : null;
                $size = isset($queueMessage['size']) && !empty($queueMessage['size']) ? $queueMessage['size'] : null;
                $svg = $queueMessage['svg'];
                try {
                    if(isset($queueMessage['svg']))
                    {
                        $generator = new \icons8_image_processing\MultiGenerator;
                        $generator->debug = $debug;
                        $files = null;
                        $files = $generator->convertFromSvg($svg, $size, [\icons8_image_processing\MultiGenerator::FORMAT_EPS]);
                        $result = [
                            'job_id' => $jobId,
                            'files' => $files
                        ];
                        $encodedResult = base64_encode(json_encode($result));
                        $amqpComponent = new AMQPComponent();
                        $amqpComponent->context = 'manually added task by console command by ' . date('Y-m-d H:i:s');
                        if ($debug) {
                            $this->log("Send converted images to queue for receive eps");
                        }
                        $amqpComponent->sendReceiveEpsTask($encodedResult);
                    }
                } catch(\Exception $e)
                {
                    $message = "Error during generating {$queueMessage['icon_name']} icon images in queue: {$e->getMessage()}";
                    \Yii::log($message, \CLogger::LEVEL_WARNING, 'queue_task.draw_eps');
                    $this->queueLog("FAIL icon images not generated for job {$jobId}. Message:  " . $e->getMessage() . '. Task: ' . var_export($queueMessage, true), $context);
                }
            }
        }
        exit(0);
    }

    protected function log($message, $level = \CLogger::LEVEL_INFO)
    {
        if ($this->debug)
            echo $message, PHP_EOL;
        \Yii::log($message, $level, 'queue_task');
    }

    protected function queueLog($message, $context = null, $level = CLogger::LEVEL_INFO)
    {
        $logMessage = date('Y-m-d H:i:s') . ' ' . $message;
        if(isset($context))
            $logMessage .= ' (context: ' . $context . ')';
        Yii::log($logMessage, $level, 'queue');
    }

    protected function timeStr($time)
    {
        return number_format($time, 4, '.', '');
    }
}

<?php

namespace icons8_reports;

use \icons8_storage_tools\ManySvgImages as ManySvgImages;
use \icons8_storage_tools\Indexer as Indexer;

class IndexProblemsReport
{
    /**
     * @var Indexer
     */
    protected $_indexer;

    /**
     * @var ManySvgImages
     */
    protected $_manySvgImagesAdviser;

    function __construct(Indexer $indexer)
    {
        $this->_indexer = $indexer;
        $this->_manySvgImagesAdviser = new ManySvgImages($indexer);
    }

    public function hasProblems()
    {
        $warnings = $this->_indexer->getWarnings();
        $advises = $this->_manySvgImagesAdviser->getAdviceList();
        return !empty($warnings) || !empty($advises);
    }

    public function report()
    {
        $warnings = $this->_indexer->getWarnings();
        $advises = $this->_manySvgImagesAdviser->getAdviceList();
        $report = $this->renderFile(APP_PATH.'/protected/views/reports/index-problems.php', [
            'indexingWarnings' => $warnings,
            'manySvgImages' => $advises,
        ]);

        $reportName = 'index-problems-'.date('Ymd-His').'.html';
        $reportAbsFile = APP_PATH . '/www/static/'.$reportName;
        $reportAbsUrl = \Yii::app()->params['app_url'] . '/static/' . $reportName;
        file_put_contents($reportAbsFile, $report);

        \Yii::log('The report about unfixable indexing problems is available at ' . $reportAbsUrl, \CLogger::LEVEL_WARNING);
        echo "\n Many Svg with same names. Report is available at $reportAbsUrl \n";
    }

    /**
     * Renders a view file.
     * @param string $_viewFile_ view file path
     * @param array $_data_ optional data to be extracted as local view variables
     * @return mixed the rendering result if required. Null otherwise.
     */
    public function renderFile($_viewFile_, $_data_ = null)
    {
        if(is_array($_data_))
            extract($_data_, EXTR_PREFIX_SAME, 'data');
        else
            $data = $_data_;
        ob_start();
        ob_implicit_flush(false);
        require($_viewFile_);
        return ob_get_clean();
    }
}
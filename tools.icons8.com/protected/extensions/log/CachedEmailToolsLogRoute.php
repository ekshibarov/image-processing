<?php

/**
 * Created by PhpStorm.
 * User: imhelle
 * Date: 17.04.2016
 * Time: 21:00
 */
class CachedEmailToolsLogRoute extends CEmailLogRoute
{
    public $resendingDelay = 3600;

    protected function processLogs($logs)
    {
        $message = $messageText = '';
        foreach($logs as $log)
        {
            $message.=$this->formatLogMessage($log[0],$log[1],$log[2],$log[3]);
            $messageText .= $log[0] . $log[1] . $log[2];
        }

        if($this->checkLogsCache($messageText, $message))
        {
            $message=wordwrap($message,70);
            $subject=$this->getSubject();
            if($subject===null)
                $subject=Yii::t('yii','Application Log');
            foreach($this->getEmails() as $email)
                $this->sendEmail($email,$subject,$message);
        }
    }


    /*
     * messages cache
     *  'log_64a3f8745c0dc6a1d1fe83349452829a' =>
     *      [
     *          'first_time' => '1422921651',
     *          'last_time' => '1460949474',
     *          'count' => '15'
     *      ]
     *
     */
    protected function checkLogsCache($messageText, &$message)
    {
        $sendMessage = false;

        $messageHash = md5($messageText);
        $now = time();

        $logCache = Yii::app()->cache->get('log_' . $messageHash);
        if(!$logCache) {
            $sendMessage = true;
            $messagesCount = 0;
            $firstMessageTime = $now;
        } else
        {
            $messagesCount = isset($logCache['count']) ? $logCache['count'] : 0;
            $firstMessageTime = isset($logCache['first_time']) ? $logCache['first_time'] : $now;
            $lastMessageTime = isset($logCache['last_time']) ? $logCache['last_time'] : $now;
            if(($lastMessageTime - $now) > $this->resendingDelay)
                $sendMessage = true;
        }
        $messagesCount++;

        $logCache = [
            'first_time' => $firstMessageTime,
            'last_time' => $now,
            'count' => $messagesCount
        ];

        Yii::app()->cache->set('log_' . $messageHash, $logCache);

        $message .= ' (' . $messagesCount . ' messages received)';

        return $sendMessage;
    }

}
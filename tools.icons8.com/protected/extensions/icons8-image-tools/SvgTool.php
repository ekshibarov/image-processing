<?php

namespace icons8_image_tools;

class SvgTool extends AbstractTool
{
	public function __construct($flags = array(), $binaries = array())
	{
		$defaultsFlags = array(
			'verbose' => 0,
			'background-color' => '',
			'transparent' => true,
		);

		$requiredBinaries = array(
			'inkscape' => false,
			'python' => false,
			'java' => false,
			'apache_fop' => false,
			'ps2eps' => false,
			'eps2eps' => false,
			'compare' => false,
		);

		$flags = array_merge($defaultsFlags, $flags);
		$binaries = array_merge($requiredBinaries, $binaries);

		$flags['verbose'] = intval($flags['verbose']);
		$flags['transparent'] = !!$flags['transparent'];

		$this->setFlags($flags);
		$this->setBinaries($binaries);
	}

	/**
	 * Export PNG image from SVG
	 *
	 * @param $svgScrFilepath
	 * @param $pngDstFilepath
	 * @param $size
	 * @return bool
	 */
	public function exportPng($svgScrFilepath, $pngDstFilepath, $size)
	{
		if ($this->_binaries['inkscape'])
		{
			$bin = $this->_binaries['inkscape'];
			$params = array(
				'-z', //'--without-gui',
				//'--export-area-page',
				'-h '.$size, //'--export-height' => $size,
				'-w '.$size. //'--export-width' => $size,
				'-f '.$svgScrFilepath, //'--file' => $svgScrFilepath,
				'-e '.$pngDstFilepath, // --export-png' => $pngDstFilepath,
			);
			if ($this->_flags['background-color'])
			{
				//$params[] = '--export-background='.$this->_flags['background-color'];
				//$params[] = '--export-background-opacity='.$this->_flags['transparent'] ? "0.0" : "1.0";
			}
			#$params[] = '--query-all';
		} elseif($this->_binaries['convert'])
		{
			$bin = $this->_binaries['convert'];
			$params = array(
				'-limit memory 32MiB',
				'-limit map 64MiB',
				'-background transparent',
				'-density 2000', // для чёткости
				"-resize {$size}x",
				"-extent {$size}x{$size}",
				"-gravity center",
				"'{$svgScrFilepath}'",
				"'{$pngDstFilepath}'",
			);
		} else
		{
			\Yii::log('Не настроены средства вывода изображений (inkscape или convert)', \CLogger::LEVEL_WARNING);
			return false;
		}

		\Yii::log('Export PNG from "' .  $svgScrFilepath . '" to "' . $pngDstFilepath . '" with size ' . $size . ' using ' . $bin, \CLogger::LEVEL_INFO);
		\FileSystemHelper::mkdir(dirname($pngDstFilepath));
		$result = $this->run($bin, $params, $output);

		if (!is_file($pngDstFilepath))
		{
			\Yii::log('Файл с изображением PNG "' . $pngDstFilepath . '" не найден ' . var_export($output, true), \CLogger::LEVEL_WARNING);
			return false;
		}
		return $result;
	}

	/**
	 * Export SVG image to PDF
	 *
	 * @param $svgScrFilepath
	 * @param $pdfDstFilepath
	 * @return bool
	 */
	public function exportPdf($svgScrFilepath, $pdfDstFilepath)
	{
		if (!$this->_binaries['cairosvg'])
		{
			\Yii::log('Не настроен cairosvg', \CLogger::LEVEL_WARNING);
			return false;
		}

		\FileSystemHelper::mkdir(dirname($pdfDstFilepath));

		\Yii::log('Export PDF from "' .  $svgScrFilepath . '" to "' . $pdfDstFilepath . '"', \CLogger::LEVEL_INFO);

		$params = array(
			$svgScrFilepath,
			'-f PDF',
			'-o '.$pdfDstFilepath,
		);
		$result = $this->run($this->_binaries['cairosvg'], $params, $output);
		if (!is_file($pdfDstFilepath))
		{
			\Yii::log('Файл с изображением PDF "' . $pdfDstFilepath . '" не найден', \CLogger::LEVEL_WARNING);
			return false;
		}
		return $result;
	}

	/**
	 * Export PNG image from SVG
	 *
	 * @param $svgScrFilepath
	 * @param $epsDstFilepath
	 * @param $size
	 * @return bool
	 */
	public function exportEps($svgScrFilepath, $epsDstFilepath, $size)
	{
		if (!$this->_binaries['java'])
		{
			\Yii::log('Не настроен java', \CLogger::LEVEL_WARNING);
			return false;
		}

		if (!$this->_binaries['apache_fop'])
		{
			\Yii::log('Не настроен Apache FOP', \CLogger::LEVEL_WARNING);
			return false;
		}

		if (!$this->_binaries['ps2eps'])
		{
			\Yii::log('Не настроен PS2EPS', \CLogger::LEVEL_WARNING);
			return false;
		}

		if (!$this->_binaries['eps2eps'])
		{
			\Yii::log('Не настроен EPS2EPS', \CLogger::LEVEL_WARNING);
			return false;
		}

		\FileSystemHelper::mkdir(dirname($epsDstFilepath));

		\Yii::log('Export EPS from "' .  $svgScrFilepath . '" to "' . $epsDstFilepath . '"', \CLogger::LEVEL_INFO);

		# выправление масштаба и границ SVG
		$svgTmpSrcFilepath = $epsDstFilepath.'.tmp.svg';
		if (!$this->fixSvgBoundingBox($svgScrFilepath, $svgTmpSrcFilepath, $size))
		{
			return false;
		}

		# генерация SVG -> PS
		$psTmpSrcFilepath = $epsDstFilepath.'.tmp.ps';
		$epsTmpSrcFilepath = $epsDstFilepath.'.tmp.eps';
		$params = array(
			$this->_binaries['java'],
			$svgTmpSrcFilepath,
			$psTmpSrcFilepath,
			'2>&1',
		);
		$result = $this->run($this->_binaries['apache_fop'], $params, $output);
		if (!is_file($psTmpSrcFilepath))
		{
			\Yii::log('Файл с изображением PS "' . $psTmpSrcFilepath . '" не найден', \CLogger::LEVEL_WARNING);
			return false;
		}

		# преобразорвание PS -> EPS
		$params = array(
			'--force',
			$psTmpSrcFilepath,
			$epsTmpSrcFilepath,
			'2>&1',
		);
		$result = $this->run($this->_binaries['ps2eps'], $params, $output);
		if (!is_file($epsTmpSrcFilepath))
		{
			\Yii::log('Файл с изображением EPS "' . $epsTmpSrcFilepath . '" не найден', \CLogger::LEVEL_WARNING);
			return false;
		}

		# исправление границ EPS
		if (!$this->fixEpsBoundingBox($epsTmpSrcFilepath, $size))
		{
			\Yii::log('Не удалось найти и исправить границы файла EPS "' . $epsTmpSrcFilepath . '"', \CLogger::LEVEL_WARNING);
			# ?
		}

		# сжатие EPS
		$epsTmpTmpSrcFilepath = $epsDstFilepath.'.tmp.opt.eps';
		$params = array(
			'--force',
			$epsTmpSrcFilepath,
			$epsTmpTmpSrcFilepath,
			'2>&1',
		);
		$result = $this->run($this->_binaries['eps2eps'], $params, $output);
		if (!is_file($epsTmpTmpSrcFilepath))
		{
			\Yii::log('Файл с изображением EPS "' . $epsTmpTmpSrcFilepath . '" не найден', \CLogger::LEVEL_WARNING);
			return false;
		}

		# ещё раз исправление границ EPS
		if (!$this->fixEpsBoundingBox($epsTmpTmpSrcFilepath, $size))
		{
			\Yii::log('Не удалось найти и исправить границы файла EPS "' . $epsTmpSrcFilepath . '"', \CLogger::LEVEL_WARNING);
			# ?
		}

		\FileSystemHelper::mv($epsTmpTmpSrcFilepath, $epsDstFilepath);
		return $result;
	}

	public function compareSvgFiles($firstSvg, $secondSvg, $size=100, $tempPath, $differencePng = null)
	{
		$firstPng = $tempPath.'/first_svg.png';
		$secondPng = $tempPath.'/second_svg.png';
		if (!$differencePng)
			$differencePng = $tempPath . '/difference.png';
		\FileSystemHelper::rm_rf($differencePng);

		if(!$this->_binaries['compare'])
		{
			\Yii::log('Не настроены средства вывода изображений (compare)', \CLogger::LEVEL_WARNING);
			return false;
		}
		if (!$this->exportPng($firstSvg, $firstPng, $size) || !$this->exportPng($secondSvg, $secondPng, $size))
		{
			return false;
		}

		$bin = $this->_binaries['compare'];
		$params = array(
			'-metric AE -fuzz 32000', // методы @see http://www.imagemagick.org/script/command-line-options.php#metric
			"'{$firstPng}'",
			"'{$secondPng}'",
			"'{$differencePng}'",
			'2>&1',
		);

		\Yii::log('Compare PNG from "' .  $firstSvg . '" to "' . $secondSvg . '" with size ' . $size . ' using ' . $bin, \CLogger::LEVEL_INFO);
		$this->run($bin, $params, $output);

		$result = reset($output);
		if (false!==strpos($result, 'images too dissimilar')) // compare: images too dissimilar `/tmp/changer/svg551660f79b816//first_svg.png' @ compare.c/CompareImageCommand/910.
		{
			return 'images too dissimilar'; // самый типовой текст ошибки, если не удалось сравнить картинки
		} elseif (false!==strpos($result, '@ compare.c')) // compare: бла-бла-бла `/path/to/file` @ compare.c/CompareImageCommand/910.
		{
			return trim(preg_replace('/compare\:(.+?)`/', '$1', $result)); // -> "бла-бла-бла"
		}

		if (!is_file($differencePng))
		{
			\Yii::log('Файл с вычисленной разницей изображений "' . $differencePng . '" не найден', \CLogger::LEVEL_INFO);
			return false; // no difference image found
		}
		return $result;
	}

	public function fixSvgBoundingBox($svgScrFilepath, $svgTmpSrcFilepath, $size)
	{
		\Yii::log('Read "' .  $svgScrFilepath . '"', \CLogger::LEVEL_INFO);

		try {
			$xmlDoc = $this->loadXml(file_get_contents($svgScrFilepath));
		} catch(\DOMException $e)
		{
			\Yii::log('Не удалось открыть XML файл "' . $svgScrFilepath . '": ' . $e->getMessage(), \CLogger::LEVEL_WARNING);
			return false;
		}

		\Yii::log('Add <svg ... viewBox="0 0 ' . $size . ' ' . $size . '">', \CLogger::LEVEL_INFO);
		/** @var \DOMNode $svgNode */
		$svgNode = $xmlDoc->getElementsByTagName('svg')->item(0);
		$this->setAttribute($svgNode, 'viewBox', "0 0 {$size} {$size}");
		$this->setAttribute($svgNode, 'enable-background', "new 0 0 {$size} {$size}");
		#$this->setAttribute($svgNode, 'preserveAspectRatio', 'none');
		$this->setAttribute($svgNode, 'x', '0px');
		$this->setAttribute($svgNode, 'y', '0px');
		$this->setAttribute($svgNode, 'width', $size.'px');
		$this->setAttribute($svgNode, 'height', $size.'px');

		\Yii::log('Add bounding rect <rect style="fill:none;" height="'.$size.'" width="'.$size.'">', \CLogger::LEVEL_INFO);
		$boundingRectNode = null;
		/*
		foreach($xmlDoc->getElementsByTagName('rect') as $rectNode)
		{
			/** @var \DOMNode $width * /
			if ('none' == $this->getAttribute($rectNode, 'fill')) {
				$boundingRectNode = $rectNode;
				break;
			} elseif (preg_match('/[\s;]*fill\s*\:\s*none[\s;]* /', $this->getAttribute($rectNode, 'style'))) {
				$boundingRectNode = $rectNode;
				break;
			}
		}
		*/
		# TODO почему то не находит :(
		if (!$boundingRectNode)
		{
			$boundingRectNode = $xmlDoc->createElement('rect');
			$svgNode->insertBefore($boundingRectNode, $svgNode->firstChild);
		}
		$this->removeAttribute($boundingRectNode, 'fill');
		$this->setAttribute($boundingRectNode, 'style', 'fill:none;');
		$this->setAttribute($boundingRectNode, 'x', '0px');
		$this->setAttribute($boundingRectNode, 'y', '0px');
		$this->setAttribute($boundingRectNode, 'height', "{$size}px");
		$this->setAttribute($boundingRectNode, 'width', "{$size}px");

		$xmlDoc->save($svgTmpSrcFilepath);
		if (!is_file($svgTmpSrcFilepath))
		{
			\Yii::log('Файл с исправленным изображением SVG "' . $svgTmpSrcFilepath . '" не найден', \CLogger::LEVEL_WARNING);
			return false;
		}
		return true;
	}

	/**
	 * Исправляет в файле EPS границы %%BoundingBox и %%HiResBoundingBox
	 * @param $filepath
	 * @param $size
	 * @return bool
	 */
	public function fixEpsBoundingBox($filepath, $size)
	{
		$file = file_get_contents($filepath);

		#попытка выправить границы %%BoundingBox: 0 6 50 44
		if (preg_match('/\%\%BoundingBox\: (-?\d+) (-?\d+) (\d+) (\d+)/', $file, $boundingBox))
		{
			$newBoundingBox = "%%BoundingBox: 0 0 {$size} {$size}";
			$file = str_replace($boundingBox[0], $newBoundingBox, $file);
			\Yii::log('Fix ' . $boundingBox[0] . ' -> ' . $newBoundingBox, \CLogger::LEVEL_INFO);
		} else
		{
			return false;
		}

		#попытка выправить границы %%HiResBoundingBox: 0.000000 6.000000 50.000000 44.000000
		if (preg_match('/\%\%HiResBoundingBox\: (-?[0-9]+\.[0-9]{6}) (-?[0-9]+\.[0-9]{6}) ([0-9]+\.[0-9]{6}) ([0-9]+\.[0-9]{6})/', $file, $boundingBox))
		{
			$newBoundingBox = "%%HiResBoundingBox: 0.000000 0.000000 {$size}.000000 {$size}.000000";
			$file = str_replace($boundingBox[0], $newBoundingBox, $file);
			\Yii::log('Fix ' . $boundingBox[0] . ' -> ' . $newBoundingBox, \CLogger::LEVEL_INFO);
		} else {
			return false;
		}

		file_put_contents($filepath, $file);
		return true;
	}

	/**
	 * @param @param \DOMElement|\DOMNode $xmlNode
	 * @param string $attributeName
	 */
	public function getAttribute($xmlNode, $attributeName)
	{
		if ($xmlNode instanceof \DOMElement)
		{
			$xmlNode->hasAttribute($attributeName) ? $xmlNode->getAttribute($attributeName) : null;
		} else
		{
			$attribute = $xmlNode->attributes->getNamedItem($attributeName);
			return !empty($attribute) ? $attribute->value : null;
		}
	}

	/**
	 * @param \DOMElement|\DOMNode $xmlNode
	 * @param string $attributeName
	 */
	public function removeAttribute($xmlNode, $attributeName)
	{
		if ($xmlNode instanceof \DOMElement) {
			$xmlNode->removeAttribute($attributeName);
		} elseif ($attribute = $xmlNode->attributes->getNamedItem($attributeName))
		{
			$xmlNode->removeChild($attribute);
		}
	}

	/**
	 * @param \DOMElement|\DOMNode $xmlNode
	 * @param string $attributeName
	 * @param string $attributeValue
	 */
	public function setAttribute($xmlNode, $attributeName, $attributeValue)
	{
		if ($xmlNode instanceof \DOMElement) {
			$xmlNode->setAttribute($attributeName, $attributeValue);
		} else
		{
			$viewBoxAttribute = $xmlNode->attributes->getNamedItem($attributeName);
			if (empty($viewBoxAttribute))
			{
				$viewBoxAttribute = $xmlNode->createAttribute($attributeName);
				$xmlNode->appendChild($viewBoxAttribute);
			}
			$viewBoxAttribute->value = $attributeValue;
		}
	}

	public function resizeSVG($svgScrFilepath, $dstFilePath, $size)
	{
		\Yii::log('Read "' .  $svgScrFilepath . '"', \CLogger::LEVEL_INFO);
		try {
			$xmlDoc = $this->loadXml(file_get_contents($svgScrFilepath));
		} catch(\DOMException $e)
		{
			\Yii::log('Не удалось открыть XML файл "' . $svgScrFilepath . '": ' . $e->getMessage(), \CLogger::LEVEL_WARNING);
			return false;
		}

		/** @var \DOMNode $svgNode */
		$svgNode = $xmlDoc->getElementsByTagName('svg')->item(0);

		$currentWidth = $this->getAttribute($svgNode, 'width');
		$currentHeight = $this->getAttribute($svgNode, 'height');

		if($currentHeight && $currentWidth)
			if($size.'px' == $currentHeight && $size.'px' == $currentWidth)
				return $svgScrFilepath; // размер и так нормальный, возвращаем исходный файл

		$this->setAttribute($svgNode, 'width', $size.'px');
		$this->setAttribute($svgNode, 'height', $size.'px');

		$svgTmpSrcFilepath = $dstFilePath .'.tmp.rsz.svg';

		$xmlDoc->save($svgTmpSrcFilepath);
		if (!is_file($svgTmpSrcFilepath))
		{
			\Yii::log('Файл с исправленным изображением SVG "' . $svgTmpSrcFilepath . '" не найден', \CLogger::LEVEL_WARNING);
			return false;
		}
		return $svgTmpSrcFilepath;
	}

	/**
	 * @param $xmlString
	 * @return \DOMDocument
	 */
	protected function loadXml($xmlString)
	{
		set_error_handler([$this, 'HandleXmlError']);
		$dom = new \DOMDocument('1.0', 'utf-8');
		$dom->loadXml($xmlString);
		restore_error_handler();
		return $dom;
	}

	public function HandleXmlError($errno, $errstr, $errfile, $errline)
	{
		if ($errno==E_WARNING && (substr_count($errstr,"DOMDocument::loadXML()")>0))
		{
			throw new \DOMException($errstr);
		}
		else
			return false;
	}
}
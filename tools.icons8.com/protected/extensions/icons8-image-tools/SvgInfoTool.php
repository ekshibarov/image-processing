<?php

namespace icons8_image_tools;

/**
 * Class SvgInfoTool
 *
 * Для указанной SVG составляет отчёт о содержимом - какие атрибуты и теги, отдельно по сложным тегам (stroke)
 */
class SvgInfoTool extends AbstractTool
{
	protected $_log = array();

	protected $_figureIdPrefix = 'figure';

	protected $_figureId = 1;

	protected $_alreadyCounted = array();

	protected $_alreadyStrokedCounted = array();

	protected $_info = array(
		'svg:x' => 0,
		'svg:y' => 0,
		'svg:width' => '',
		'svg:height' => '',
		'svg:viewBox' => '',
        'svg:viewBox:coordinates' => [], // startX, startY, endX, endY
		'svg:style' => '',

        'svg:boundingBox:coordinates' => [], // startX, startY, endX, endY

        'colors' => array(),

		'groups' => 0,
		'styles' => 0,

		'figures' => 0,
		'figure.circle' => 0,
		'figure.ellipse' => 0,
		'figure.line' => 0,
		'figure.path' => 0,
		'figure.polyline' => 0,
		'figure.rect' => 0,

		'strokes' => 0,
		'stroke.groups' => 0,
		'stroke.circle' => 0,
		'stroke.ellipse' => 0,
		'stroke.line' => 0,
		'stroke.path' => 0,
		'stroke.polyline' => 0,
		'stroke.rect' => 0,
	);

	public function info($fileAbsPath)
	{
		$xmlDoc = $this->open($fileAbsPath);

		$xpath = new \DOMXpath($xmlDoc);

		// регистрация пространства имён для SVG требуется чтобы внутри XML найти теги path или любые другие
		$context = $xmlDoc->documentElement; // это должен быть тег <svg ...>
		$svgPrefix = 'svg';
		$namespaceURI = $context->namespaceURI; // <svg xmlns="http://www.w3.org/2000/svg" ...>
		$xpath->registerNamespace($svgPrefix, $namespaceURI);
		$this->_log[] = 'registered namespace "' . $svgPrefix . '" = "' . $namespaceURI . '"';

		$info = &$this->_info;

		foreach($context->attributes as $attribute)
			$info['svg:'.$attribute->nodeName] = (string)$attribute->value;

		// все примитивы
        $elements = $xpath->query("//*");
        $paths = [];
        foreach($elements as $element)
        {
            $this->countFigures($element);
            if($element->nodeName=='path')
                $paths[] = $element->attributes->getNamedItem('d')->value;

            $this->findColors($element);
            $attrStroke = $element->attributes->getNamedItem('stroke'); // <line stroke="#333333" ...
            $attrStyle = $element->attributes->getNamedItem('style'); // <line style="fill:#333333;stroke:#333333;stroke-width:2;
            if ($attrStroke && 'none' != $attrStroke->value)
                $this->countStrokedFigures($element);
            elseif ($attrStyle && $this->hasStrokesStyles($attrStyle->value))
                $this->countStrokedFigures($element);
        }
        $elements = $xpath->query("//{$svgPrefix}:style"); // <style xmlns="http://www.w3.org/2000/svg">.st0{fill:none;stroke:#ff00ff;}</style>
		foreach($elements as $element)
		{
			/** @var $styleTextNode \DOMText */
			foreach($element->childNodes as $styleTextNode)
			{
				if ($styleTextNode->nodeType != XML_TEXT_NODE)
					continue;

				$styleTextNode->nodeValue = preg_match_all('/\.([^{]+){([^}]+)}/u', $styleTextNode->textContent, $matches);
				foreach($matches[0] as $i => $cssTerm)
				{
					$selectorClass = trim($matches[1][$i]);
					$cssRules = $matches[2][$i];
					if ($this->hasStrokesStyles($cssRules))
					{
						#echo "//*[@class=\"$selectorClass\"]";
						$selectorElements = $xpath->query("//*[@class=\"$selectorClass\"]");
						/** @var \DOMElement $selectorElement */
						foreach($selectorElements as $selectorElement)
							$this->countStrokedFigures($selectorElement);
					}
				}
			}
		}
        $viewBox = $info['svg:viewBox'];
        $this->setViewBoxCoordinates($viewBox);
        $this->setBoundingBoxCoordinates(file_get_contents($fileAbsPath), 'fullSVG');

        $info['colors'] = implode(',', $info['colors']);

		return $info;
	}

	public function open($fileAbsPath)
	{
		if (!is_file($fileAbsPath)) {
			\Yii::log('SVG Info tool: file "' . $fileAbsPath . '" is absent', \CLogger::LEVEL_ERROR);
			throw new \Exception('SVG file "' . $fileAbsPath . '" is absent');
		}

		/** @var $xmlDoc \DOMDocument */

		$svgContent = file_get_contents($fileAbsPath);
		if ('<svg' == substr($svgContent, 0, 4))
			$svgContent = '<?xml version="1.0" encoding="utf-8"?>' . $svgContent;

		$xmlDoc = new \DOMDocument('1.0', 'utf-8');
		if (false === @$xmlDoc->loadXML($svgContent))
		{
			#\Yii::log('Не удалось открыть файл "' . $fileAbsPath . '" как XML', \CLogger::LEVEL_ERROR);
			throw new \Exception('File "' . $fileAbsPath . '" is not in SVG format');
		}

		$this->_log[] = 'opened XML file "' . $fileAbsPath . '"';

		return $xmlDoc;
	}

	/**
	 * @param \DOMElement $element
	 */
	protected function countFigures($element)
	{
		$this->setElementId($element);
		$id = $element->attributes->getNamedItem('id')->value;

		if (isset($this->_alreadyCounted[$id]))
			return;
		$this->_alreadyCounted[$id] = true;

		$info = &$this->_info;
		switch($element->nodeName)
		{
			case 'svg':
				return;

			case 'style':
				$nodeName = 'styles';
				break;

			case 'g':
				$nodeName = 'groups';
				break;

			default:
				$nodeName = 'figure.'.$element->nodeName;
				$info['figures']++;
				break;
		}
		$info[$nodeName] = (isset($info[$nodeName]) ? $info[$nodeName] : 0) + 1;
	}

	/**
	 * @param \DOMElement $element
	 */
	public function findColors($element)
	{
		$color = 'default';
		$attrFill = $element->attributes->getNamedItem('fill'); // <line fill="#333333" ...
		if ($attrFill)
		{
			$color = (string)$attrFill->value;
		}

		$attrStyle = $element->attributes->getNamedItem('style'); // <line style="fill:#333333;" ...
		if ($attrStyle)
		{
			$tool = new CssTool;
			$stylesArray = $tool->stylesToArray((string)$attrStyle->value);
			if (isset($stylesArray['fill']))
				$color = $stylesArray['fill'];
		}

		$color = strtolower($color);
		$this->_info['colors'][] = $color;
	}

	/**
	 * @param \DOMElement $element
	 */
	protected function countStrokedFigures($element)
	{
		$this->setElementId($element);
		$id = $element->attributes->getNamedItem('id')->value;

		if (isset($this->_alreadyStrokedCounted[$id]))
			return;
		$this->_alreadyStrokedCounted[$id] = true;

		$info = &$this->_info;
		switch($element->nodeName)
		{
			case 'svg':
			case 'style':
				return;

			case 'g':
				$nodeName = 'stroke.groups';
				break;

			default:
				$nodeName = 'stroke.'.$element->nodeName;
				$info['strokes']++;
				break;
		}
		$info[$nodeName] = (isset($info[$nodeName]) ? $info[$nodeName] : 0) + 1;
	}

	/**
	 * @param \DOMElement $element
	 */
	public function setElementId($element)
	{
		/** @var \DOMAttr $attrId */
		$attrId = $element->attributes->getNamedItem('id');
		if (!$attrId)
		{
			$attrId = new \DOMAttr('id');
			$attrId->value = $this->_figureIdPrefix . ($this->_figureId++);
			$element->setAttributeNode($attrId);
		}
	}

	/**
	 * Возвращает true если в стилях $styles найден атрибут stroke и он указывает на наличие обводки
	 * @param $styles
	 * @return bool
	 */
	public function hasStrokesStyles($styles)
	{
		$styles = preg_split('/\s*;\s*/', $styles); // stroke:#333333;stroke-width:2;
		foreach($styles as $style)
		{
			if (false!==strpos($style, ':')) // stroke:#333333
			{
				list($name,$value) = explode(':', $style);
				$name = trim($name); // удалить лишние пробелы
				$value = trim($value); // удалить лишние пробелы
				if ('stroke'==$name && 'none'!=$value) // stroke:#333333
					return true;
			}
		}
		return false;
	}

    public function setBoundingBoxCoordinates($src, $type = 'path')
    {
        $boundingBoxArray = [];
        switch($type)
        {
            case 'path':     $boundingBoxCommand = 'echo "' . $src . '" | svg-path-bounding-box 2>&1'; break;
            case 'fullSVG':
                 default:    $boundingBoxCommand = 'echo "' . str_replace('"', '\"', $src) . '" | svg-bounding-box 2>&1'; break;
        }
        exec($boundingBoxCommand, $result);
        if(is_array($result) && strpos(current($result), 'sh:') === false && strpos(current($result), 'Error') === false) // если нет ошибок
        {
            $resultArray = explode(' ', current($result));
            if($resultArray)
            {
                $boundingBoxArray = [
                    'startX' => $resultArray[0],
                    'startY' => $resultArray[1],
                    'width' => $resultArray[2],
                    'height' => $resultArray[3]
                ];
            }
        }
        else{
            \Yii::log('A problem have been encountered  during using a svg-path-bounding-box command. ' .
                var_export($result, true), \CLogger::LEVEL_ERROR);
            $this->_log[] = 'A problem have been encountered  during using a svg-path-bounding-box command. ' .
                var_export($result, true);
        }
        $this->_info['svg:boundingBox:coordinates'] = $boundingBoxArray;
    }

    public function setViewBoxCoordinates($viewBox)
    {
        $viewBoxArray = [];
        $resultArray = preg_split('/[\s,]+/', $viewBox);
        if(count($resultArray) == 4)
        {
            $viewBoxArray = [
                'startX' => $resultArray[0],
                'startY' => $resultArray[1],
                'width' => $resultArray[2],
                'height' => $resultArray[3]
            ];
        }
        $this->_info['svg:viewBox:coordinates'] = $viewBoxArray;
    }

	/**
	 * @return array
	 */
	public function getLog()
	{
		return $this->_log;
	}
}

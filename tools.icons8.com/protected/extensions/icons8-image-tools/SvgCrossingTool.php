<?php

namespace icons8_image_tools;

/**
 * Class SvgCrossingTool
 *
 * Для указанной SVG составляет команды получения перечёркнутой версии с использованием Inkscape и SVGO
 */
class SvgCrossingTool extends AbstractTool
{
	/**
	 * @var \DOMDocument
	 */
	protected $_svgXmlDocument;

	protected $_log = array();

	/**
	 * Команды для Inscape
	 * @see http://how-to.wikia.com/wiki/How_to_use_Inkscape_in_commandline_mode/List_of_verbs
	 * @var array
	 */
	protected $_inkscapeVerbs = array();

	public function __construct($binaries = array())
	{
		$this->setBinaries($binaries);
	}

	public function cross($template, $srcFileAbsPath, $dstFileAbsPath, $tmpAbsPath = '/tmp')
	{
		if (!$this->_binaries['svgo'])
		{
			\Yii::log('Не настроен svgo', \CLogger::LEVEL_WARNING);
			return false;
		}

		if (!$this->_binaries['inkscape'])
		{
			\Yii::log('Не настроен inkscape', \CLogger::LEVEL_WARNING);
			return false;
		}

		$tmpAbsPath = rtrim($tmpAbsPath, '/').'/';

		$svgDebugIdFileAbsPath = $tmpAbsPath.'debug-id.svg';
		$inkscapeSvgFileAbsPath = $tmpAbsPath.'inkscape.svg';
		$svgTmpFileAbsPath = $tmpAbsPath.'svgo.svg';

		if (!$this->_prepareCrossWithInkscape($template, $srcFileAbsPath))
		{
			// что-то не так в обработке SVG через DOMDocument
			$this->_log[] = 'SVG analysis failed';
			\Yii::log('SVG analysis failed', \CLogger::LEVEL_WARNING);
			return false;
		}

		$this->_save($inkscapeSvgFileAbsPath);
		$this->_save($svgDebugIdFileAbsPath);

		if (!$this->_crossWithInkscape($inkscapeSvgFileAbsPath, $tmpAbsPath))
		{
			// Файл не удалось обработать с помощью Inkscape
			$this->_log[] = 'Cross with Inkscape failed';
			\Yii::log('Cross with Inkscape failed', \CLogger::LEVEL_WARNING);
			return false;
		}
		if (!$this->_simplifyWithSvgo($inkscapeSvgFileAbsPath, $svgTmpFileAbsPath))
		{
			// Файл не удалось упростить с помощью SVGO
			$this->_log[] = 'Simplify with SVGO failed';
			\Yii::log('Simplify with SVGO failed', \CLogger::LEVEL_WARNING);
			return false;
		}

		if (!\FileSystemHelper::cp($svgTmpFileAbsPath, $dstFileAbsPath))
		{
			$this->_log[] = 'Failed save file to ' . $dstFileAbsPath;
			\Yii::log('Failed save file to ' . $dstFileAbsPath, \CLogger::LEVEL_WARNING);
			return false;
		}

		$this->_log[] = 'File is ready, saved to ' . $dstFileAbsPath;
		return true;
	}

	protected function _open($srcFileAbsPath)
	{
		if (!is_file($srcFileAbsPath)) {
			\Yii::log('SVG Crossing tool: file "' . $srcFileAbsPath . '" is absent', \CLogger::LEVEL_ERROR);
			throw new \Exception('SVG file "' . $srcFileAbsPath . '" is absent');
		}

		/** @var $xmlDoc \DOMDocument */

		$svgContent = file_get_contents($srcFileAbsPath);
		if ('<svg' == substr($svgContent, 0, 4))
			$svgContent = '<?xml version="1.0" encoding="utf-8"?>' . $svgContent;

		$xmlDoc = new \DOMDocument('1.0', 'utf-8');
		$xmlDoc->validateOnParse = true;
		if (false === @$xmlDoc->loadXML($svgContent))
		{
			throw new \Exception('File "' . $srcFileAbsPath . '" is not in SVG format');
		}

		$this->_log[] = 'opened XML file "' . $srcFileAbsPath . '"';

		return $xmlDoc;
	}

	protected function _prepareCrossWithInkscape($template, $svgFileAbsPath)
	{
		$this->_log[] = 'analysing ' . $svgFileAbsPath;

		$timeMs = microtime(true);
		$this->_inkscapeVerbs = array();
		$this->_svgXmlDocument = $this->_open($svgFileAbsPath);

		// регистрация пространства имён для SVG; требуется чтобы внутри XML найти любые теги
		$context = $this->_svgXmlDocument->documentElement; // это должен быть тег <svg ...>
		$svgPrefix = 'svg';
		$namespaceURI = $context->namespaceURI; // <svg xmlns="http://www.w3.org/2000/svg" ...>

		$xpath = new \DOMXpath($this->_svgXmlDocument);
		$xpath->registerNamespace($svgPrefix, $namespaceURI);
		$this->_log[] = '  registered namespace "' . $svgPrefix . '" = "' . $namespaceURI . '"';

		// собрать все кривые в одну группу
		$mainGroupNode = $this->_svgXmlDocument->createElement('g');
		$attrId = new \DOMAttr('id');
		$attrId->value = 'figure-main';
		$mainGroupNode->setAttributeNode($attrId);

		$query = "/{$svgPrefix}:svg/*";
		$elements = $xpath->query($query);
		foreach($elements as $element)
			$mainGroupNode->appendChild($element);
		$context->appendChild($mainGroupNode);

		// добавить черту
		$lineSvgFileAbsPath = APP_PATH.'/static/svg_crossed/'.$template.'_line.svg';
		$_lineSvgXmlDocument = $this->_open($lineSvgFileAbsPath);
		$_lineXpath = new \DOMXpath($_lineSvgXmlDocument);
		$_lineXpath->registerNamespace($svgPrefix, $namespaceURI);
		$_lineElement = $_lineXpath->query("//*[@id='template-line']")->item(0);
		if (!$_lineElement)
		{
			$this->_log[] = 'SVG with line ' . $lineSvgFileAbsPath . ' : no <path id="template-line"> node';
			return false;
		}
		$_lineElementCopy = $this->_svgXmlDocument->importNode($_lineElement, true);
		$this->_log[] = '    added "template-line" node from ' . $lineSvgFileAbsPath;
		$context->appendChild($_lineElementCopy);

		// добавить текст
		$textSvgFileAbsPath = APP_PATH.'/static/svg_crossed/'.$template.'_text.svg';
		$_textSvgXmlDocument = $this->_open($textSvgFileAbsPath);
		$_textXpath = new \DOMXpath($_textSvgXmlDocument);
		$_textXpath->registerNamespace($svgPrefix, $namespaceURI);
		$_textElement = $_textXpath->query("//*[@id='template-text']")->item(0);
		if (!$_textElement)
		{
			$this->_log[] = 'SVG with text ' . $textSvgFileAbsPath . ' : no <path id="template-text"> node';
			return false;
		}
		$_textElementCopy = $this->_svgXmlDocument->importNode($_textElement, true);
		$context->appendChild($_textElementCopy);
		$this->_log[] = '    added "template-text" node from ' . $textSvgFileAbsPath;

		// произвести операцию "КРИВЫЕ объединить с ЧЕРТОЙ и вычесть ТЕКСТ"
		$this->_inkscapeVerbs[] = '--select=figure-main --verb=SelectionUnGroup --verb=SelectionUnion';
		$this->_inkscapeVerbs[] = '--select=template-line --verb=SelectionUnion';
		$this->_inkscapeVerbs[] = '--select=template-text --verb=SelectionDiff';

		$this->_log[] = 'analyze done in ' . number_format(1000*(microtime(true) - $timeMs), 2, '.', '') . ' ms';

		return true;
	}

	protected function _save($svgFilepath)
	{
		if (!is_null($this->_svgXmlDocument))
		{
			$result = $this->_svgXmlDocument->save($svgFilepath);
			return $result;
		}
		return false;
	}

	protected function _simplifyWithSvgo($srcSvgFileAbsPath, $dstSvgAbsPath)
	{
		$svgo = $this->_binaries['svgo'];

		// @see https://github.com/svg/svgo  плагины для SVGO
		$disabledPlugins = '--disable=convertPathData --disable=convertShapeToPath --disable=mergePaths';
		$command = "$svgo --pretty $disabledPlugins --input=$srcSvgFileAbsPath --output=$dstSvgAbsPath";
		\Yii::log('$ > ' . $command, \CLogger::LEVEL_INFO);
		$this->_log[] = '$ ' . $command;

		$timeMs = microtime(true);
		$this->run($command, array(), $output);
		foreach($output as $line)
			$this->_log[] = '$   < ' . $line;
		$this->_log[] = '$ done in ' . number_format(1000*(microtime(true) - $timeMs), 2, '.', '') . ' ms';

		return is_file($dstSvgAbsPath); //&& filesize($srcSvgFileAbsPath)!==filesize($dstSvgAbsPath);
	}

	protected function _crossWithInkscape($svgFileAbsPath, $tmpAbsPath)
	{
		$inkscape = $this->_binaries['inkscape'];
		$xvfb = $this->_binaries['xvfb-run'];

		$command = $xvfb . ' -a ' . $inkscape . ' --with-gui ' . implode(' ', $this->_inkscapeVerbs) . ' --verb=FileSave --verb=FileClose -f ' . $svgFileAbsPath;

		\Yii::log('$ ' . $command, \CLogger::LEVEL_INFO);
		$this->_log[] = '$ > ' . $command;

		$timeMs = microtime(true);
		$this->run($command, array(), $output, 30); // with timeout
		foreach($output as $line)
			$this->_log[] = '$   < ' . $line;
		$this->_log[] = '$ done in ' . number_format(1000*(microtime(true) - $timeMs), 2, '.', '') . ' ms';

		# TODO сравнить файлы, сделать вывод о проблемах

		return true;
	}

	/**
	 * @return array
	 */
	public function getLog()
	{
		return $this->_log;
	}
}

<?php

namespace icons8_image_tools;

class PngTool extends AbstractTool
{
	public function __construct($flags = array(), $binaries = array())
	{
		$defaultsFlags = array(
			'strategy' => 'compatible',
			'background-color' => '',
			'transparent' => true,
			'multipass' => false,
		);

		$requiredBinaries = array(
			'imgo' => false,
		);

		$flags = array_merge($defaultsFlags, $flags);
		$binaries = array_merge($requiredBinaries, $binaries);

		$flags[] = 'brute-force' == strtolower($flags['strategy']) ? 'brute-force' : 'compatible';
		$flags['transparent'] = !!$flags['transparent'];
		$flags['multipass'] = !!$flags['multipass'];

		$this->setFlags($flags);
		$this->setBinaries($binaries);
	}

	/**
	 * Optimize PNG images
	 *
	 * @param array $pngs paths to separate PNG images or image folders
     * @return bool
	 */
	public function optimize(array $pngs)
	{
		if (!$this->_binaries['imgo'])
		{
			\Yii::log('Не настроены средства вывода изображений (imgo)', \CLogger::LEVEL_WARNING);
			return false;
		}

		\Yii::log('Optimize PNG from "' .  implode('", "', $pngs) . '" by strategy ' . $this->_flags['strategy'] . '/' . ($this->_flags['multipass'] ? 'multy pass' : 'single pass') , \CLogger::LEVEL_INFO);
		$params = array(
			'-png', // обрабатывать только файлы PNG
		);
		if ('brute-force' == $this->_flags['strategy'])
		{
			$params[] = '--brute'; // - режим продвинутого сжатия, опасен для IE6, сжимает лучше, но раза в 2-3 медленнее
		}
		if ($this->_flags['multipass'])
		{
			$params[] = '--multipass'; // - если файл удалось сжать, он будет обработан повторно, до тех пор, пока эффективность сжатия не будет равна 0
		}
		if ($this->_flags['background-color'])
		{
			$params[] = '-bkgd'.$this->_flags['background-color']; // устанавливает bKGD, после чего сжимает файл
		}
		#$params[] = '--quiet'; // - скрытый режим, никакой информации выведено не будет
		#$params[] = '--log'; // - выводит на экран дополнительную информацию, применяется в отладочных целях

		$params[] = '--';
		$params[] = array_values($pngs);

 		$result = $this->run($this->_binaries['imgo'], $params, $output);
		return $result;
	}
}
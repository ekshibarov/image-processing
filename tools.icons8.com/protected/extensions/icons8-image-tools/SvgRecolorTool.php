<?php

namespace icons8_image_tools;

/**
 * Class SvgRecolorTool
 *
 * В указанной SVG меняет цвета линий и заливок на заданный
 */
class SvgRecolorTool
{
    /**
     * @var \DOMDocument
     */
    protected $_recoloredSvgXml;

    protected $_log = array();

    /**
     * Возвращает true если были внесены изменения
     * @param string $fileAbsPath полный путь к файлу
     * @param string $color во формате #RRGGBB
     * @return bool
     * @throws \Exception
     */
    public function recolorImage($fileAbsPath, $color = '#000000')
    {
        if (!is_file($fileAbsPath)) {
            \Yii::log('SVG Recoloring tool: file "' . $fileAbsPath . '" is absent', \CLogger::LEVEL_ERROR);
            throw new \Exception('SVG file is absent');
        }

        $color = strtolower($color);

        # TODO проверять необходимость перекраски без чтения файла ?

        /** @var $element \DOMElement */
        /** @var $xmlDoc \DOMDocument */
        /** @var $attrFill \DOMAttr */
        /** @var $attrStroke \DOMAttr */
        /** @var $attrStyle \DOMAttr */

        $this->_recoloredSvgXml = $xmlDoc = new \DOMDocument('1.0', 'utf-8');
        if (false === $xmlDoc->load($fileAbsPath))
        {
            \Yii::log('Не удалось открыть файл "' . $fileAbsPath . '" как XML', \CLogger::LEVEL_ERROR);
            throw new \Exception('File is not in SVG format');
        }
        $this->_log[] = 'opened XML file "' . $fileAbsPath . '"';

        $xpath = new \DOMXpath($xmlDoc);

        // регистрация пространства имён для SVG требуется чтобы внутри XML найти теги path или любые другие
        $context = $xmlDoc->documentElement; // это должен быть тег <svg ...>
        $svgPrefix = 'svg';
        $namespaceURI = $context->namespaceURI; // <svg xmlns="http://www.w3.org/2000/svg" ...>
        $xpath->registerNamespace($svgPrefix, $namespaceURI);
        $this->_log[] = 'registered namespace "' . $svgPrefix . '" = "' . $namespaceURI . '"';

        $recolored = false;

        // замена атрибутов fill в тегах вида <polyline fill="#333333" ...
        $elements = $xpath->query("//*[@fill]");
        foreach($elements as $element)
        {
            $attrFill = $element->attributes->getNamedItem('fill');
            if (strtolower($attrFill->value) != $color && 'none' != $attrFill->value) {
                $this->_log[] = 'replaced [' . $element->nodeName . ' fill="' . $attrFill->value . '" to "' . $color . '" ...]';
                $attrFill->value = $color;
                $recolored = true;
            }
        }

        // замена атрибутов stroke в тегах вида <line stroke="#333333" ...
        $elements = $xpath->query("//*[@stroke]");
        foreach($elements as $element)
        {
            $attrStroke = $element->attributes->getNamedItem('stroke');
            if (strtolower($attrStroke->value) != $color && 'none' != $attrStroke->value) {
                $this->_log[] = 'replaced [' . $element->nodeName . ' stroke="' . $attrStroke->value . '" to "' . $color . '" ...]';
                $attrStroke->value = $color;
                $recolored = true;
            }
        }

        // замена цветов в inline стилях вида <line style="fill:#333333;stroke:#333333;stroke-width:2;stroke-linejoin:round;stroke-miterlimit:10;" ...
        $elements = $xpath->query("//*[@style]");
        foreach($elements as $element)
        {
            $attrStyle = $element->attributes->getNamedItem('style');
            $styles = $this->fixStyles($attrStyle->value, $color);
            if (false!==$styles)
            {
                $this->_log[] = 'replaced [' . $element->nodeName . ' fill="' . $attrStyle->value . '" to "' . $styles . '" ...]';
                $attrStyle->value = $styles;
                $recolored = true;
            }
        }

        // вставка атрибута fill у всех тегов <path>, если у них нет такого атрибута
        $elements = $xpath->query("//{$svgPrefix}:*");
        foreach($elements as $element)
        {
            if ('style'!=$element->nodeName && !$element->attributes->getNamedItem('fill'))
            {
                $attrFill = $xmlDoc->createAttribute('fill');
                $attrFill->value = $color;
                $element->appendChild($attrFill);
                $this->_log[] = 'added [' . $element->nodeName . ' fill="' . $attrFill->value . '" ...]';
                $recolored = true;
            }
        }

        // исправление стилей в тегах вида <style xmlns="http://www.w3.org/2000/svg">.st0{fill:none;stroke:#ff00ff;}</style>
        $elements = $xpath->query("//{$svgPrefix}:style");
        foreach($elements as $element)
        {
            /** @var $styleTextNode \DOMText */
            foreach($element->childNodes as $styleTextNode)
            {
                if ($styleTextNode->nodeType != XML_TEXT_NODE) {
                    continue;
                }
                $self = $this;
                $styleTextNode->nodeValue = preg_replace_callback('/{([^}]+)}/u', function($matches) use($self, $color, &$recolored) {
                    $styles = $self->fixStyles($matches[1], $color);
                    if (false!==$styles)
                        $recolored = true;
                    return false !== $styles ? "{{$styles}}" : $matches[0];
                }, $styleTextNode->textContent);
            }
        }

        if ($recolored)
            $this->_log[] = 'recolor complete';

        return $recolored;
    }

    public function save($absFilepath)
    {
        if (!is_null($this->_recoloredSvgXml))
        {
            \Yii::log('Save recolored SVG as "' . $absFilepath . '"', \CLogger::LEVEL_INFO);
            $result = $this->_recoloredSvgXml->save($absFilepath);
            $this->_log[] = 'recolored SVG is saved';
            return $result;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getLog()
    {
        return $this->_log;
    }

    public function fixStyles($styles, $color)
    {
        $styles = preg_split('/\s*;\s*/', $styles);
        $styleChanged = false;
        foreach($styles as &$style)
        {
            if (false!==strpos($style, ':')) // fill:#333333
            {
                list($name,$value) = explode(':', $style);
                $value = trim($value); // удалить лишние пробелы
                if (strlen($value)>0 && preg_match('/^#[a-fA-F0-9]{6}$/', $value) && strtolower($value) != $color) // #333333
                {
                    $style = "{$name}:{$color}";
                    $styleChanged = true;
                }
            }
        }
        return $styleChanged ? implode(';', $styles) : false;
    }

    public function validate($fileAbsPath)
    {
        if (!is_file($fileAbsPath)) {
            #\Yii::log('Файл "' . $fileAbsPath . '" отсутствует', \CLogger::LEVEL_ERROR);
            throw new \Exception('SVG file is absent');
        }

        $svgContent = file_get_contents($fileAbsPath);
        if ('<svg' == substr($svgContent, 0, 4))
            $svgContent = '<?xml version="1.0" encoding="utf-8"?>' . $svgContent;

        $xmlDoc = new \DOMDocument('1.0', 'utf-8');
        if (false === @$xmlDoc->loadXML($svgContent))
        {
            #\Yii::log('Не удалось открыть файл "' . $fileAbsPath . '" как XML', \CLogger::LEVEL_ERROR);
            throw new \Exception('File is not in SVG format');
        }

        $context = $xmlDoc->documentElement; // это должен быть тег <svg ...>
        if ('svg' != $context->nodeName)
        {
            #\Yii::log('XML "' . $fileAbsPath . '" не в формате SVG', \CLogger::LEVEL_ERROR);
            throw new \Exception('File is not in SVG format');
        }

        $namespaceURI = $context->namespaceURI; // <svg xmlns="http://www.w3.org/2000/svg" ...>
        if ('http://www.w3.org/2000/svg' != $namespaceURI)
        {
            // TODO ну и что?
        }
        return true;
    }
}

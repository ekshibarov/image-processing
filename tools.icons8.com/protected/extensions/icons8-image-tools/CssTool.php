<?php

namespace icons8_image_tools;

class CssTool
{
	public function combineStyles($firstStyles, $secondStyles)
	{
		$firstStylesArray = $this->stylesToArray($firstStyles);
		$secondStylesArray = $this->stylesToArray($secondStyles);
		// TODO проверить порядок; должен быть такой как в Inkscape
		$combinedStylesArray = array_merge($firstStylesArray, $secondStylesArray);
		return $this->stylesToString($combinedStylesArray);
	}

	/**
	 * @param string $cssRules
	 * @return array
	 */
	public function stylesToArray($cssRules)
	{
		$cssRulesArray = [];
		$styles = preg_split('/\s*;\s*/', $cssRules); // stroke:#333333;stroke-width:2;
		foreach($styles as $style)
		{
			if (false!==strpos($style, ':')) // stroke:#333333
			{
				list($name,$value) = explode(':', $style);
				// $name = "stroke"
				// $value = "#333333"
				$name = trim($name); // удалить лишние пробелы
				$value = trim($value); // удалить лишние пробелы
				$cssRulesArray[$name] = $value;
			}
		}
		// $cssRulesArray = [ "stroke" => "#333333", "stroke-width" => 2 ]
		return $cssRulesArray;
	}

	/**
	 * @param array $stylesArray
	 * @return string
	 */
	public function stylesToString($stylesArray)
	{
		$styles = [];
		foreach($stylesArray as $name => $value)
		{
			$styles[] = "{$name}:{$value}";
		}
		return implode(';', $styles);
	}

	/**
	 * @param string $cssRulesPlain
	 * @return array
	 */
	public function cssRulesToArray($cssRulesPlain)
	{
		// $cssRulesPlain = "
		//      .st0{fill:none;stroke:#ff00ff;}
		//      .st1{fill:#000000;}
		//      .st0{stroke-width:2;}
		// "

		// TODO удалить комментарии

		// разбить на правила
		$rules = array_filter(array_map('trim', explode('}', $cssRulesPlain)));
		$cssRulesArray = [];
		foreach($rules as $rule)
		{
			list($selector, $styles) = explode('{', $rule);
			$selector = preg_replace('/\s+/siu', ' ', trim($selector));
			$stylesArray = $this->stylesToArray($styles);
			if (!isset($cssRules[$selector]))
			{
				$cssRulesArray[$selector] = $stylesArray;
			} else
			{
				$cssRulesArray[$selector] = $this->combineStyles($cssRulesArray[$selector], $stylesArray);
			}
		}

		// $cssRulesArray = [
		//   '.st0' => [ "fill" => "none", "stroke" => "#ff00ff", "stroke-width" => 2 ],
		//   '.st1' => [ "fill" => "#000000" ],
		// ]
		return $cssRulesArray;
	}
}
<?php

namespace icons8_image_tools;

class SimplifyException extends \Exception { }

/**
 * Class SvgSimplifyingTool
 *
 * Для указанной SVG составляет команды упрощения с использованием Inkscape
 */
class SvgSimplifyingTool extends AbstractTool
{
	/**
	 * @var \DOMDocument
	 */
	protected $_svgXmlDocument;

	/**
	 * Команды для Inscape
	 * @see http://how-to.wikia.com/wiki/How_to_use_Inkscape_in_commandline_mode/List_of_verbs
	 * @var array
	 */
	protected $_inkscapeVerbs = array();

	/**
	 * Признак необходимости упрощения через Inkscape
	 * @var bool
	 */
	protected $_forceUseInkscape = false;

	protected $_figureIdPrefix = 'figure';

	protected $_figureId = 1;

	protected $_partUnionIds = array();

	/**
	 * Признак для выполнения финального объединения всех фигур в одну
	 * @var bool
	 */
	protected $_addSelectionUnionVerb = true;

	protected $webBrowsingSvgFolder;

	protected $webBrowsingSvgRelPath;

	public $fileName;

	protected $webBrowsingFileUrl;

	protected $prettyLog;

	public function __construct($binaries = array())
	{
		$this->setBinaries($binaries);
		$this->setWebBrowsingPath();
	}

	public function simplify($srcFileAbsPath, $dstFileAbsPath, $tmpAbsPath = '/tmp')
	{
		$result = false;
		try
		{
			$result = $this->_trySimplify($srcFileAbsPath, $dstFileAbsPath, $tmpAbsPath);
		} catch(SimplifyException $e)
		{
			\Yii::log('Failed to simplify "' . $srcFileAbsPath . '" : ' . $e->getMessage(), \CLogger::LEVEL_ERROR);
		}
		return $result;
	}

	protected function _trySimplify($srcFileAbsPath, $dstFileAbsPath, $tmpAbsPath = '/tmp')
	{
		if (!$this->_binaries['svgo'])
		{
			\Yii::log('Не настроен svgo', \CLogger::LEVEL_WARNING);
			return false;
		}

		if (!$this->_binaries['inkscape'])
		{
			\Yii::log('Не настроен inkscape', \CLogger::LEVEL_WARNING);
			return false;
		}

		$tmpAbsPath = rtrim($tmpAbsPath, '/').'/';
		$svgTmpFileAbsPath = $tmpAbsPath.'svgo.0.tmp.svg';

		if (!$this->_simplifyWithSvgo($srcFileAbsPath, $svgTmpFileAbsPath))
		{
			// Файл не удалось упростить с помощью SVGO
			$this->_log[] = 'Simplify with SVGO failed';
			$this->saveForWebBrowsing($svgTmpFileAbsPath);
			throw new SimplifyException('Simplify with SVGO failed');
		}
		$this->_log[] = 'Simplified with SVGO, saved to ' . $svgTmpFileAbsPath;

		// проверка содержимого после упрощения
		if (false!==($message = $this->_checkSvgIsBroken($svgTmpFileAbsPath)))
		{
			// Файл содержит не содержит ни одного примитива, либо содержит некорректные данные
			$this->_log[] = 'SVG image is broken: ' . $message;
			$this->addPrettyLog('SVG поврежден: <br>' . $message);
			$this->saveForWebBrowsing($svgTmpFileAbsPath);
			throw new SimplifyException('SVG image is broken: ' . $message);
		}

		$maxTries = 6; // упростить за один раз иногда не получается, поэтому цикл
		for($try = 1; $try <= $maxTries; $try++)
		{
			$svgoFileAbsPath = $tmpAbsPath.'svgo.'.$try.'.tmp.svg';
			$svgDebugIdFileAbsPath = $tmpAbsPath.'debug-id.'.$try.'.svg';
			$inkscapeSvgFileAbsPath = $tmpAbsPath.'inkscape.'.$try.'.svg';
			$inkscapeLogAbsPath = $tmpAbsPath.'inkscape.'.$try.'.log';

			if (!$this->_canSimplifyWithInkscape($svgTmpFileAbsPath))
			{
				// файл можно не упрощать
				$this->addPrettyLog('Файл не может быть упрощен');
				$this->saveForWebBrowsing($svgTmpFileAbsPath);
				break;
			}
			if ($maxTries == $try) // максимальное количество попыток
			{
				// Файл не удалось упростить с помощью Inkscape
				$this->_log[] = 'Simplify with Inkscape failed after '.($try-1).' tries';
				$this->addPrettyLog('Не удалось упростить с помощью Inkscape');
				throw new SimplifyException('Simplify with Inkscape failed after '.($try-1).' tries');
			}

			$this->_log[] = 'Can be simplified with Inkscape';
			$this->_save($inkscapeSvgFileAbsPath);
			$this->_log[] = 'Prepared to Inkscape, saved to ' . $inkscapeSvgFileAbsPath;

			if (!\FileSystemHelper::cp($inkscapeSvgFileAbsPath, $svgDebugIdFileAbsPath))
			{
				$this->_log[] = 'Failed save debug copy file to ' . $svgDebugIdFileAbsPath;

				throw new SimplifyException('Failed save debug copy file to ' . $svgDebugIdFileAbsPath);
			}
			$this->_log[] = 'Debug copy saved to ' . $svgDebugIdFileAbsPath;

			if (!$this->_simplifyWithInkscape($inkscapeSvgFileAbsPath, $tmpAbsPath))
			{
				// Файл не удалось упростить с помощью Inkscape
				$this->_log[] = '#'.$try.' : Simplify with Inkscape failed';

				$this->saveForWebBrowsing($inkscapeSvgFileAbsPath);

				throw new SimplifyException('#'.$try.' : Simplify with Inkscape failed');
			}

			$this->_log[] = '#'.$try.' : Simplified with Inkscape, saved to ' . $inkscapeSvgFileAbsPath;
			if (!$this->_simplifyWithSvgo($inkscapeSvgFileAbsPath, $svgoFileAbsPath))
			{
				// Файл не удалось упростить с помощью SVGO
				$this->_log[] = '#'.$try.' : Simplify with SVGO failed';
				$this->addPrettyLog('Не удалось упростить с помощью SVGO');

				$this->saveForWebBrowsing($svgoFileAbsPath);

				throw new SimplifyException('#'.$try.' : Simplify with SVGO failed');
			}

			// проверка содержимого после упрощения
			if (false!==($message = $this->_checkSvgIsBroken($svgoFileAbsPath)))
			{
				// Файл содержит не содержит ни одного примитива, либо содержит некорректные данные
				$this->_log[] = 'SVG image is broken: ' . $message;
				$this->addPrettyLog('SVG поврежден: <br>' . $message);
				$this->saveForWebBrowsing($svgoFileAbsPath);
				throw new SimplifyException('#'.$try.' : SVG image is broken: ' . $message);
			}

			$this->_log[] = '#'.$try.' : Simplified with SVGO, saved to ' . $svgoFileAbsPath;
			if (!\FileSystemHelper::cp($svgoFileAbsPath, $dstFileAbsPath))
			{
				$this->_log[] = '#'.$try.' : Failed save file to ' . $dstFileAbsPath;
				throw new SimplifyException('#'.$try.' : Failed save file to ' . $dstFileAbsPath);
			}

			// запомнить имя промежуточного файла для следующей итерации
			$svgTmpFileAbsPath = $svgoFileAbsPath;
		}

		// проверка содержимого после упрощения
		if (false!==($message = $this->_checkSvgIsBroken($svgTmpFileAbsPath, true))) // с финальными проверками
		{
			// Файл содержит не содержит ни одного примитива, либо содержит некорректные данные
			$this->_log[] = 'SVG image is broken: ' . $message;
			$this->addPrettyLog('SVG поврежден: <br>' . $message);
			$this->saveForWebBrowsing($svgTmpFileAbsPath);
			throw new SimplifyException('SVG image is broken: ' . $message);
		}

		// файл можно не упрощать
		$this->_log[] = 'File ' . $svgTmpFileAbsPath . ' is simple enough';

		// Проверить визуальные различия оригинальной и упрощённой версий SVG
		$this->_checkSvgDifference($srcFileAbsPath, $svgTmpFileAbsPath, $tmpAbsPath);

		if (!\FileSystemHelper::cp($svgTmpFileAbsPath, $dstFileAbsPath))
		{
			$this->_log[] = 'Failed save file to ' . $dstFileAbsPath;
			throw new SimplifyException('Failed save file to ' . $dstFileAbsPath);
		}

		$this->_log[] = 'File is ready, saved to ' . $dstFileAbsPath;
		return true;
	}

	protected function _open($srcFileAbsPath)
	{
		if (!is_file($srcFileAbsPath))
			throw new SimplifyException('SVG file "' . $srcFileAbsPath . '" is absent');

		/** @var $xmlDoc \DOMDocument */

		$svgContent = file_get_contents($srcFileAbsPath);
		if ('<svg' == substr($svgContent, 0, 4))
			$svgContent = '<?xml version="1.0" encoding="utf-8"?>' . $svgContent;

		$xmlDoc = new \DOMDocument('1.0', 'utf-8');
		if (false === @$xmlDoc->loadXML($svgContent))
			throw new SimplifyException('File "' . $srcFileAbsPath . '" is not in SVG format');

		$this->_log[] = 'opened XML file "' . $srcFileAbsPath . '"';

		return $xmlDoc;
	}

	/**
	 * Определяет, можно ли упростить файл $svgoFileAbsPath с помощью Inkscape
	 * Составляет список команд для упрощения
	 *
	 * @param string $svgoFileAbsPath
	 * @return bool
	 */
	protected function _canSimplifyWithInkscape($svgoFileAbsPath)
	{
		$this->_log[] = 'analysing ' . $svgoFileAbsPath;
		$this->_partUnionIds = array();
		$this->_forceUseInkscape = false;
		$totalFiguresCount = 0;

		$timeMs = microtime(true);
		$this->_inkscapeVerbs = array();
		$this->_svgXmlDocument = $this->_open($svgoFileAbsPath);
		$xpath = new \DOMXpath($this->_svgXmlDocument);

		// иногда необходимо изменить объекты и проанализировать их вновь, а только потом объединять
		$this->_addSelectionUnionVerb = true;

		// регистрация пространства имён для SVG; требуется чтобы внутри XML найти любые теги
		$context = $this->_svgXmlDocument->documentElement; // это должен быть тег <svg ...>
		$svgPrefix = 'svg';
		$namespaceURI = $context->namespaceURI; // <svg xmlns="http://www.w3.org/2000/svg" ...>
		$xpath->registerNamespace($svgPrefix, $namespaceURI);
		$this->_log[] = '  registered namespace "' . $svgPrefix . '" = "' . $namespaceURI . '"';

		// Применить стили классов <style> ... </style> и удалить их
		$this->_removeClassStyles($xpath, $svgPrefix);

		// пустые элементы удалить, сложные объекты преобразовать в пути, группы открыть
		$query = "//*";
		$this->_log[] = '  search for complex objects: ' . $query;
		$elements = $xpath->query($query);
		foreach($elements as $element)
		{
			if ($this->_isEmptyFigure($element) // если это прозрачный элемент без обводки
				&& 'g' !== $element->nodeName // и этот элемент - не группа
				&& 'g' !== $element->parentNode->nodeName // и этот элемент не в группе
				)
			{
				// удалить узел
				$element->parentNode->removeChild($element);
				$this->_log[] = '    <' . $element->nodeName . ' fill="none" > ... -> remove';

				$this->_forceUseInkscape = true;
				continue; // перейти к следующему элементу
			}

			switch($element->nodeName)
			{
				case 'svg':
					break;

				case 'rect': //  <rect width="24" height="24" ... />
				case 'polygon': // <polygon points="25.2,40.914.6,47.7 13.2,35.1 1.2,31 10.2,22.1 6,10.1 18.6,11.6 25.2,0.9 31.9,11.6 44.5,10.1 40.2,22.1 49.2,31 37.3,35.1 35.9,47.7 "/>
				case 'polyline': // <polyline points="9,17 9,9 41,9 41,23"/>
				case 'circle': // <circle cx="42.5" cy="42.5" r="1"/>
				case 'ellipse': // <ellipse class="lt3" cx="120" cy="280" rx="100" ry="50"/>
				case 'line': // <line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="22" y1="20" x2="2" y2="20" />
					$totalFiguresCount++;
					$this->_addVerbObjectToPath($element);
					$id = $this->_getElementId($element);
					$this->_log[] = '    <' . $element->nodeName . ' id="' . $id . '" ... -> path';

					if ($this->_isStrokedFigure($element))
					{
						$this->_addVerbStrokeToPath($element);
						$this->_log[] = '    <' . $element->nodeName . ' id="' . $id . '" --> shape';
					}
					break;

				case 'g': // группы
					$this->_addVerbUngroupAll($element);
					$id = $this->_getElementId($element);
					$this->_log[] = '    <' . $element->nodeName . ' id="' . $id . '" ... -> ungroup';

					// после разгруппировки свойства группы могли "перейти" к элементам внутри группы, в частности атрибуты "stroke"
					$this->_addSelectionUnionVerb = false; // нельзя объединять после разгруппировки
					break;

				case 'path': // кривая линия
					if ($this->_isSinglePoint($element)) // кривая из одной точки <path d="M29.3,16.5" .../>
					{
						// удалить узел
						$element->parentNode->removeChild($element);
						$this->_log[] = '    <' . $element->nodeName . ' d="M X,Y" > ... -> remove';
						$this->_forceUseInkscape = true;
						continue 2; // перейти к следующему элементу
					}
					$totalFiguresCount++;
					if ($this->_isStrokedFigure($element))
					{
						$this->_addVerbStrokeToPath($element);
						$id = $this->_getElementId($element);
						$this->_log[] = '    <' . $element->nodeName . ' id="' . $id . '" --> shape';
					}
					break;

				default:
					$id = $this->_getElementId($element);
					$this->_log[] = '    <' . $element->nodeName . ' id="' . $id . '" ... -> no action';
					break;
			}
		}

		$this->_log[] = 'analyze done in ' . number_format(1000*(microtime(true) - $timeMs), 2, '.', '') . ' ms';
		return $totalFiguresCount>1 || $this->_forceUseInkscape || count($this->_inkscapeVerbs)>0;
	}

	/**
	 * @param \DOMElement $element
	 * @return bool
	 */
	protected function _isStrokedFigure($element)
	{
		$attrStroke = $element->attributes->getNamedItem('stroke'); // <line stroke="#333333" ...
		if ($attrStroke && 'none' != (string)$attrStroke->value)
			return true;

		$attrStyle = $element->attributes->getNamedItem('style'); // <line style="stroke:#333333;" ...
		if ($attrStyle)
		{
			$tool = new CssTool;
			$stylesArray = $tool->stylesToArray((string)$attrStyle->value);
			if (isset($stylesArray['stroke']) && 'none'!=$stylesArray['stroke'])
				return true;
		}

		return false;
	}

	/**
	 * @param \DOMElement $element
	 * @return bool
	 */
	protected function _isFilledFigure($element)
	{
		$attrFill = $element->attributes->getNamedItem('fill'); // <line fill="#333333" ...
		if ($attrFill && 'none' == (string)$attrFill->value)
			return false;

		$attrStyle = $element->attributes->getNamedItem('style'); // <line style="fill:#333333;" ...
		if ($attrStyle)
		{
			$tool = new CssTool;
			$stylesArray = $tool->stylesToArray((string)$attrStyle->value);
			if (isset($stylesArray['fill']) && 'none'==$stylesArray['fill'])
				return false;
		}

		return true;
	}

	/**
	 * @param \DOMElement $element
	 */
	protected function _setElementStyle($element, $style)
	{
		/** @var \DOMAttr $attrId */
		$attrStyle = $element->attributes->getNamedItem('style');
		if (!$attrStyle)
		{
			$attrStyle = new \DOMAttr('style');
			$attrStyle->value = $style;
			$element->setAttributeNode($attrStyle);
		} else
		{
			$attrStyle->value = (new CssTool)->combineStyles($attrStyle->value, $style);
		}
	}

	/**
	 * @param \DOMElement $element
	 * @return string
	 */
	protected function _getElementId($element)
	{
		/** @var \DOMAttr $attrId */
		$attrId = $element->attributes->getNamedItem('id');
		if (!$attrId)
		{
			$attrId = new \DOMAttr('id');
			$attrId->value = $this->_figureIdPrefix . ($this->_figureId++);
			$element->setAttributeNode($attrId);
		}
		return (string)$attrId->value;
	}

	/**
	 * @param \DOMElement $element
	 */
	protected function _addVerbObjectToPath($element)
	{
		$id = $this->_getElementId($element);
		$commandKey = 'toPath'.$id;
		if (isset($this->_inkscapeVerbs[$commandKey]))
			return; // уже добавляли

		$this->_inkscapeVerbs[$commandKey] = '--select=' . $id . ' --verb=ObjectToPath --verb=EditDeselect';
		$this->_partUnionIds[] = '--select=' . $id;
	}

	/**
	 * @param \DOMElement $element
	 */
	protected function _addVerbUngroupAll($element)
	{
		$id = $this->_getElementId($element);
		$commandKey = 'ungroup'.$id;
		if (isset($this->_inkscapeVerbs[$commandKey]))
			return; // уже добавляли

		$this->_inkscapeVerbs[$commandKey] = '--select=' . $id . ' --verb=SelectionUnGroup --verb=EditDeselect';
	}

	/**
	 * @param \DOMElement $element
	 */
	protected function _addVerbStrokeToPath($element)
	{
		$id = $this->_getElementId($element);
		$commandKey = 'toShape'.$id;
		if (isset($this->_inkscapeVerbs[$commandKey]))
			return; // уже добавляли

		$this->_inkscapeVerbs[$commandKey] = '--select=' . $id . ' --verb=StrokeToPath --verb=EditDeselect';
		$this->_partUnionIds[] = '--select=' . $id;
	}

	/**
	 * Копирует стилей классов в инлайн-стили и удаляет стили классов
	 * <style xmlns="http://www.w3.org/2000/svg">.st0{fill:none;stroke:#ff00ff;} ... </style>
	 * в инлайновые <path class="st0" style="fill:none;stroke:#ff00ff;" ... />
	 * 
	 * @param \DOMXpath $xpath
	 * @param string $svgPrefix
	 * @return bool
	 */
	protected function _removeClassStyles($xpath, $svgPrefix)
	{
		$tool = new CssTool;
		$query = "//{$svgPrefix}:style";
		$this->_log[] = '  search for style blocks: ' . $query;
		$elements = $xpath->query($query);
		foreach($elements as $element)
		{
			/** @var $styleTextNode \DOMText */
			foreach($element->childNodes as $styleTextNode)
			{
				if ($styleTextNode->nodeType != XML_TEXT_NODE) {
					throw new SimplifyException("Unsupported style content");
					// а может быть continue ?
				}

				$cssRules = $tool->cssRulesToArray($styleTextNode->textContent);
				foreach($cssRules as $selector => $stylesArray)
				{
					// $selector = ".st1" или ".st1 .st2" или ".st1.st2"
					if ('.'!==$selector{0}) // селектор в виде "path.st1" или "path"
					{
						throw new SimplifyException("Tag selector '{$selector} not supported");
					}
					if (substr_count($selector,' ')>1) // селектор в виде каскада несколько классов ".st1 .st2"
					{
						throw new SimplifyException("Cascade class selector '{$selector} not supported");
					}
					if (substr_count($selector,'.')>1) // селектор в виде комбинации классов ".st1.st2"
					{
						throw new SimplifyException("Combined class selector '{$selector} not supported");
					}
					$selectorClass = substr($selector, 1); // '.st0' -> 'st0'

					$selectorElements = $xpath->query("//*[@class=\"$selectorClass\"]");
					/** @var \DOMElement $selectorElement */
					foreach($selectorElements as $selectorElement)
					{
						$this->_setElementStyle($selectorElement, $tool->stylesToString($stylesArray));
					}
				}
			}

			// удалить узел <style>
			$element->parentNode->removeChild($element);
			$this->_forceUseInkscape = true;
		}

		return false;
	}

	/**
	 * Возвращает true если указанный элемент является прозрачным квадратом
	 * @param \DOMElement $element
	 * @return bool
	 */
	protected function _isEmptyFigure($element)
	{
		// предположу, что прозрачные области могут только атрибут fill="none" и stroke="none"
		// <rect width="24" height="24" fill="none" stroke="none" />

		if (!$this->_isFilledFigure($element) && !$this->_isStrokedFigure($element))
			return true;
	}

	/**
	 * Возвращает true если указанный элемент является прозрачным квадратом
	 * @param \DOMElement $element
	 * @return bool
	 */
	protected function _isSinglePoint($element)
	{
		// <path fill="none" stroke="#000" stroke-width="2" stroke-miterlimit="10" d="M29.3,16.5" />


		/** @var $attr \DOMAttr */
		$attr = $element->attributes->getNamedItem('d');
		if ($attr)
		{
			$data = str_replace(' ', '', (string)$attr->value); // "M2 9.3, 16.5" -> "M29.3,16.5"
			if (preg_match('/^M(-?(?:\d+|\d*\.\d+)),(-?(?:\d+|\d*\.\d+))$/ui', $data))
			// определяет что строка $data похожа на "M<число>,<число>", где <число> - это целое или дробное со знаком "-" или без знака
			//     например "M-1,-.1" "M1.1,.1"
			{
				return true;
			}
		}

		return false;
	}

	protected function _save($svgFilepath)
	{
		if (!is_null($this->_svgXmlDocument))
		{
			$result = $this->_svgXmlDocument->save($svgFilepath);
			return $result;
		}
		return false;
	}

	protected function _simplifyWithSvgo($srcSvgFileAbsPath, $dstSvgAbsPath)
	{
		$svgo = $this->_binaries['svgo'];

		// @see https://github.com/svg/svgo  плагины для SVGO
		$disabledPlugins = '--disable=convertPathData --disable=convertShapeToPath --disable=mergePaths';
		$command = "$svgo --pretty $disabledPlugins --input=$srcSvgFileAbsPath --output=$dstSvgAbsPath";
		$this->run($command, array(), $output);
		return is_file($dstSvgAbsPath); //&& filesize($srcSvgFileAbsPath)!==filesize($dstSvgAbsPath);
	}

	protected function _simplifyWithInkscape($svgFileAbsPath, $tmpAbsPath)
	{
		$inkscape = $this->_binaries['inkscape'];
		$xvfb = $this->_binaries['xvfb-run'];
		$hashBefore = sha1_file($svgFileAbsPath);

		// количество объектов, больше которого объединять опасно - можно получить битую картинку
		$tooMuchObjects = count($this->_partUnionIds) > 10; // магическое число

		$command = $xvfb . ' -a ' . $inkscape . ' --with-gui ' . implode(' ', $this->_inkscapeVerbs);
		if ($this->_addSelectionUnionVerb)
		{
			if ($tooMuchObjects) // слишком много объектов
			{
				// объединять порциями по несколько объектов
				$unionParts = array_chunk($this->_partUnionIds, 5);
				foreach($unionParts as $unionPart)
				{
					$command .= ' ' . implode(' ', $unionPart);
					$command .= ' --verb=SelectionUnion --verb=EditDeselect';
				}
			}
			else
			{
				$command .= ' --verb=EditSelectAllInAllLayers --verb=SelectionUnion';
			}
		}
		$command .= ' --verb=FileSave --verb=FileClose -f ' . $svgFileAbsPath;

		$this->run($command, array(), $output, 30); // with timeout

		// сравнить файлы: если не отличаются, значит Inkscape упал или был завершён по таймауту
		$hashAfter = sha1_file($svgFileAbsPath);
		if (0 == strcmp($hashBefore, $hashAfter))
		{
			$this->_log[] = 'Inkscape quit unexpectedly, image not processed';
			return false;
		}

		return true;
	}

	/**
	 * Возвращает true если SVG файл содержит некорректные данные либо вообще не содержит примитивов (пустой)
	 * @param $svgAbsPath
	 * @return bool|string
	 */
	protected function _checkSvgIsBroken($svgAbsPath, $finalCheck = false)
	{
		$tool = new SvgInfoTool;
		$info = $tool->info($svgAbsPath);
		if (!isset($info['figures']) || $info['figures']<1)
		{
			return 'No figures'; // нет примитивов
		}

		if (isset($info['svg:viewbox']) && !preg_match('/^0 0 \d\d \d\d$/', $info['svg:viewbox'])) // пример: "0 0 50 50"
		{
			return 'Wrong viewbox (viewbox="' . $info['svg:viewbox'].'")'; // нарушен viewbox
		}

		if ($finalCheck)
		{
			if ($info['figures']>1)
			{
				return 'More than one figure (total number of figures: ' . $info['figures'] . ')'; // лишние примитивы
			}

			if (!isset($info['styles']) && $info['styles']>0)
				return 'Class styles still exists'; // остались стили классов

			if (isset($info['colors']))
			{
				$colors = array_flip(explode(',', $info['colors']));
				if (isset($colors['none']))
				{
					return 'Figures without fill found (fill="none")'; // элементы без цвета
				}
				if(isset($colors['default']) && isset($colors['#000000']))
				{
					// считаю, что default = #000000
					unset($colors['default']);
				}
				if (count($colors)>1)
				{
					return 'More than one fill color (list of colors: ' . $info['colors'] . ')'; // несколько цветов
				}
			}

			if (!isset($info['strokes']) && $info['strokes']>0)
				return 'Stroked figures still exists'; // остались элемент с обводкой
		}

		return false;
	}

	/**
	 * Бросает исключение если файл $simplifiedFileAbsPath слишком сильно отличается от $srcFileAbsPath
	 * @param string $srcFileAbsPath
	 * @param string $simplifiedFileAbsPath
	 * @param string $tmpAbsPath
	 * @throws SimplifyException
	 */
	protected function _checkSvgDifference($srcFileAbsPath, $simplifiedFileAbsPath, $tmpAbsPath)
	{
		$metricThreshold = \Yii::app()->params['compare_svg_metric_threshold']; // граничное значение метрики различия изображений
		$compareTool = new SvgTool(array(
			'verbose' => $this->_flags['verbose'],
			'background-color' => '#ff00ff',
			'transparent' => false,
		), $this->_binaries);

		$metric = $compareTool->compareSvgFiles($srcFileAbsPath, $simplifiedFileAbsPath, 100, $tmpAbsPath);
		if (false === $metric)
		{
			$reportFails[] = 'Difference metric not calculated for files ' . $srcFileAbsPath . ' and ' . $simplifiedFileAbsPath;
			throw new SimplifyException('Simplified image is broken: image difference metric not calculated');
		} else
		{
			if (is_numeric($metric))
			{
				if ($metric > $metricThreshold)
				{
					$this->_log[] = 'Simplified file ' . $simplifiedFileAbsPath . ' differ from original ' . $srcFileAbsPath . ' : image difference metric ' . $metric . ' > ' . $metricThreshold;
					throw new SimplifyException('Simplified image is broken: image difference metric ' . $metric . ' > ' . $metricThreshold);
				}
			} else {
				$this->_log[] = 'Simplified file ' . $simplifiedFileAbsPath . ' differ from original ' . $srcFileAbsPath . ': ' . $metric;
				throw new SimplifyException('Simplified image is broken: ' . $metric);
			}
		}
	}

	protected function setWebBrowsingPath()
	{
		$this->webBrowsingSvgFolder = \Yii::getPathOfAlias('application') . '/../www';
		$this->webBrowsingSvgRelPath = '/svg/' . date("Ymd") . '/';
	}

	public function getWebBrowsingUrl()
	{
		if(!empty($this->webBrowsingFileUrl))
			return '(view file on ' . $this->webBrowsingFileUrl . ')';
		else
			return '';
	}

	protected function saveForWebBrowsing($tmpSrcPath)
	{
		$fileWebBrowsingRelPath = $this->webBrowsingSvgRelPath . $this->fileName . '_' . uniqid() . '.svg';
		$fileWebBrowsingFullPath = $this->webBrowsingSvgFolder . $fileWebBrowsingRelPath;
		\FileSystemHelper::mkdir($this->webBrowsingSvgFolder . $this->webBrowsingSvgRelPath);
		if(\FileSystemHelper::cp($tmpSrcPath, $fileWebBrowsingFullPath))
			$this->webBrowsingFileUrl = \Yii::app()->params['app_url'] . $fileWebBrowsingRelPath;
		else
			throw new \Exception('Can not create file for web browsing for broken simplified SVG (' . $fileWebBrowsingFullPath . ')');
	}

	protected function addPrettyLog($text)
	{
		$this->prettyLog = PHP_EOL . $text . PHP_EOL;
	}

	public function getPrettyLog()
	{
		if(!empty($this->prettyLog))
			return PHP_EOL . '----------------' . PHP_EOL . $this->prettyLog;
		else
			return '';
	}
}

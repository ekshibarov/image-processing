<?php

namespace icons8_image_tools;

abstract class AbstractTool
{
	protected $_binaries = array(
		'inkscape' => false,
	);

	protected $_flags = array(
		'verbose' => 0,
		'keep_files' => false,
	);

	protected $_log = array();

	public function setBinary($key, $value)
	{
		$this->_binaries[$key] = $value;
	}

	public function setBinaries($binaries)
	{
		$this->_binaries = array_merge($this->_binaries, $binaries);
	}

	public function setFlag($key, $value)
	{
		$this->_flags[$key] = $value;
	}

	public function setFlags($flags)
	{
		$this->_flags = array_merge($this->_flags, $flags);
	}

	/**
	 * @return array
	 */
	public function getLog()
	{
		return $this->_log;
	}

	/**
	 * @param string $binary
	 * @param array $arguments
	 * @param array $output byref output
	 * @param bool $timeout time to kill process
	 * @return bool
	 */
	public function run($binary, array $arguments = array(), array &$output = null, $timeout = false)
	{
		$return = 0;
		$output = array();
		$cmd = $binary;
		foreach ($arguments as $argument => $value)
		{
			if (!is_numeric($argument) && strlen($argument))
			{
				$cmd .= ' ' . $argument;
				if ($value !== null)
				{
					if (strncmp('=', substr($cmd, -1), 1))
					{
						$cmd .= ' ';
					}
					$cmd .= escapeshellarg($value);
				}
			} elseif (is_array($value))
			{
				foreach ($value as $arg)
				{
					$arg = trim($arg);
					if (strlen($arg))
					{
						$cmd .= ' ' . escapeshellarg($arg);
					}
				}
			} elseif (strlen($value))
			{
				$cmd .= ' ' . $value;
			}
		}
		if (false !== $timeout)
			$cmd = 'timeout --signal=SIGKILL '.(int)$timeout.'s '.$cmd;

		\Yii::log('$ ' . $cmd, \CLogger::LEVEL_INFO);
		$this->_log[] = '$ > ' . $cmd;
		$timeMs = microtime(true);
		@exec($cmd, $output, $return);
		$output = preg_grep('@(^\s*$)|(inkscape\:\d+)|RANDR@', $output, PREG_GREP_INVERT); // костыль : удалить дурацкие сообщения от Inkscape
		foreach($output as $line)
			$this->_log[] = '$   < ' . $line;
		$this->_log[] = '$ done in ' . number_format(1000*(microtime(true) - $timeMs), 2, '.', '') . ' ms';
		return !$return;
	}
}
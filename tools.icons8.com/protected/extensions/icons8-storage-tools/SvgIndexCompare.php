<?php
namespace icons8_storage_tools;

/**
 * Class SvgIndexCompare
 * Сравнение индексов по SVG-файлам. При нахождении различий вызывает события удаление, добавление или изменение иконки
 *
 * @property mixed $onIconDeleted - свойство для события "удалена иконка"
 * @property mixed $onIconCreated - свойство для события "найдена новая иконка"
 * @property mixed onIconChanged - свойство для события "найдены изменения иконки"
 */
class SvgIndexCompare extends \CComponent
{
    /** @var \icons8_storage_tools\Indexer  */
    public $oldIndex;

    /** @var \icons8_storage_tools\Indexer  */
    public $newIndex;

    public function __construct(Indexer $oldIndex, Indexer $newIndex)
    {
        $this->oldIndex = $oldIndex;
        $this->newIndex = $newIndex;
    }

    public function compare()
    {
        $oldIndex = $this->oldIndex->getIndex();
        $newIndex = $this->newIndex->getIndex();

        // поиск удалённых иконок
        foreach($oldIndex as $iconName => $listByIcon) // сверяем старый индекс с новым
        {
            if (!is_array($listByIcon))
            {
                echo 'icon ', $iconName;
                var_export($listByIcon);
                exit;
            }
            foreach($listByIcon as $platform => $listByPlatform)
            {
                if (isset($listByPlatform['svg']) && is_array($listByPlatform['svg'])) // нашли svg
                {
                    if(!isset($newIndex[$iconName][$platform]['svg']) || !is_array($newIndex[$iconName][$platform]['svg'])) // в новом индексе нет соответствующего svg
                    {
                        foreach($listByPlatform['svg'] as $actualRelPath => $hash)
                        {
                            $event = new \CEvent($this, array(
                                'iconName' => $iconName,
                                'platform' => $platform,
                                'files' => $listByPlatform,
                                'actualRelPath' => $actualRelPath,
                                'deleteSim' => true,
                            ));
                            $this->onIconDeleted($event);
                        }
                    }
                    else // вот это когда срабатывает?
                    {
                        foreach($listByPlatform['svg'] as $actualRelPath => $hash)
                        {
                            if(!isset($newIndex[$iconName][$platform]['svg'][$actualRelPath]))
                            {
                                $event = new \CEvent($this, array(
                                    'iconName' => $iconName,
                                    'platform' => $platform,
                                    'files' => $listByPlatform,
                                    'actualRelPath' => $actualRelPath,
                                    'deleteSim' => true,
                                ));
                                $this->onIconDeleted($event);
                            }
                        }
                    }
                }
                if (isset($listByPlatform['sim']) && is_array($listByPlatform['sim'])) // нашли упрощенный svg
                {
                    if(!isset($newIndex[$iconName][$platform]['sim']) || !is_array($newIndex[$iconName][$platform]['sim'])) // в новом индексе нет соответствующего sim
                    {
                        if(isset($newIndex[$iconName][$platform]['svg']) && is_array($newIndex[$iconName][$platform]['svg'])) // а svg остался
                        {
                            foreach($listByPlatform['svg'] as $actualRelPath => $hash)
                            {
                                $event = new \CEvent($this, array(
                                    'actualRelPath' => $actualRelPath,
                                    'iconName' => $iconName,
                                    'platform' => $platform,
                                    'deleteSim' => true,
                                    'files' => $listByPlatform // все файлы с новыми путями
                                ));
                                //echo 'changed from sim compare' . PHP_EOL;
                                $this->onIconChanged($event); // заменить
                            }
                        }
                    }
                }
            }
        }

        // поиск новых и изменившихся иконок
        foreach ($newIndex as $iconName => $listByIcon) // сверяем новый индекс со старым
        {
            foreach ($listByIcon as $platform => $listByPlatform)
            {
                if (isset($listByPlatform['svg']) && is_array($listByPlatform['svg'])) // у нас есть svg!)
                {
                    if(isset($oldIndex[$iconName][$platform]['svg']) && is_array($oldIndex[$iconName][$platform]['svg'])) // в старом индексе найден соответствующий svg
                    {
                        foreach($listByPlatform['svg'] as $actualRelPath => $hash)
                        {
                            if(isset($oldIndex[$iconName][$platform]['svg'][$actualRelPath])) {
                                if($oldIndex[$iconName][$platform]['svg'][$actualRelPath] == $hash) // хеши совпали! :)
                                {
                                    if (isset($listByPlatform['sim']) && is_array($listByPlatform['sim'])) // нашли sim
                                    {
                                        $simChanged = false;
                                        if(isset($oldIndex[$iconName][$platform]['sim']) && is_array($oldIndex[$iconName][$platform]['sim'])) // в старом индексе найден соответствующий sim
                                        {
                                            foreach($listByPlatform['sim'] as $actualSimRelPath => $simHash)
                                            {
                                                if($oldIndex[$iconName][$platform]['sim'][$actualSimRelPath] != $simHash)
                                                {
                                                    $simChanged = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $simChanged = true;
                                        }
                                        if($simChanged)
                                        {
                                            $event = new \CEvent($this, array(
                                                'actualRelPath' => $actualRelPath,
                                                'iconName' => $iconName,
                                                'platform' => $platform,
                                                'deleteSim' => false,
                                                'files' => $listByPlatform // все файлы с новыми путями
                                            ));
                                            $this->onIconChanged($event); // заменить
                                        }
                                    }
                                }
                                else // хеши не совпали
                                {
                                    // при изменении иконки может быть как изменение оригинального файла SVG, так и пути к нему
                                    // в случае перноса в другую директорию нужно удалить все сгенерированные файлы со старыми путями
                                    // и сгенерировать заново с новыми путями
                                    $event = new \CEvent($this, array(
                                        'iconName' => $iconName,
                                        'platform' => $platform,
                                        'deleteSim' => false,
                                        'actualRelPath' => $actualRelPath,
                                        'files' => $oldIndex[$iconName][$platform], // все файлы со старыми путями
                                    ));
                                    $this->onIconDeleted($event); // удалить

                                    $event = new \CEvent($this, array(
                                        'actualRelPath' => $actualRelPath,
                                        'iconName' => $iconName,
                                        'platform' => $platform,
                                        'deleteSim' => false,
                                        'files' => $listByPlatform // все файлы с новыми путями
                                    ));
                                    $this->onIconChanged($event); // заменить
                                }
                            }
                            else
                            {
                                $event = new \CEvent($this, array(
                                    'iconName' => $iconName,
                                    'platform' => $platform,
                                    'actualRelPath' => $actualRelPath,
                                ));
                                $this->onIconCreated($event);
                            }
                        }
                    }
                    else // в старом индексе нет соответствующего svg
                    {
                        foreach($listByPlatform['svg'] as $actualRelPath => $hash)
                        {
                            $event = new \CEvent($this, array(
                                'iconName' => $iconName,
                                'platform' => $platform,
                                'actualRelPath' => $actualRelPath,
                            ));
                            $this->onIconCreated($event);
                        }
                    }
                }
                else // у нас нет svg(
                {
                    // ?
                }

            }
        }
    }

    public function onIconCreated($event)
    {
        $this->raiseEvent('onIconCreated', $event);
    }

    public function onIconDeleted($event)
    {
        $this->raiseEvent('onIconDeleted', $event);
    }

    public function onIconChanged($event)
    {
        $this->raiseEvent('onIconChanged', $event);
    }

} 
<?php

namespace icons8_storage_tools;

class Indexer
{

    /**
     * Путь к файлу результатов
     * Путь относительно директории /protected
     * @var string
     */
    public $indexPath = 'runtime/icons8-storage-files.php';

    /**
     * Путь к файлу шаблона результатов
     * Путь относительно директории /protected
     * @var string
     */
    public $indexTemplatePath = 'config/icons8-storage/files.php-dist';

    protected $_index = [];

    protected $_warnings = [];

    /**
     * Файлы по этой маске не обрабатывать
     * @var array
     */
    protected $_ignoreMasks = array(
        '/grids/',
        '/previews/',
        '/PSD/',
        '/Extra/',

        '/PSD Old/',
        '/SVG_for_Font_Old/',
        '/SVG_Crossed_Old/',
    );

    public function buildIndex($absPath)
    {
        $this->_index = array();
        $this->_warnings = array();

        foreach ($this->_ignoreMasks as &$mask)
        {
            $mask = '~' . preg_quote($mask) . '~i'; // маски без учёта регистра
        }

        $absPath = rtrim($absPath, '/');
        $absPathLength = strlen($absPath);

        // Рекурсивное сканирование директорий
        \Yii::log("Индексирование файлов в директории \"{$absPath}\" ...");
        $dir = dir($absPath);
        while (false !== ($subfolder = $dir->read()))
        {
            if ('.' == $subfolder || '..' == $subfolder)
                continue;

            if (false === ($platformApiCode = \Platform::getApiCodeBySubfolder($subfolder))) // если это не папка коллекции или платформы
                continue;

            $platformAbsPath = "{$absPath}/{$subfolder}";
            if (!is_dir($platformAbsPath)) // вдруг это файл?
                continue;

            \Yii::log("Индексирование директории \"{$platformAbsPath}\" для платформы \"{$platformApiCode}\" ...");
            $dirIterator = new \RecursiveDirectoryIterator($platformAbsPath);
            $fileIterator = new \RecursiveIteratorIterator($dirIterator);

            foreach ($fileIterator as $file)
            {
                /** @var $file \SplFileInfo */
                $fileName = trim($file->getFilename());
                if (empty($fileName) || '.' == $fileName{0})
                    continue;

                foreach ($this->_ignoreMasks as $quotedMask)
                {
                    if (preg_match($quotedMask, $file))
                    {
                        continue 2;
                    }
                }
                $absFilePath = (string)$file;
                $relFilePath = substr($absFilePath, $absPathLength + 1);
                $pathinfo = pathinfo($absFilePath);
                $baseIconName = $pathinfo['filename'];
                if (!isset($pathinfo['extension']))
                {
                    $message = "Файл \"{$file}\" не включен в индекс: отсутствует расширение";
                    $this->_warnings[] = $message;
                    continue;
                }
                $extension = strtolower($pathinfo['extension']);
                $iconName = strtolower($baseIconName);
                switch ($extension)
                {
                    case 'svg':
                        if (false !== strpos($relFilePath, '/SVG_Crossed/'))
                        {
                            $iconsIndexNode = &$this->_index[$iconName][$platformApiCode]['svg.crossed'];
                        } elseif (false !== strpos($relFilePath, '/SVG_for_Font/'))
                        {
                            $iconsIndexNode = &$this->_index[$iconName][$platformApiCode]['svg.simplified'];
                        } elseif (false !== strpos($relFilePath, '/SVG/'))
                        {
                            $iconsIndexNode = &$this->_index[$iconName][$platformApiCode]['svg'];
                        } else
                        {
                            $this->_warnings[] = [ "SVG image is in wrong folder", $relFilePath ];
                            continue 2;
                        }
                        $iconsIndexNode[$relFilePath] = md5_file($file);
                        break;

                    case 'sim':
                        if(false !== strpos($relFilePath, '/SVG/'))
                        {
                            $iconsIndexNode = &$this->_index[$iconName][$platformApiCode]['sim'];
                            $iconsIndexNode[$relFilePath] = md5_file($file);
                        } else
                        {
                            $this->_warnings[] = [ "Manually simplified SVG image (.sim) is in wrong folder", $relFilePath ];
                            continue 2;
                        }
                        break;

                    case 'png':
                        if (!preg_match('/^(.+)-(\d+)$/', $iconName, $matches)) // 'Android/PNG/128/Animals/cat-128.png
                        {
                            $this->_warnings[] = [ "PNG file has no size", $relFilePath ];
                            continue 2;
                        }
                        list(, $baseIconName, $size) = $matches;
                        $iconsIndexNode = &$this->_index[$baseIconName][$platformApiCode]['png'][$size];
                        $iconsIndexNode[$relFilePath] = true;
                        break;

                    case 'eps':
                    case 'pdf':
                        $iconsIndexNode = &$this->_index[$iconName][$platformApiCode][$extension];
                        $iconsIndexNode[$relFilePath] = true;
                        break;

                    default:
                        // коллекции, лицензии и другие сопутствующие файлы
                        break;
                }
            }
            \Yii::log(" ... индексирование директории \"{$platformAbsPath}\" для платформы \"{$platformApiCode}\" выполнено");
        }
        \Yii::log("... индексирование файлов в директории \"{$absPath}\" выполнено");

        ksort($this->_index);

        return empty($this->_warnings);
    }

    /**
     * @param string $indexPath
     * @param string $indexTemplatePath
     */
    public function save($indexPath = null, $indexTemplatePath = null)
    {
        if (!empty($this->_index) && (!empty($this->_warnings)))
        {
            \Yii::log('Index save failed: there are warnings during index build', \CLogger::LEVEL_ERROR);
            return;
        }

        if (is_null($indexPath))
            $indexPath = $this->indexPath;

        if (is_null($indexTemplatePath))
            $indexTemplatePath = $this->indexTemplatePath;

        $basePath = \Yii::app()->basePath;
        $indexAbsPath = "{$basePath}/{$indexPath}";
        if ($indexTemplatePath)
        {
            $indexTemplateAbsPath = "{$basePath}/{$indexTemplatePath}";
            if (is_file($indexTemplateAbsPath))
            {
                list($indexContent,) = explode('## index begin here', file_get_contents($indexTemplateAbsPath));
            } else
            {
                \Yii::log('Файл шаблона результатов "' . $indexTemplateAbsPath . '" не найден', \Clogger::LEVEL_ERROR);
                $indexContent = "<?php\n\n/* Do not modify this file */\n\n";
            }
        } else
        {
            \Yii::log('Путь к файлу шаблона результатов не указан', \Clogger::LEVEL_WARNING);
            $indexContent = "<?php\n\n/* Do not modify this file */\n\n";
        }

        // Сохранение карты файлов
        \Yii::log("Сохранение индекса в \"{$indexAbsPath}\" ...");
        $indexContent .= '## index begin here' . PHP_EOL . PHP_EOL;
        $indexContent .= 'return ';
        $indexContent .= var_export($this->_index, true);
        $indexContent .= ';' . PHP_EOL;

        file_put_contents($indexAbsPath, $indexContent);
        \Yii::log(' ... сохранение индекса выполнено');
    }

    public function load($indexPath = null)
    {
        if (is_null($indexPath))
            $indexPath = $this->indexPath;

        $basePath = \Yii::app()->basePath;
        $indexAbsPath = "{$basePath}/{$indexPath}";

        if (is_file($indexAbsPath))
        {
            $this->_index = include($indexAbsPath);
            if (!is_array($this->_index))
            {
                \Yii::log('Icon files map "' . $indexAbsPath . '" is empty', \CLogger::LEVEL_ERROR);
                $this->_index = array();
            }
        } else
        {
            \Yii::log('Icon files map "' . $indexAbsPath . '" does not exists', \CLogger::LEVEL_ERROR);
            $this->_index = array();
        }
    }

    /**
     * Возвращает всю информацию о файлах в виде итератора
     * @return \ArrayIterator
     */
    public function getIndex()
    {
        return new \ArrayIterator($this->_index);
    }

    /**
     * Возвращает файлы для указанной иконки и указанной платформы
     * Если платформа не указана, то возвращает все файлы этой иконки
     * @param $iconName
     * @param null $platform
     * @return array
     */
    public function getFiles($iconName, $platform = null)
    {
        if (isset($this->_index[$iconName]))
        {
            if ($platform)
                return isset($this->_index[$iconName][$platform]) ? $this->_index[$iconName][$platform] : [];
            return $this->_index[$iconName];
        }
        return [];
    }

    /**
     * @return mixed
     */
    public function getWarnings()
    {
        return $this->_warnings;
    }
}
<?php

namespace icons8_storage_tools;

/**
 * Содержит хендлеры для событий, вызываемых в IndexCompare
 * Class CompareEventHandler
 * @package icons8_storage_tools
 */
class CompareEventHandler
{
    public $storagePath = '';

    public $debug = false;

    public $redrawQueue = [];

    public $stats = [
        'processed' => 0,
        'deleted' => 0,
        'removed' => 0,
        'changed' => 0,
        'created' => 0,
        'queued' => 0,
    ];

    function __construct($storagePath)
    {
        $this->storagePath = $storagePath;
    }

    /**
     * @param \CEvent $event
     */
    public function createIcon($event)
    {
        if($event->sender instanceof SvgIndexCompare)
        {
            ++$this->stats['processed'];
            ++$this->stats['created'];
            $platform = $event->params['platform'];
            $iconName = $event->params['iconName'];
            $actualRelPath = $event->params['actualRelPath'];
            $this->log('create icon "' . $platform . '/' . $iconName . '" (delayed)');

            $this->redrawQueue[] = $actualRelPath;
            ++$this->stats['queued'];
        } else {
            \Yii::log('CompareEventHandler handle not SvgIndexCompare event', \CLogger::LEVEL_WARNING);
        }
    }

    /**
     * @param \CEvent $event
     */
    public function changeIcon($event)
    {
        if($event->sender instanceof SvgIndexCompare)
        {
            ++$this->stats['processed'];
            ++$this->stats['changed'];
            $platform = $event->params['platform'];
            $iconName = $event->params['iconName'];
            $this->log('change icon "' . $platform . '/' . $iconName . '"');
            $this->deleteIcon($event); // удалить новые файлы
            $this->createIcon($event); // сгенерировать файлы опять
        } else {
            \Yii::log('CompareEventHandler handle not SvgIndexCompare event', \CLogger::LEVEL_WARNING);
        }
    }

    /**
     * @param \CEvent $event
     */
    public function deleteIcon($event)
    {
        if($event->sender instanceof SvgIndexCompare)
        {
            ++$this->stats['processed'];
            ++$this->stats['deleted'];
            $platform = $event->params['platform'];
            $iconName = $event->params['iconName'];
            $deleteSim = $event->params['deleteSim'];
            $files = $event->params['files'];
            $this->log('delete icon "' . $platform . '/' . $iconName . '"');

            $count = 0;
            foreach($files as $type => $listByType)
            {
                switch($type)
                {
                    case 'svg': // SVG файл никогда не удалять - он управляется другими средствами
                        break;

                    case 'sim': // SIM удаляем только если удален SVG, передаем в параметре
                        if($deleteSim)
                        {
                            foreach($listByType as $fileRelPath => $hash)
                            {
                                \FileSystemHelper::rm_rf($this->storagePath . $fileRelPath);
                                $this->log('    removed file ' . $this->storagePath . $fileRelPath);
                                $count++;
                            }
                        }
                        break;

                    case 'png':
                        foreach ($listByType as $listBySize)
                        {
                            foreach ($listBySize as $fileRelPath => $hash)
                            {
                                \FileSystemHelper::rm_rf($this->storagePath . $fileRelPath);
                                $this->log('    removed file ' . $this->storagePath . $fileRelPath);
                                $count++;
                            }
                        }
                        break;

                    default:
                        foreach($listByType as $fileRelPath => $hash)
                        {
                            \FileSystemHelper::rm_rf($this->storagePath . $fileRelPath);
                            $this->log('    removed file ' . $this->storagePath . $fileRelPath);
                            $count++;
                        }
                        break;
                }
            }

            if ($count)
            {
                $this->actionNotifyZipper();
                $this->stats['removed'] += $count;
            }
        } else {
            \Yii::log('CompareEventHandler handle not SvgIndexCompare event', \CLogger::LEVEL_WARNING);
        }
    }

    public function actionNotifyZipper()
    {
        file_put_contents(\Yii::app()->params['repack_semaphore_path'], date('c'));
        $this->log('Установлен семафор ' . \Yii::app()->params['repack_semaphore_path'] . ' для перепаковки ZIP');
    }

    protected function log($message, $level = \CLogger::LEVEL_INFO)
    {
        if ($this->debug)
            echo $message, PHP_EOL;
        \Yii::log($message, $level);
    }
} 
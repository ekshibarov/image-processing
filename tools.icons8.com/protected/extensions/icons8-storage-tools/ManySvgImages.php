<?php

namespace icons8_storage_tools;

/**
 * Class ManySvgImages
 *
 * Ищет файлы SVG с одинаковыми именами в разных директориях
 */
class ManySvgImages
{
    /**
     * @var Indexer
     */
    protected $_indexer;

    protected $_advices;

    function __construct(Indexer $indexer)
    {
        $this->_indexer = $indexer;
    }

    public function getDescription()
    {
        return "Несколько файлов SVG с одинаковыми именами";
    }

    public function getAdviceList()
    {
        if (is_null($this->_advices))
        {
            $this->_advices = [];

            foreach($this->_indexer->getIndex() as $iconName => $listByIcon)
            {
                foreach($listByIcon as $platform => $listByPlatform)
                {
                    foreach($listByPlatform as $type => $listByType)
                    {
                        if ('svg' == $type && count($listByType)>1)
                        {
                            $this->_advices[] = array($iconName, $platform, $listByType);
                        }
                    }
                }
            }
        }
        return $this->_advices;
    }
}

<?php

namespace icons8_image_processing;

/**
 * Class CategoryPreviewGenerator
 * @package icons8_image_processing
 */
class CategorySharePreviewGenerator extends ImageGenerator
{
    const   WIDTH = 640,
            HEIGHT = 374;

    const   ICONS_COUNT = 10;

    public static $sizesAndPositions = [            // параметры иконок для превью:
        ['size' => 72, 'position' => [112, 97]],    // размер иконки и ее сдвиг по X и по Y
        ['size' => 48, 'position' => [291, 108]],
        ['size' => 50, 'position' => [480, 118]],
        ['size' => 53, 'position' => [38, 192]],
        ['size' => 61, 'position' => [216, 185]],
        ['size' => 69, 'position' => [379, 166]],
        ['size' => 42, 'position' => [562, 186]],
        ['size' => 60, 'position' => [127, 249]],
        ['size' => 50, 'position' => [317, 247]],
        ['size' => 50, 'position' => [474, 225]],
    ];

    public static $backgroundPalette = [
        'light-red'     => '#FF6333',
        'dark-red'      => '#D4522A',
        'light-orange'  => '#FF9A4C',
        'dark-orange'   => '#C7783C',
//        'light-yellow'  => '#FFDD33',
        'dark-yellow'   => '#F5BC20',
        'light-lime'    => '#79DD15',
        'dark-lime'     => '#4BB73D',
        'light-green'   => '#32C24C',
        'dark-green'    => '#319E45',
        'light-blue'    => '#2AC8F7',
        'dark-blue'     => '#00A4D6',
        'light-cyan'    => '#3A7DFF',
        'dark-cyan'     => '#1E2EDE',
        'light-purple'  => '#A767A0',
        'deep-purple'   => '#734C6E',
        'light-dove'    => '#3A606E',
        'dark-dove'     => '#2A454F'
    ];

    public static $platforms = [
        'Android_L',
        'Android',
        'iOS7',
        'windows8'
    ];

    public $tempCategoryPath;
    public $tempRecoloredPath;
    public $tempPNGPath;

    /* Путь к файлу шрифта относительно /protected */
    public $font = 'config/share-picture/helveticaneuecyr-roman.otf';

    /* Размер шрифта для заголовка */
    public $titleFontSize = 24;

    /* Размер шрифта надписи снизу */
    public $bottomFontSize = 16;

    public $categoryName;
    public $categoryApiCode;

    function __construct()
    {
        if (!class_exists('\Imagick', false))
            throw new \Exception('Class \Imagick not found');

        parent::__construct();
    }

    /**
     * @param array $categoryArray
     *
    $categoryArray = {
    icons:[
    {svg:<file>},
    {svg:<file>},
    {svg:<file>}
    ],
    name:<название категории>
    api_code:<api код>
    }
     * @return string $previewAbsPath
     */
    public function export($categoryArray)
    {
        $this->categoryName = $categoryArray['name'];
        $this->categoryApiCode = str_replace(' ', '_', $categoryArray['api_code']);

        $this->log('- drawing preview for ' . $this->categoryName . ' category -');

        $iconsRelPathsArray = [];
        $this->tempCategoryPath = $this->tempPath . 'category/';
        $this->tempRecoloredPath = $this->tempCategoryPath . 'recolored/';
        $this->tempPNGPath = $this->tempCategoryPath . 'PNG/';
        $previewDirAbsPath = $this->storagePath . 'Share/category/';
        $previewAbsPath = false;
        \FileSystemHelper::mkdir($previewDirAbsPath);
        \FileSystemHelper::mkdir($this->tempPNGPath);
        \FileSystemHelper::mkdir($this->tempRecoloredPath);

        foreach($categoryArray['icons'] as $iconArray)
            $iconsRelPathsArray[] = $iconArray['svg'];

        foreach($iconsRelPathsArray as $iconRelPath)
        {
            $iconsAbsPath = rtrim($this->storagePath, '/') . '/' . $iconRelPath;
            if (is_file($iconsAbsPath))
                $iconsAbsPathArray[] = $iconsAbsPath;
        }

        if (!empty($iconsAbsPathArray))
        {
            $previewAbsPath = $previewDirAbsPath . $this->categoryApiCode . '.png';
            $recoloredIcons = $this->recolorSVGs($iconsAbsPathArray);
            $pngFiles = $this->createPNGs($recoloredIcons);
            $tempPreview = $this->drawPreview($pngFiles);
            \FileSystemHelper::cp($tempPreview, $previewAbsPath);
            $this->log('preview successfully created and saved to ' . $previewAbsPath);
        }
        else
        {
            $this->log('There is no SVG files to create category preview for category "' . $this->categoryName . '"');
        }
        return $previewAbsPath;
    }

    protected function recolorSVGs($svgFiles)
    {
        $this->log('recoloring SVGs');
        $recoloredSVGs = [];
        $counter = 0;
        foreach($svgFiles as $svgFile)
        {
            $recolorTool = new \icons8_image_tools\SvgRecolorTool();
            $recolored = $recolorTool->recolorImage($svgFile, '#ffffff');
            if($recolored)
            {
                $recoloredSvgAbsPath = $this->tempRecoloredPath . $this->categoryApiCode . $counter . '.svg';
                $recolorTool->save($recoloredSvgAbsPath);
                $recoloredSVGs[] = $recoloredSvgAbsPath;
                $counter++;
            }
        }
        return $recoloredSVGs;
    }

    protected function createPNGs($svgFiles)
    {
        $this->log('creating PNGs');
        $pngFiles = [];

        $svgTool = new \icons8_image_tools\SvgTool(array(
            'verbose' => $this->config['verbose'],
            'background-color' => '#26c445',
            'transparent' => true,
        ), $this->config['binaries']);

        $sizesAndPositions = self::$sizesAndPositions;
        foreach($svgFiles as $key => $svgFile)
        {
            $pngAbsPath = $this->tempPNGPath . $key . '.png';

            $size = current($sizesAndPositions)['size'];
            if ($svgTool->exportPng($svgFile, $pngAbsPath, $size))
            {
                $pngFiles[] = $pngAbsPath;
                next($sizesAndPositions);
            } else
            {
                $this->log('export to PNG ' . $pngAbsPath . ' failed', \CLogger::LEVEL_WARNING);
                break;
            }
        }
        return $pngFiles;
    }

    protected function drawPreview($pngFiles)
    {
        $this->log('drawing preview...');
        $image = new \Imagick();
        $image->setResourceLimit(6, 1); // thread limit = 1 for prevent the crash

        $image->newImage(self::WIDTH, self::HEIGHT, new \ImagickPixel(self::$backgroundPalette[array_rand(self::$backgroundPalette)])); // общая подложка
        $image->setImageFormat('png');

        $this->addTitleText($image);
        $this->addIconsToPreview($pngFiles, $image);
        $this->addBottomText($image);

        $tempPreviewPath = $this->tempCategoryPath . $this->categoryName . '.png';
        file_put_contents($tempPreviewPath, $image);

        return $tempPreviewPath;
    }

    /**
     * @param \Imagick $image
     * @return \ImagickDraw
     */
    protected function addTitleText(&$image)
    {
        $this->log('    adding title');
        $titleText = ucwords(str_replace('_', ' ', $this->categoryName)) . ' Icons in Any Format, Size and Color';

        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->font);
        $draw->setFontWeight(200);
        $draw->setFontSize($this->titleFontSize);
        $draw->setFillColor(new \ImagickPixel('white'));
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);

        $metrics = $image->queryFontMetrics($draw, $titleText);
        $textMarginX = (self::WIDTH - $metrics['textWidth']) / 2; // левый отступ - отцентрировали по X
        $textMarginY = 58; // фиксировали положение по Y

        $draw->annotation($textMarginX, $textMarginY, $titleText);
        $image->drawImage($draw); // внесли надпись
    }

    /**
     * @param \Imagick $image
     * @return \ImagickDraw
     */
    protected function addBottomText(&$image)
    {
        $this->log('    adding bottom text');
        $bottomText = '20,000 icons more at ';
        $bottomLinkText = 'icons8.com';

        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->font);
        $draw->setFontWeight(200);
        $draw->setFontSize($this->bottomFontSize);
        $draw->setFillColor(new \ImagickPixel('white'));
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);

        $metrics = $image->queryFontMetrics($draw, $bottomText);
        $bottomTextWidth = $metrics['textWidth'];
        $bottomTextX = 205;
        $bottomTextY = 337;
        $bottomLinkTextX = $bottomTextWidth + $bottomTextX;

        $draw->annotation($bottomTextX, $bottomTextY, $bottomText);
        $image->drawImage($draw);// нарисовали текст без подчеркивания в нижней надписи

        $draw->setTextDecoration(\Imagick::DECORATION_UNDERLINE);
        $draw->annotation($bottomLinkTextX, $bottomTextY, $bottomLinkText);
        $image->drawImage($draw); // нарисовали текст с подчеркиванием в нижней надписи
    }

    /**
     * @param $pngFiles
     * @param \Imagick $image
     * @return \Imagick
     */
    protected function addIconsToPreview($pngFiles, &$image)
    {
        $this->log('    adding icons');
        $sizesAndPositions = self::$sizesAndPositions;
        foreach($pngFiles as $pngFile)
        {
            $position = current($sizesAndPositions)['position'];
            $image->compositeImage(new \Imagick($pngFile), \Imagick::COMPOSITE_DEFAULT, $position[0], $position[1]);
            next($sizesAndPositions);
        }
    }

    protected function optimizePreview($previewPath)
    {
        $this->log('optimizing preview...');
        $pngTool = new \icons8_image_tools\PngTool(array(
            'verbose' => $this->config['verbose'],
            'strategy' => $this->config['strategy'],
            'multipass' => $this->config['multipass'],
            'transparent' => false,
        ), $this->config['binaries']);
        return $pngTool->optimize(array($previewPath));
    }

    protected function createIconArrayByName($iconAbsPathArray)
    {
        $resultArray = [];
        foreach($iconAbsPathArray as $iconAbsPath)
        {
            $fileName = strtolower(end(explode('/', $iconAbsPath)));
            $resultArray[$fileName] = $iconAbsPath;
        }
        return $resultArray;
    }

    // перемешиваем массив, сохраняя ключи
    protected function shuffleAssoc($array)
    {
        $shuffledArray = [];

        $shuffledKeys = array_keys($array);
        shuffle($shuffledKeys);

        foreach ($shuffledKeys as $shuffledKey)
        {
            $shuffledArray[$shuffledKey] = $array[$shuffledKey];
        }
        return $shuffledArray;
    }

} 
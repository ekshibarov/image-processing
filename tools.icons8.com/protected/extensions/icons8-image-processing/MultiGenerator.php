<?php

namespace icons8_image_processing;

use icons8_image_tools\SvgTool;

class MultiGenerator extends ImageGenerator
{
    const FORMAT_EPS = "eps";

    /**
     * Convert file from SVG to other formats
     * @param  string $svg Base64 encoded SVG
     * @param  array $sizes Size of output icon. Ex. for EPS: [1600]
     * @param  array $formats Image formats. Ex. for EPS: ["eps"]
     * @return array Array with base64 encoded images
     */
    public function convertFromSvg($svg, $sizes, $formats)
    {
        $svg = (strpos($svg, '<svg') !== false) ? $svg : base64_decode($svg);

        // Save encoded SVG to file with unique name
        $tmpDir = \Yii::app()->params['tmp_path'];
        $fileId = uniqid('', true);
        $svgFile = rtrim($tmpDir, '/') . '/' . $fileId . '.svg';
        file_put_contents($svgFile, $svg);

        $files = [];

        // Convert to each format
        foreach ($formats as $format) {
            switch ($format) {
                case self::FORMAT_EPS:
                    $files[self::FORMAT_EPS] = [];
                    /**
                     * @var SvgTool $svgTool
                     */
                    $svgTool = new \icons8_image_tools\SvgTool([], $this->config['binaries']);

                    // Convert file for each size
                    foreach ($sizes as $size) {
                        $epsFile = rtrim($tmpDir, '/') . '/' . $fileId . "_$size.eps";
                        $this->log('Export to EPS ' . $epsFile . ' ...');

                        // If convert success, read and encode file as base64
                        if ($svgTool->exportEps($svgFile, $epsFile, $size))
                        {
                            $handle = fopen($epsFile, "rb");
                            $contents = fread($handle, filesize($epsFile));
                            fclose($handle);
                            $files[self::FORMAT_EPS][$size] = base64_encode($contents);

                            // Delete file after encoding encoding
                            unlink($epsFile);
                            $this->log('... export to EPS ' . $epsFile . '_' . $size . '.eps success');
                        } else
                        {
                            \Yii::log('Export to EPS ' . $epsFile . '_' . $size . '.eps failed', \CLogger::LEVEL_WARNING, 'images_generator');
                            return null;
                        }
                    }
                    break;
            }
        }

        // Delete SVG file after convert
        unlink($svgFile);
        return $files;
    }
}

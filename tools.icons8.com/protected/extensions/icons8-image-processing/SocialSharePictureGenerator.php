<?php

namespace icons8_image_processing;

/**
 * Class SocialSharePictureGenerator
 * @package icons8_image_processing
 */
class SocialSharePictureGenerator extends ImageGenerator
{

    public $bigIconSize = 320;

    public $smallIconSize = 70;

    /* Путь к файлу шрифта относительно /protected */
    public $font = 'config/share-picture/helveticaneuecyr-roman.otf';

    /* Цвет фона */
    public $backgroundColor = '#32c24d';

    /* Ширина изображения */
    public $imageWidth = 480;

    /* Высота изображения */
    public $imageHeight = 480;

    public $smallImagesDistance = 18;

    public static $maxIconsCount = 6;

    public $iconsColor = '#ffffff';

    public $iconName;

    public static $platformsOrder = [   // порядок, в котором нужно выстроить иконки платформ
        \Platform::API_CODE_IOS7 => '',
        \Platform::API_CODE_WINDOWS8 => '',
        \Platform::API_CODE_WINDOWS10 => '',
        \Platform::API_CODE_ANDROID => '',
        \Platform::API_CODE_ANDROID_L => '',
        \Platform::API_CODE_COLOR => '',
        \Platform::API_CODE_OFFICE => ''
    ];

    function __construct()
    {
        if (!class_exists('\Imagick', false))
            throw new \Exception('Class \Imagick not found');
        parent::__construct();
    }

    /**
     * @param array $iconsArray
     * @return string|false $googlePictureFileAbsPath
    {
    icons:[
    {id:1,platform:ios,svg:<file>,link:<ic8.link>},
    {id:2,platform:ios,svg:<file>,link:<ic8.link>},
    {id:3,platform:ios,svg:<file>,link:<ic8.link>}
    ],
    name:<название иконки>
    canonical_name:<человекопонятное название иконки>
    }
     */
    public function export($iconsArray)
    {
        $this->log('- drawing Social preview for ' .  $iconsArray['name'] . ' icon -');
        $socialPictureFileAbsPath = false;
        $this->iconName = $iconName = $iconsArray['name'];

        $tempPath = $this->tempPath . 'social/';
        \FileSystemHelper::mkdir($tempPath);

        $iconsRelPathsArray = [];

        $this->iconName = $iconsArray['name'];
        foreach($iconsArray['icons'] as $iconArray)
            $iconsRelPathsArray[$iconArray['platform']] = $iconArray['svg'];

        $reorderedRelPathsArray = self::reorderIcons($iconsRelPathsArray);
        foreach($reorderedRelPathsArray as $platform => $iconRelPath)
        {
            $iconsAbsPath = rtrim($this->storagePath, '/') . '/' . $iconRelPath;
            if (is_file($iconsAbsPath))
                $iconsAbsPathArray[$platform] = $iconsAbsPath;
        }

        if (!empty($iconsAbsPathArray))
        {
            $recoloredIcons = $this->recolorIcons($iconsAbsPathArray);
            $exportedPNGs = $this->exportPNGs($recoloredIcons);
            $image = $this->drawImage($exportedPNGs);

            $socialPictureAbsPath = $this->storagePath . 'Share/social/' . $iconName{0} . '/';
            $socialPictureFileAbsPath = $socialPictureAbsPath . $iconName . '.png';

            \FileSystemHelper::mkdir($socialPictureAbsPath);
            file_put_contents($socialPictureFileAbsPath, $image);

            $pngTool = new \icons8_image_tools\PngTool(array(
                'verbose' => $this->config['verbose'],
                'strategy' => $this->config['strategy'],
                'multipass' => $this->config['multipass'],
                'transparent' => false,
            ), $this->config['binaries']);
            //$pngTool->optimize(array($socialPictureFileAbsPath));

            $this->log('Social Share picture for icon "' . $iconName . '" exported to ' . $socialPictureFileAbsPath);

        }

        return $socialPictureFileAbsPath;
    }

    protected function exportPNGs($recoloredIcons)
    {
        $exportedPNGs = array();
        $tempPngPath = $this->tempPath . 'social/PNG/';
        \FileSystemHelper::mkdir($tempPngPath);

        $svgTool = new \icons8_image_tools\SvgTool(array(
            'verbose' => $this->config['verbose'],
            'background-color' => $this->backgroundColor,
            'transparent' => true,
        ), $this->config['binaries']);

        $first = true;
        foreach ($recoloredIcons as $platform => $recoloredSvg)
        {
            $size = ($first) ? $this->bigIconSize : $this->smallIconSize; // первая - большая
            $pngAbsPath = $tempPngPath . $this->iconName . '_' . $platform . '.png';
            if ($svgTool->exportPng($recoloredSvg, $pngAbsPath, $size))
            {
                $exportedPNGs[$platform] = $pngAbsPath;
                $this->log(' ... export to PNG ' . $this->iconName . '.png success');
            } else
            {
                $this->log('Export to PNG ' . $this->iconName . '.png failed', \CLogger::LEVEL_WARNING);
                break;
            }
            $first = false;
        }
        return $exportedPNGs;
    }

    public function drawImage($exportedPngs)
    {
        $bigIconMarginLeft = ($this->imageWidth - $this->bigIconSize) / 2;
        $count = count($exportedPngs) - 1; // количество маленьких картинок снизу
        if($count > 4)
            $this->smallImagesDistance = 12;

        $image = new \Imagick();
        $image->setResourceLimit(6, 1); // 6 means THREAD_LIMIT (unfortunately, there is no such constant defined, so u have to use this integer value);
        $image->newimage($this->imageWidth, $this->imageHeight, new \ImagickPixel($this->backgroundColor)); // рисуем зеленое поле
        $image->setImageFormat('png');

        $firstIconMargin = ($this->imageWidth - ($this->smallIconSize * $count) - (($count - 1) * $this->smallImagesDistance)) / 2;
        $counter = 0;
        if (count($exportedPngs) > 1) // рисуем вариант с большой по центру и маленькими снизу
        {
            $bigIconMarginTop = 25;
            foreach ($exportedPngs as $platform => $png)
            {
                if ($counter == 0) // первая картинка - большая
                {
                    $image->compositeImage(new \Imagick($png), \Imagick::COMPOSITE_DEFAULT, $bigIconMarginLeft, $bigIconMarginTop);
                } else
                {
                    $colorExcess = $addMarginColor = 0;
                    $topMargin = 372;
                    $leftMargin = $firstIconMargin + (($counter-1) * $this->smallIconSize) + (($counter-1) * $this->smallImagesDistance);
                    if ($platform == \Platform::API_CODE_COLOR) // если иконка цветная - рисуем фон
                    {
                        $addMarginColor = 5;
                        $colorExcess = 3;
                        $drawBackground = new \ImagickDraw();
                        $drawBackground->setFillColor(new \ImagickPixel('white'));
                        $drawBackground->circle($leftMargin + floor($this->smallIconSize / 2) + $colorExcess + $addMarginColor, $topMargin + ($this->smallIconSize / 2), $leftMargin + $colorExcess + $addMarginColor, $topMargin - $colorExcess);
                        $image->drawImage($drawBackground);

                    }
                    $image->compositeImage(new \Imagick($png), \Imagick::COMPOSITE_OVER, $leftMargin  + $colorExcess + $addMarginColor, $topMargin);
                }
                $counter++;
            }
        } else // рисуем вариант с одной большой по центру
        {
            if (key($exportedPngs) == \Platform::API_CODE_COLOR) // если иконка цветная - рисуем фон
            {
                $addMargin = 5;
                $drawBackground = new \ImagickDraw();
                $drawBackground->setFillColor(new \ImagickPixel('white'));
                $drawBackground->circle($this->imageWidth / 2, $this->imageHeight / 2, (($this->imageWidth - $this->bigIconSize) / 2) - $addMargin, (($this->imageHeight - $this->bigIconSize) / 2) + $addMargin);
                $image->drawImage($drawBackground);

            }
            $imagickPng = new \Imagick(current($exportedPngs));
            $image->compositeImage($imagickPng, \Imagick::COMPOSITE_DEFAULT, ($this->imageWidth - $this->bigIconSize) / 2,  ($this->imageHeight - $this->bigIconSize) / 2);
        }

        return $image;
    }

    protected static function reorderIcons($iconsPathsArray)
    {
        $reorderedPathsArray = self::$platformsOrder;
        foreach($iconsPathsArray as $platformCode => $iconPath)
        {
            $reorderedPathsArray[$platformCode] = $iconPath;
        }
        $reorderedPathsArray = array_filter($reorderedPathsArray);
        if(count($reorderedPathsArray) > self::$maxIconsCount)
            unset($reorderedPathsArray[\Platform::API_CODE_WINDOWS8]);
        return array_filter($reorderedPathsArray);
    }

    public function recolorIcons($iconsAbsPathsArray)
    {
        $recoloredIcons = [];
        $recolorTool = new \icons8_image_tools\SvgRecolorTool();
        foreach($iconsAbsPathsArray as $platform => $iconAbsPath)
        {
            if($platform != \Platform::API_CODE_COLOR && $platform != \Platform::API_CODE_OFFICE)
            {
                $recolorTool->recolorImage($iconAbsPath, $this->iconsColor);
                $recoloredSvgAbsPath = $this->tempPath . $platform . '_' . $this->iconName . '.svg';
                $recolorTool->save($recoloredSvgAbsPath);
                $recoloredIcons[$platform] = $recoloredSvgAbsPath;

            }
            else
                $recoloredIcons[$platform] = $iconAbsPath;
        }
        return $recoloredIcons;
    }

    /**
     * Удаляет картинку для указанной иконки
     * @param $iconName
     * @return bool
     */
    public function remove($iconName)
    {
        $socialPictureAbsPath = $this->storagePath . 'Share/social/' . $iconName{0} . '/';
        $socialPictureFileAbsPath = $socialPictureAbsPath . $iconName . '.png';
        $retValue = \FileSystemHelper::rm_rf($socialPictureFileAbsPath);
        $this->log('Social picture for icon "' . $iconName . '" is removed from ' . $socialPictureFileAbsPath);
        return $retValue;
    }
} 
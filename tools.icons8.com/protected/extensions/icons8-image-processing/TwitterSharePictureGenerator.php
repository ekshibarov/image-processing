<?php

namespace icons8_image_processing;

/**
 * Class TwitterSharePictureGenerator
 * @package icons8_image_processing
 */
class TwitterSharePictureGenerator extends ImageGenerator
{

    public $iconName;

    /* Путь к файлу шрифта относительно /protected */
    public $font = 'config/share-picture/helveticaneuecyr-roman.otf';
    public $fontThin = 'config/share-picture/helveticaneuecyr-thin.otf';

    public static $imageParams = [
        'iconSize' => 100,              // размер иконок
        'topFontSize' => 38,            // размер шрифта верхней надписи
        'topFontColor' => '#02AB2C',    // цвет шрифта верхней надписи
        'bottomFontSize' => 16,         // размер шрифта нижней надписи
        'backgroundColor' => '#ffffff', // цвет фона
        'iconsColor' => '#4A4A4A',      // цвет иконок
        'imageHeight' => 495,           // высота всего превью
        'imageWidth' => 572,            // ширина всего превью
        'iconsDistance' => 32,          // расстояние между иконками
        'marginTopSingle' => 180,       // отступ сверху для ряда иконок, когда он один
        'marginTopFirst' => 126,        // отступ сверху для первого ряда, когда их два
        'marginTopSecond' => 255,       // отступ сверху для второго ряда, когда их два
        'maxIconsCount' => 6
    ];

    public static $platformsOrder = [   // порядок, в котором нужно выстроить иконки платформ
        \Platform::API_CODE_IOS7 => '',
        \Platform::API_CODE_WINDOWS8 => '',
        \Platform::API_CODE_WINDOWS10 => '',
        \Platform::API_CODE_ANDROID => '',
        \Platform::API_CODE_ANDROID_L => '',
        \Platform::API_CODE_COLOR => '',
        \Platform::API_CODE_OFFICE => ''
    ];

    function __construct()
    {
        if (!class_exists('\Imagick', false))
            throw new \Exception('Class \Imagick not found');
        parent::__construct();
    }

    /**
     * @param array $iconsArray
     * @return string|false $twitterPictureFileAbsPath
    {
    icons:[
    {id:1,platform:ios,svg:<file>,link:<ic8.link>},
    {id:2,platform:ios,svg:<file>,link:<ic8.link>},
    {id:3,platform:ios,svg:<file>,link:<ic8.link>}
    ],
    name:<название иконки>
    canonical_name:<человекопонятное название иконки>
    }
     */
    public function export($iconsArray)
    {
        $this->log('- drawing Twitter preview for ' .  $iconsArray['name'] . ' icon -');
        $twitterPictureFileAbsPath = false;
        $this->tempPath = $this->tempPath . 'twitter/';
        \FileSystemHelper::mkdir($this->tempPath);

        $iconsRelPathsArray = [];

        $this->iconName = $iconsArray['name'];
        foreach($iconsArray['icons'] as $iconArray)
            $iconsRelPathsArray[$iconArray['platform']] = $iconArray['svg'];

        $reorderedRelPathsArray = self::reorderIcons($iconsRelPathsArray);
        foreach($reorderedRelPathsArray as $platform => $iconRelPath)
        {
            $iconsAbsPath = rtrim($this->storagePath, '/') . '/' . $iconRelPath;
            if (is_file($iconsAbsPath))
                $iconsAbsPathArray[$platform] = $iconsAbsPath;
        }
        if (!empty($iconsAbsPathArray))
        {
            $recoloredIcons = $this->recolorIcons($iconsAbsPathArray);
            $exportedPNGs = $this->exportPNGs($recoloredIcons);
            $image = $this->drawImage($exportedPNGs, $iconsArray);

            $twitterPictureAbsPath = $this->storagePath . 'Share/twitter/' . $this->iconName{0} . '/';
            $twitterPictureFileAbsPath = $twitterPictureAbsPath . $this->iconName . '.png';

            \FileSystemHelper::mkdir($twitterPictureAbsPath);
            file_put_contents($twitterPictureFileAbsPath, $image);

            $pngTool = new \icons8_image_tools\PngTool(array(
                'verbose' => $this->config['verbose'],
                'strategy' => $this->config['strategy'],
                'multipass' => $this->config['multipass'],
                'transparent' => false,
            ), $this->config['binaries']);
            //$pngTool->optimize(array($twitterPictureFileAbsPath));

            $this->log('Twitter picture for icon "' . $this->iconName . '" exported to ' . $twitterPictureFileAbsPath);
        }
        else
        {
            $this->log('There is no SVG files to create Twitter share preview for icon "' . $this->iconName . '"');
        }
        return $twitterPictureFileAbsPath;
    }

    protected function exportPNGs($recoloredIcons)
    {
        $exportedPNGs = array();
        $tempPngPath = $this->tempPath . 'twitter/PNG/';

        $svgTool = new \icons8_image_tools\SvgTool(array(
            'verbose' => $this->config['verbose'],
            'background-color' => self::$imageParams['backgroundColor'],
            'transparent' => true,
        ), $this->config['binaries']);

        foreach ($recoloredIcons as $platform => $recoloredSvg)
        {
            $size = self::$imageParams['iconSize'];
            $pngAbsPath = $tempPngPath . $this->iconName . '_' . $platform . '.png';
            if ($svgTool->exportPng($recoloredSvg, $pngAbsPath, $size))
            {
                $exportedPNGs[$platform] = $pngAbsPath;
                $this->log(' ... export to PNG ' . $this->iconName . '.png success');
            } else
            {
                $this->log('Export to PNG ' . $this->iconName . '.png failed', \CLogger::LEVEL_WARNING);
                break;
            }
        }
        return $exportedPNGs;
    }

    protected static function reorderIcons($iconsPathsArray)
    {
        $reorderedPathsArray = self::$platformsOrder;
        foreach($iconsPathsArray as $platformCode => $iconPath)
        {
            $reorderedPathsArray[$platformCode] = $iconPath;
        }
        $reorderedPathsArray = array_filter($reorderedPathsArray);
        if(count($reorderedPathsArray) > self::$imageParams['maxIconsCount'])
            unset($reorderedPathsArray[\Platform::API_CODE_WINDOWS8]);
        return array_filter($reorderedPathsArray);
    }

    protected function drawImage($exportedPngs, $iconsArray)
    {
        $image = new \Imagick();
        $image->setResourceLimit(6, 1); // 6 means THREAD_LIMIT (unfortunately, there is no such constant defined, so u have to use this integer value);
        $image->newimage(self::$imageParams['imageWidth'],
            self::$imageParams['imageHeight'],
            new \ImagickPixel(self::$imageParams['backgroundColor']) // рисуем белое поле
        );
        $image->setImageFormat('png');

        switch(count($exportedPngs))
        {
            case 1: // одна иконка по центру
                $left = (self::$imageParams['imageWidth'] - self::$imageParams['iconSize']) / 2;
                $top = self::$imageParams['marginTopSingle'];
                $image->compositeImage(new \Imagick(current($exportedPngs)), \Imagick::COMPOSITE_DEFAULT, $left, $top);
                break;
            case 2:
            case 3:
                $top = self::$imageParams['marginTopSingle'];
                $left = (self::$imageParams['imageWidth'] - ((self::$imageParams['iconsDistance'] * (count($exportedPngs) - 1)) +
                            (self::$imageParams['iconSize'] * count($exportedPngs)))) / 2;
                foreach($exportedPngs as $png)
                {
                    $image->compositeImage(new \Imagick($png), \Imagick::COMPOSITE_DEFAULT, $left, $top);
                    $left += (self::$imageParams['iconsDistance'] + self::$imageParams['iconSize']);
                }
                break;
            case 4:
            case 5:
            case 6:
                $counter = 1;
                $firstRowCount = floor(count($exportedPngs) / 2);
                $secondRowCount = count($exportedPngs) - $firstRowCount;
                $leftFirstRow = (self::$imageParams['imageWidth'] -
                        ((self::$imageParams['iconsDistance'] * ($firstRowCount - 1)) +
                            (self::$imageParams['iconSize'] * $firstRowCount)))
                    / 2;
                $leftSecondRow =  (self::$imageParams['imageWidth'] -
                        ((self::$imageParams['iconsDistance'] * ($secondRowCount - 1)) +
                            (self::$imageParams['iconSize'] * $secondRowCount)))
                    / 2;
                foreach($exportedPngs as $png)
                {
                    if($counter <= $firstRowCount) // 1 ряд иконок
                    {
                        $top = self::$imageParams['marginTopFirst'];
                        $image->compositeImage(new \Imagick($png), \Imagick::COMPOSITE_DEFAULT, $leftFirstRow, $top);
                        $leftFirstRow += (self::$imageParams['iconsDistance'] + self::$imageParams['iconSize']);
                    }
                    else
                    {
                        $top = self::$imageParams['marginTopSecond'];
                        $image->compositeImage(new \Imagick($png), \Imagick::COMPOSITE_DEFAULT, $leftSecondRow, $top);
                        $leftSecondRow += (self::$imageParams['iconsDistance'] + self::$imageParams['iconSize']);
                    }
                    $counter++;
                }
                break;
        }

        $this->drawTopText($image, $iconsArray);
        $this->drawBottomText($image, $iconsArray);
        return $image;
    }

    public function drawBottomText(\Imagick &$image, $iconsArray)
    {
        $linkMarginTop = 423; // расположение ссылки по Y
        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->font);
        $draw->setFontWeight(200);
        $draw->setFillColor(new \ImagickPixel('black'));
        $draw->setFontSize(self::$imageParams['bottomFontSize']);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true); // задали параметры шрифта для надписи
        $draw->setFillOpacity(0.8);

        $linkText = current($iconsArray['icons'])['link'];
        $metrics = $image->queryFontMetrics($draw, $linkText);

        $linkMarginX = (self::$imageParams['imageWidth'] - $metrics['textWidth']) / 2; // отступ текста по X

        // наложение ссылки
        $draw->annotation($linkMarginX, $linkMarginTop, $linkText);
        $image->setImageFormat('png');
        $image->drawImage($draw);
    }

    public function drawTopText(\Imagick &$image, $iconsArray)
    {
        $textMarginTop = 66; // расположение ссылки по Y
        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->fontThin);
        $draw->setFontWeight(200);
        $draw->setFillColor(new \ImagickPixel(self::$imageParams['topFontColor']));
        $draw->setFontSize(self::$imageParams['topFontSize']);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true); // задали параметры шрифта для надписи
        //$draw->setFillOpacity(0.8);

        $nameForText = isset($iconsArray['canonical_name']) ? $iconsArray['canonical_name'] : ucfirst($iconsArray['name']);
        $text = trim($nameForText) . ' Icon';
        $metrics = $image->queryFontMetrics($draw, $text);

        $linkMarginX = (self::$imageParams['imageWidth'] - $metrics['textWidth']) / 2; // отступ текста по X

        // наложение ссылки
        $draw->annotation($linkMarginX, $textMarginTop, $text);
        $image->setImageFormat('png');
        $image->drawImage($draw);

    }

    public function recolorIcons($iconsAbsPathsArray)
    {
        $recoloredIcons = [];
        $recolorTool = new \icons8_image_tools\SvgRecolorTool();
        foreach($iconsAbsPathsArray as $platform => $iconAbsPath)
        {
            if($platform != \Platform::API_CODE_COLOR && $platform != \Platform::API_CODE_OFFICE)
            {
                $recolorTool->recolorImage($iconAbsPath, self::$imageParams['iconsColor']);
                $recoloredSvgAbsPath = $this->tempPath . $platform . '_' . $this->iconName . '.svg';
                $recolorTool->save($recoloredSvgAbsPath);
                $recoloredIcons[$platform] = $recoloredSvgAbsPath;

            }
            else
                $recoloredIcons[$platform] = $iconAbsPath;
        }
        return $recoloredIcons;
    }

    /**
     * Удаляет картинку для указанной иконки
     * @param $iconName
     * @return bool
     */
    public function remove($iconName)
    {
        $twitterPictureAbsPath = $this->storagePath . 'Share/twitter/' . $iconName{0} . '/';
        $twitterPictureFileAbsPath = $twitterPictureAbsPath . $iconName . '.png';
        $retValue = \FileSystemHelper::rm_rf($twitterPictureFileAbsPath);
        $this->log('Twitter picture for icon "' . $iconName . '" is removed from ' . $twitterPictureFileAbsPath);
        return $retValue;
    }
} 
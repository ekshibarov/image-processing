<?php
/**
 * Created by PhpStorm.
 * User: bleak
 * Date: 09.04.15
 * Time: 15:56
 */

namespace icons8_image_processing;

/**
 * Class ImageGenerator
 * @package icons8_image_processing
 */
abstract class ImageGenerator {

    public $config;

    public $tempPath;

    public $storagePath;

    public $debug = true;
    public $debugFiles = false;

    public function __construct()
    {
        $this->config = \Yii::app()->params;
        $this->storagePath = rtrim($this->config['storage_path'], '/') . '/';

        do // подбор папки для временных файлов
        {
            $this->tempPath = rtrim($this->config['tmp_path'], '/') . '/' . uniqid('svg') . '/';
        }
        while(is_dir($this->tempPath));
        \FileSystemHelper::mkdir($this->tempPath);

        register_shutdown_function(array($this, 'removeTempFiles'));
    }

    public function actionNotifyZipper()
    {
        file_put_contents($this->config['repack_semaphore_path'], date('c'));
        $this->log('Установлен семафор ' . $this->config['repack_semaphore_path'] . ' для перепаковки ZIP');
    }

    protected function checkSvg($svgFile)
    {
        if (!$svgFile)
        {
            echo 'Файл не указан', PHP_EOL;
            exit(-1); // Файл не указан
        }

        $storagePathLen = strlen($this->storagePath);
        if (0 == strcasecmp($this->storagePath, substr($svgFile, 0, $storagePathLen)))
        {
            $svgFile = substr($svgFile, $storagePathLen);
        } else
        {
            $_file = ltrim($svgFile, '/');
            $_fileAbs = realpath($this->storagePath.$_file);
            if (!is_file($_fileAbs))
            {
                echo 'Указанный файл "', $svgFile . '" размещён не в директории ' . $this->storagePath, PHP_EOL; // todo для юзерских файлов надо будет проверять какой-то другой путь
                exit(-2); // Путь не в хранилище
            }
            $svgFile = $_file;
        }

        if (false === strpos($svgFile, '/SVG/'))
        {
            echo 'Указанный файл "', $svgFile, '" размещён не в папке /SVG/', PHP_EOL;
            exit(-4); // Не папка с файлами SVG
        }

        return $svgFile;
    }


    public function removeTempFiles()
    {
        if ($this->debug && $this->debugFiles)
        {
            echo 'Папка "', $this->tempPath, '" сохранена для отладки', PHP_EOL;
            return;
        }
        \FileSystemHelper::rm_rf($this->tempPath);
    }

    protected function log($message, $level = \CLogger::LEVEL_INFO, $force = false)
    {
        if ($this->debug || $force)
            echo $message, PHP_EOL;
        \Yii::log($message, $level);
    }
} 
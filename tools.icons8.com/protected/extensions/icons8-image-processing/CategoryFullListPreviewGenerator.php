<?php

namespace icons8_image_processing;
use icons8_image_tools\SvgTool;

/**
 * Class CategoryFullListPreviewGenerator
 * @package icons8_image_processing
 * @property \icons8_image_tools\SvgTool $svgTool
 * @property \icons8_image_tools\SvgRecolorTool $recolorTool
 */
class CategoryFullListPreviewGenerator extends ImageGenerator
{

    public $categoryName;
    public $alias;

    private $recolorTool;
    private $svgTool;

    /* Путь к файлу шрифта относительно /protected */
    public $font = 'config/share-picture/helveticaneuecyr-roman.otf';
    public $fontThin = 'config/share-picture/helveticaneuecyr-thin.otf';
    public $fontTop = 'config/share-picture/Sanchezregular.otf';

    public static $imageParams = [
        'iconSize' => 50,              // размер иконок
        'topFontSize' => 48,            // размер шрифта верхней надписи
        'topFontColor' => '#37474F',    // цвет шрифта верхней надписи
        'bottomTextFontSize' => 20,         // размер шрифта нижней надписи
        'bottomTextMargin' => 40,         // размер шрифта нижней надписи
        'bottomLinkFontSize' => 20,         // размер шрифта нижней надписи
        'bottomLinkFontColor' => '#93BDEE',         // размер шрифта нижней надписи
        'backgroundColor' => '#ffffff', // цвет фона
        'iconFontSize' => 12,         // размер шрифта иконки
        'iconFontColor' => '#9b9b9b', // цвет фона
        'iconsColor' => '#3A3A3A',      // цвет иконок
        'imageHeight' => 495,           // высота всего превью
        'imageWidth' => 800,            // ширина всего превью
        'heapImageWidth' => 1150,            // ширина превью, когда иконки выводятся кучей
        'heapIconsRowCount' => 16,            // ширина превью, когда иконки выводятся кучей
        'iconLabelDistance' => 9,          // расстояние от ряда иконок до подписи
        'iconsRowDistance' => 20,          // расстояние между рядами иконок
        'iconsDistance' => 40,          // расстояние между иконками в ряду
        'marginTopText' => 107,         // отступ сверху для надписи
        'marginTopFirstRow' => 182,       // отступ сверху для первого ряда иконок
        'marginBottomLastRow' => 95,       // отступ снизу после последнего ряда иконок
    ];

    public static $platformsOrder = [   // порядок, в котором нужно выстроить иконки платформ
        \Platform::API_CODE_IOS7 => '',
        \Platform::API_CODE_IOS7_FILLED => '',
        \Platform::API_CODE_WINDOWS10 => '',
        \Platform::API_CODE_WINDOWS8 => '',
        \Platform::API_CODE_ANDROID_L => '',
        \Platform::API_CODE_ANDROID => '',
        \Platform::API_CODE_COLOR => '',
        \Platform::API_CODE_OFFICE => ''
    ];

    function __construct()
    {
        if (!class_exists('\Imagick', false))
            throw new \Exception('Class \Imagick not found');
        parent::__construct();
    }

    /**
     * @param array $iconsArray
     * @return string|false $categoryPictureFileAbsPath
    {
    common_icons: [
    icons:[
    {platform:ios,svg:<file>},
    {platform:ios,svg:<file>},
    {platform:ios,svg:<file>}
    ],
    name:<название иконки>
    ],
    category_name:<название бандла>
    link:<ссылка на бандл>

    }
     */
    public function export($iconsArray)
    {
        $this->recolorTool = new \icons8_image_tools\SvgRecolorTool();
        $this->svgTool = $svgTool = new \icons8_image_tools\SvgTool(array(
            'verbose' => $this->config['verbose'],
            'background-color' => self::$imageParams['backgroundColor'],
            'transparent' => true,
        ), $this->config['binaries']);

        $this->log('- drawing preview for ' .  $iconsArray['category_name'] . ' category -');
        $this->tempPath = $this->tempPath . 'category_full/';
        \FileSystemHelper::mkdir($this->tempPath);

        $categoryPictureFileAbsPath = false;
        $this->categoryName = $iconsArray['category_name'];
        $this->alias = $iconsArray['category_alias'];

        $commonIconsAbsPathArray = $this->reorderIcons($iconsArray['common_icons']);

        $counter = 0;
        foreach($commonIconsAbsPathArray as &$commonIcon)
        {
            $prepared = [];
            foreach($commonIcon['icons'] as $platform => $iconAbsPath)
            {
                $counter++;

                $pathInfo = pathinfo($iconAbsPath);
                $tmpIconPath = $this->tempPath . $this->alias . '_' . $pathInfo['filename'] . '.svg';
                \FileSystemHelper::cp($iconAbsPath, $tmpIconPath);

                $recoloredIcon = ($platform != \Platform::API_CODE_COLOR && $platform != \Platform::API_CODE_OFFICE) ?
                    $this->recolorIcon($iconAbsPath, $platform . '_' . $counter) :
                    $tmpIconPath;

                $prepared[$platform] = $svgTool->resizeSVG($recoloredIcon, $recoloredIcon, self::$imageParams['iconSize']);
            }
            $commonIcon['prepared'] = $prepared;
        }
        $iconsArray['common_icons'] = $commonIconsAbsPathArray;
        $image = $this->drawImage($iconsArray);
        if ($image)
        {
            $categoryPictureAbsPath = $this->storagePath . 'Share/category_icons/' . $this->alias{0} . '/';
            $categoryPictureFileAbsPath = $categoryPictureAbsPath . $this->alias . '.png';

            \FileSystemHelper::mkdir($categoryPictureAbsPath);
            file_put_contents($categoryPictureFileAbsPath, $image);

            $pngTool = new \icons8_image_tools\PngTool(array(
                'verbose' => $this->config['verbose'],
                'strategy' => $this->config['strategy'],
                'multipass' => $this->config['multipass'],
                'transparent' => false,
            ), $this->config['binaries']);
            //$pngTool->optimize(array($categoryPictureFileAbsPath));

            $this->log('Preview picture for category "' . $this->categoryName . '" exported to ' . $categoryPictureFileAbsPath);

        } else
        {
            $this->log('Preview picture for category "' . $this->categoryName . '" not created');
        }
        return $categoryPictureFileAbsPath;
    }

    protected function exportPNG($recoloredSvg, $name)
    {
        $tempPngPath = $this->tempPath . 'PNG/';

        $size = self::$imageParams['iconSize'];
        $pngAbsPath = $tempPngPath . $this->alias . '_' . $name . '.png';
        if ($this->svgTool->exportPng($recoloredSvg, $pngAbsPath, $size))
        {
            $this->log(' ... export to PNG ' . $this->alias . '_' . $name . '.png success');
        }
        return $pngAbsPath;
    }

    protected function reorderIcons($commonIconsArray)
    {
        usort($commonIconsArray, function ($a, $b) {
            if (count($a['icons']) == count($b['icons'])) return 0;
            return (count($a['icons']) > count($b['icons'])) ? -1 : 1;
        });

        foreach($commonIconsArray as &$commonIcon)
        {
            $icons = self::$platformsOrder;
            foreach($commonIcon['icons'] as $platform => $iconRelPath)
            {
                $iconsAbsPath = rtrim($this->storagePath, '/') . '/' . $iconRelPath;
                if (is_file($iconsAbsPath))
                    $icons[$platform] = $iconsAbsPath;
            }
            $commonIcon['icons'] = array_filter($icons);
        }

        return $commonIconsArray;
    }

    protected function drawImage($iconsArray)
    {
        $image = new \Imagick();
        $image->setResourceLimit(6, 1); // 6 means THREAD_LIMIT (unfortunately, there is no such constant defined, so u have to use this integer value);
        $iconRowHeight = self::$imageParams['iconSize'] + self::$imageParams['iconFontSize'] +
            self::$imageParams['iconLabelDistance'] + self::$imageParams['iconsRowDistance'];
        $imageHeight = (count($iconsArray['common_icons']) * $iconRowHeight) + self::$imageParams['marginTopFirstRow'] +
            self::$imageParams['marginBottomLastRow'];
        $image->newimage(self::$imageParams['imageWidth'],$imageHeight,
            new \ImagickPixel(self::$imageParams['backgroundColor']) // рисуем белое поле
        );
        $image->setImageFormat('png');

        $rowsCounter = 0;
        foreach($iconsArray['common_icons'] as $commonIconArray)
        {
            $iconsCounter = 0;
            $marginTop = self::$imageParams['marginTopFirstRow'] + ($rowsCounter * $iconRowHeight);
            $count = count($commonIconArray['icons']);

            foreach($commonIconArray['prepared'] as $platform => $pngAbsPath)
            {
                $marginLeft = ((self::$imageParams['imageWidth'] -
                            (($count * (self::$imageParams['iconSize'])) +
                                (($count - 1) * self::$imageParams['iconsDistance']))) / 2) +
                    $iconsCounter * (self::$imageParams['iconSize'] + self::$imageParams['iconsDistance']);
                $image->compositeImage(new \Imagick($pngAbsPath), \Imagick::COMPOSITE_DEFAULT, $marginLeft, $marginTop);

                $iconsCounter++;
            }
            $this->drawIconText($image, $commonIconArray['name'], $marginTop);
            $rowsCounter++;
        }

        $this->drawTopText($image, $iconsArray['category_name']);
        $this->drawBottomLeftText($image, $iconsArray['category_name']);
        $this->drawBottomLink($image, $iconsArray['link']);
        return $image;
    }

    protected function drawHeapImage($iconsArray, $count)
    {
        $image = new \Imagick();
        $image->setResourceLimit(6, 1); // 6 means THREAD_LIMIT (unfortunately, there is no such constant defined, so u have to use this integer value);
        $iconRowHeight = self::$imageParams['iconSize'] + self::$imageParams['iconsRowDistance'];
        $imageHeight = (ceil($count / self::$imageParams['heapIconsRowCount']) * $iconRowHeight) + self::$imageParams['marginTopFirstRow'] +
            self::$imageParams['marginBottomLastRow'];
        $image->newimage(self::$imageParams['heapImageWidth'],$imageHeight,
            new \ImagickPixel(self::$imageParams['backgroundColor']) // рисуем белое поле
        );
        $image->setImageFormat('png');

        $rowsCounter = 0;
        foreach($iconsArray['common_icons'] as $commonIconArray)
        {
            $iconsCounter = 0;
            $marginTop = self::$imageParams['marginTopFirstRow'] + ($rowsCounter * $iconRowHeight);
            $count = count($commonIconArray['icons']);

            foreach($commonIconArray['prepared'] as $platform => $pngAbsPath)
            {
                $marginLeft = ((self::$imageParams['imageWidth'] -
                            (($count * (self::$imageParams['iconSize'])) +
                                (($count - 1) * self::$imageParams['iconsDistance']))) / 2) +
                    $iconsCounter * (self::$imageParams['iconSize'] + self::$imageParams['iconsDistance']);
                $image->compositeImage(new \Imagick($pngAbsPath), \Imagick::COMPOSITE_DEFAULT, $marginLeft, $marginTop);

                $iconsCounter++;
            }
            $this->drawIconText($image, $commonIconArray['name'], $marginTop);
            $rowsCounter++;
        }

        $this->drawTopText($image, $iconsArray['category_name']);
        $this->drawBottomLeftText($image, $iconsArray['category_name']);
        $this->drawBottomLink($image, $iconsArray['link']);
        return $image;
    }

    public function drawBottomLeftText(\Imagick &$image, $name)
    {
        $linkMarginTop = $image->getImageHeight() - self::$imageParams['bottomTextMargin']; // расположение ссылки по Y
        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->font);
        $draw->setFontWeight(200);
        $draw->setFillColor(new \ImagickPixel('black'));
        $draw->setFontSize(self::$imageParams['bottomTextFontSize']);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true); // задали параметры шрифта для надписи
        $draw->setFillOpacity(0.7);

        $linkMarginX = 40; // отступ текста по X

        // наложение ссылки
        $draw->annotation($linkMarginX, $linkMarginTop, $name . ' Icons');
        $image->setImageFormat('png');
        $image->drawImage($draw);
    }

    public function drawBottomLink(\Imagick &$image, $link)
    {
        $linkMarginTop = $image->getImageHeight() - self::$imageParams['bottomTextMargin']; // расположение ссылки по Y
        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->font);
        $draw->setFontWeight(200);
        $draw->setFillColor(new \ImagickPixel(self::$imageParams['bottomLinkFontColor']));
        $draw->setFontSize(self::$imageParams['bottomTextFontSize']);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true); // задали параметры шрифта для надписи

        $metrics = $image->queryFontMetrics($draw, $link);

        $linkMarginX = (self::$imageParams['imageWidth'] - $metrics['textWidth']) - 40; // отступ текста по X

        // наложение ссылки
        $draw->annotation($linkMarginX, $linkMarginTop, $link);
        $image->setImageFormat('png');
        $image->drawImage($draw);
    }

    public function drawIconText(\Imagick &$image, $text, $marginTop)
    {
        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->font);
        $draw->setFontWeight(200);
        $draw->setFillColor(new \ImagickPixel(self::$imageParams['iconFontColor']));
        $draw->setFontSize(self::$imageParams['iconFontSize']);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true); // задали параметры шрифта для надписи
        $draw->setFillOpacity(0.8);

        $metrics = $image->queryFontMetrics($draw, $text);

        $marginLeft = (self::$imageParams['imageWidth'] - $metrics['textWidth']) / 2; // отступ текста по X

        // наложение ссылки
        $draw->annotation($marginLeft, $marginTop + self::$imageParams['iconLabelDistance'] +
            self::$imageParams['iconSize'] +
            self::$imageParams['iconFontSize'], $text);
        $image->drawImage($draw);
    }

    public function drawTopText(\Imagick &$image, $name)
    {
        $textMarginTop = self::$imageParams['marginTopText']; // расположение ссылки по Y
        $draw = new \ImagickDraw();
        $draw->setFont(\Yii::app()->basePath . '/' . $this->fontTop);
        $draw->setFontWeight(200);
        $draw->setFillColor(new \ImagickPixel(self::$imageParams['topFontColor']));
        $draw->setFontSize(self::$imageParams['topFontSize']);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true); // задали параметры шрифта для надписи
        //$draw->setFillOpacity(0.8);

        $metrics = $image->queryFontMetrics($draw, $name);

        $linkMarginX = (self::$imageParams['imageWidth'] - $metrics['textWidth']) / 2; // отступ текста по X

        // наложение ссылки
        $draw->annotation($linkMarginX, $textMarginTop, $name);
        $image->drawImage($draw);

    }

    public function recolorIcon($svgAbsPath, $name)
    {
        $this->recolorTool->recolorImage($svgAbsPath, self::$imageParams['iconsColor']);
        $recoloredSvgAbsPath = $this->tempPath . $this->alias . '_' . $name . '.svg';
        $this->recolorTool->save($recoloredSvgAbsPath);

        return $recoloredSvgAbsPath;
    }

    /**
     * Удаляет картинку для указанной иконки
     * @param $iconName
     * @return bool
     */
    public function remove($iconName)
    {
        $categoryPictureAbsPath = $this->storagePath . 'Share/free_icons/' . $iconName{0} . '/';
        $categoryPictureFileAbsPath = $categoryPictureAbsPath . $iconName . '.png';
        $retValue = \FileSystemHelper::rm_rf($categoryPictureFileAbsPath);
        $this->log('Twitter picture for icon "' . $iconName . '" is removed from ' . $categoryPictureFileAbsPath);
        return $retValue;
    }
} 
<?php

/**
 * Logger that can handle PHP fatal error even if OUT OF MEMORY error occurs
 */
class WebLogRoute extends CWebLogRoute {

	private $outOfMemoryReserve;

	public function init() {
		parent::init();
		$this->outOfMemoryReserve = str_repeat('x', 1024 * 200); // 200K of memory reserve for OUT OF MEMORY fatal error
		register_shutdown_function(array($this, 'onShutdown'));
	}

	public function onShutdown() {
		// release 200K of memory to make possible to collect extra log messages or to send notice by email
		unset($this->outOfMemoryReserve);

		$error = error_get_last();
		if (E_ERROR == $error['type']) { // PHP Fatal Error
			$msg = 'FATAL ERROR : ' . date("Y-m-d H:i:s (T)") . " \"{$error['message']}\" in file {$error['file']} at line {$error['line']}";
			Yii::log($msg, CLogger::LEVEL_ERROR);
		}
	}
}
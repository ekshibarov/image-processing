<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class AMQPComponent
 * @property string $host
 * @property integer $port
 * @property string $user
 * @property string $password
 * @property string $virtualHost
 */
class AMQPComponent
{
  public function __construct()
  {
    $config = Yii::app()->params;
    $this->host           = $config['queue']['host'];
    $this->port           = $config['queue']['port'];
    $this->user           = $config['queue']['user'];
    $this->password       = $config['queue']['password'];
    $this->virtualHost    = $config['queue']['virtualHost'];
    $this->queuesEnabled  = $config['queue']['queuesEnabled'];
  }

  /**
   * @param string $msg
   * @param string $queue
   * @param string $exchange
   */
  public function sendMessage($msg, $queue, $exchange)
  {
    $connection = new AMQPStreamConnection(
        $this->host,
        $this->port,
        $this->user,
        $this->password,
        $this->virtualHost
    );
    $channel = $connection->channel();
    /*
        The following code is the same both in the consumer and the producer.
        In this way we are sure we always have a queue to consume from and an
            exchange where to publish messages.
    */
    /*
        name: $queue
        passive: false
        durable: true // the queue will survive server restarts
        exclusive: false // the queue can be accessed in other channels
        auto_delete: false //the queue won't be deleted once the channel is closed.
    */
    $channel->queue_declare($queue, false, true, false, false);
    /*
        name: $exchange
        type: direct
        passive: false
        durable: true // the exchange will survive server restarts
        auto_delete: false //the exchange won't be deleted once the channel is closed.
    */
    $channel->exchange_declare($exchange, 'direct', false, true, false);
    $channel->queue_bind($queue, $exchange);
    $messageBody = $msg;
    $message = new AMQPMessage(
        $messageBody,
        [
            'content_type' => 'text/plain',
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ]
    );
    $channel->basic_publish($message, $exchange);
    $channel->close();
    $connection->close();
  }

  /**
   * Write message to a queues logfile
   * @param  string $message
   * @param  string $level
   */
  protected function queueLog($message, $level = CLogger::LEVEL_INFO)
  {
    $logMessage = date('Y-m-d H:i:s') . ' ' . $message;
    if (isset($this->context)) {
      $logMessage .= ' (context: ' . $this->context . ')';
    }
    $logMessage .= PHP_EOL;
    Yii::log($logMessage, $level, 'queue');
  }


  /**
   * Message with eps, encoded by base64
   * @param  string $message Base64 encoded JSON with eps
   * @return boolean
   */
  public function sendReceiveEpsTask($message)
  {
  if ($this->queuesEnabled) {
      $queue = 'receive_eps';
      $this->sendMessage($message, $queue, 'router');
      return true;
    } else {
      $this->queueLog('Queues not enabled', CLogger::LEVEL_WARNING);
      return false;
    }
  }
}

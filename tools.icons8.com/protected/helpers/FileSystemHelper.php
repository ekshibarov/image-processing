<?php
/**
 * Некоторые функции, которых не хватает при работе с файловой системой
 * Class FileSystemHelper
 */
class FileSystemHelper {

	const
		/** Права по умолчанию для папок, создаваемых через FileSystemHelper::mkdir */
		DEFAULT_MKDIR_RIGHTS = 0775
	;

	/**
	 * Создаёт директорию рекурсивно и назначает права
	 * Аналог mkdir -p '/abs/path/to/dir' && chmode -R '/abs/path/to/dir'
	 * Указание путей через запятую и другие фокусы командной строки не поддерживает
	 *
	 * @param string $path
	 * @param int $rights
	 * @return bool false если не создалась
	 */
	public static function mkdir($path, $rights = self::DEFAULT_MKDIR_RIGHTS) {
		#TODO безопасность - чтобы работала только в пределах разрешённого списка директорий
		if (!is_dir($path))
		{
			$oldMask = umask(0);
			$result = @mkdir($path, $rights, true);
			umask($oldMask);
			return $result;
		}
		return true;
	}

	/**
	 * Удаляет файл или директорию рекурсивно, то есть полностью
	 * Аналог rm -rf '/abs/path/to/dir'
	 * Файловые маски не поддерживает - нужен абсолютный путь
	 *
	 * @param $path
     * @return bool
	 */
	public static function rm_rf($path) {
		#TODO безопасность - чтобы работала только в пределах разрешённого списка директорий
		if (is_dir($path))
		{
			foreach (scandir($path) as $object)
			{
				if ($object == "." || $object == "..")
				{
					continue;
				}
				$object = $path.DIRECTORY_SEPARATOR.$object;
				if (is_dir($object))
				{
					if (!self::rm_rf($object))
						return false;
				} else
				{
					if (!@unlink($object))
						return false;
				}
			}
			if (!@rmdir($path))
				return false;
		} elseif (is_file($path))
		{
			if (!@unlink($path))
				return false;
		}
		return true;
	}

	/**
	 * Переименовывает файл или директорию
	 * Аналог mv '/path/one' '/path/another'
	 * Файловые маски не поддерживает - нужен абсолютный путь
	 *
	 * @param $pathFrom
	 * @param $pathTo
     * @return bool
	 */
	public static function mv($pathFrom, $pathTo) {
		#TODO безопасность - чтобы работала только в пределах разрешённого списка директорий
		if (!file_exists($pathFrom))
		{
			return false;
		}
		if ($pathFrom === $pathTo)
			return true;

		return self::rm_rf($pathTo) && @rename($pathFrom, $pathTo);
	}

		/**
	 * Копирует файл
	 * Аналог cp '/path/one.ext' '/path/another.ext'
	 * Файловые маски не поддерживает - нужен абсолютный путь
	 *
	 * @param $pathFrom
	 * @param $pathTo
	 * @param $rights
     * @return bool
	 */
	public static function cp($pathFrom, $pathTo, $rights = false) {
		#TODO безопасность - чтобы работала только в пределах разрешённого списка директорий
		if (!file_exists($pathFrom))
			return false;

		if (self::rm_rf($pathTo) && @copy($pathFrom, $pathTo))
		{
			if (false !== $rights)
				return @chmod($pathTo, $rights);
			else
				return true;
		}
		return false;
	}

	/**
	 * Удаляет все пустые поддиректории
	 * То есть те, в которых нет файлов
	 *
	 * @param $path
	 * @return bool
	 */
	public static function rm_empty($path) {
		$empty = true;
		foreach (scandir($path) as $object) {
			if ('.' == $object || '..' == $object) {
				continue;
			}
			$objectPath = "{$path}/{$object}";
			if (is_file($objectPath))
			{
				$empty = false;
			} elseif (!self::rm_empty($objectPath))
			{
				$empty = false;
			}
		}
		if ($empty)
		{
			echo 'Remove empty folder "', $path, '"', PHP_EOL;
			rmdir($path);
		}
		return $empty;
	}

    /**
     *
     * @param string $path - путь к директории, в которой выполняем поиск.
     * @param string $fileName - имя файла, можно с подстановочными символами ('apple-*.png')
     * @param bool $caseInsensitive - регистронезависимо? (apple = aPPle)
     * @return array
     */
    public static function find($fileName, $path = '.', $caseInsensitive = false)
    {
        $result = array();

        $findCommand = 'find ';
        $findCommand .= rtrim($path, '/') . '/';
        $findCommand .= ($caseInsensitive) ? ' -iname' : ' -name';
        $findCommand .= " '{$fileName}' 2>/dev/null";

        exec($findCommand, $result);
        return $result;
    }
}
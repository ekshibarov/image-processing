<?php

/**
 * Created by PhpStorm.
 * User: bleak
 * Date: 01.09.2015
 * Time: 21:36
 */
class ToolsIconHelper
{
    const
        FILE_EPS = 'eps',

        FILE_PDF = 'pdf',

        FILE_SVG_EDITABLE = 'svg',

        FILE_SVG_SIMPLIFIED = 'svg.simp',

        FILE_SVG_PLAIN = 'svg.plain',

        FILE_SVG_CROSSED = 'svg.crossed',

        FILE_PNG = 'png',

        FILE_TWITTER = 'twitter',

        FILE_GOOGLE = 'google',

        FILE_SOCIAL = 'social',

        FILE_SVG_SIZES = 'svg_sizes';

    const 	UPLOAD_SUCCESS = 0,
            UPLOAD_IN_PROGRESS = 1,
            UPLOAD_PARTIALLY_UPLOADED = 2,
            UPLOAD_SIMPLIFICATION_FAIL = 3,
            UPLOAD_FAIL = 4;

    public $storagePath;

    public $debug = false;

    public static function addFile(&$filesArray, $filePath, $type, $size = 0, $plain = false)
    {
        $storagePath = rtrim(\Yii::app()->params['storage_path'], '/') . '/';
        $storagePathLen = strlen($storagePath);

        if($plain && is_file($filePath) && stripos($filePath, 'tmp/') !== false) // plain ���� ����� ���������� � tmp
        {
            $file =  file_get_contents($filePath);
        }
        else
        {

            if (0 == strcasecmp($storagePath, substr($filePath, 0, $storagePathLen)))
            {
                $filePath = substr($filePath, $storagePathLen);
            } else
            {
                $_file = ltrim($filePath, '/');
                $_fileAbs = realpath($storagePath.$_file);
                if (!is_file($_fileAbs)) {
                    throw new Exception('File in wrong directory: ' . $storagePath.$_file . ' ' . realpath($storagePath.$_file));
                }
                $filePath = $_file;
            }
            $file = ($plain) ? file_get_contents(realpath($storagePath.ltrim($filePath, '/'))) : $filePath;
        }

        if($type == self::FILE_PNG)
        {
            if($size)
                $filesArray[$type][$size] = $file;
            else {
                throw new Exception('PNG without size');
            }
        }
        elseif($type == self::FILE_SVG_SIZES)
        {
            if($size)
                if($plain)
                    $filesArray[$type][$size][self::FILE_SVG_PLAIN] = $file;
                else
                    $filesArray[$type][$size][self::FILE_SVG_EDITABLE] = $file;
            else {
                throw new Exception('office icon without size');
            }
        }
        else
            $filesArray[$type] = $file;

    }

}
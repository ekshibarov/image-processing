# -- tools.icon8.com

# index files, fix wrong and, remove obsolete, draw new, redraw changed images
# просканировать файлы, исправить ошибки, удалить устаревшие, нарисовать новые, перерисовать изменившиеся иконки
# выключаем. теперь синхронизация только из апи 40 04,10,16,22 * * * cd <?php echo APP_PATH ?>/console ; bash sync.sh

# упаковать файлы иконок в ZIP
33 */3 * * * cd <?php echo APP_PATH ?>/console ; bash zip_icons8.sh > /dev/null

# упаковать файлы иконок для отдельного большого архива
22 8 * * * cd <?php echo APP_PATH ?>/console ; bash ./generatezipshared/zip_icons8_extended_all.sh > /dev/null

# -- /tools.icon8.com
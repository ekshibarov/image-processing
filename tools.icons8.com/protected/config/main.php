<?php

if(!ini_get('date.timezone')){
	date_default_timezone_set('GMT');
}

// подключение автолоадера композера
if(file_exists(APP_PATH . '/protected/vendor/autoload.php')) {
	include_once(APP_PATH . '/protected/vendor/autoload.php');
}

function params_local() {
	if (file_exists(dirname(__FILE__) . '/params.local.php')) {
		return require(dirname(__FILE__) . '/params.local.php');
	} else {
		return array();
	}
}

$params = CMap::mergeArray(
	require(dirname(__FILE__) . '/params.php'),
	params_local()
);

Yii::$classMap = require_once(dirname(__FILE__) . '/classmap/classmap.php');

Yii::setPathOfAlias('icons8_image_tools', realpath(__DIR__ . '/../extensions/icons8-image-tools'));
Yii::setPathOfAlias('icons8_storage_tools', realpath(__DIR__ . '/../extensions/icons8-storage-tools'));
Yii::setPathOfAlias('icons8_image_processing', realpath(__DIR__ . '/../extensions/icons8-image-processing'));
Yii::setPathOfAlias('icons8_reports', realpath(__DIR__ . '/../extensions/icons8-reports'));
Yii::setPathOfAlias('http_requests', realpath(__DIR__ . '/../extensions/http-requests'));

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath' => realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'),
	'runtimePath' => $params['runtime_path'],
	'name' => $params['title'],
	'sourceLanguage' => 'en',
	'language' => $params['language'],

	// preloading 'log' component
	'preload' => array('log'),

	// import classes into autoload
	'import' => array(
		'application.components.*',
		'application.helpers.ToolsIconHelper',
		//'application.models.*',
		//'ext.icons8-image-tools.*',
		//'ext.icons8-image-processing.*',
		//'ext.icons8-storage-tools.*',
	),

	// application components
	'components' => array(
		// AMQPComponent for RabbitMQ
		'amqp' => array(
			'class' => 'application.components.AMQPComponent',
			'host' => 'localhost',
			'port' => 5672,
			'user' => 'guest',
			'password' => 'guest',
			'queuesEnabled' => true,
		),
		// application logs
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				// write log messages into file
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning', // , info, trace
					'enabled' => true,
					'except' => array( // don't log this categories
						'exception.CHttpException.404', // 404 Page Not Found
						'queue.*', // 404 Page Not Found
						'tools.simplification_fail' // сообщения в кириллице на почту дизайнерам и нам. все равно нечитабельны в файле
					),
				),
				'amqp_task' => array(
					'class' => 'CFileLogRoute',
					'logFile' => 'amqp_task.log',
					'levels' => 'error, warning, trace, info',
					'enabled' => true,
					'categories' => 'amqp_task.*'
				),
				'queue' => array(
					'class' => 'CFileLogRoute',
					'logFile' => 'queue.log',
					'levels' => 'error, warning, trace, info',
					'enabled' => true,
					'categories' => 'queue.*'
				),

				// show log messages on web pages
				array(
					'class' => 'application.components.WebLogRoute',
					'levels' => 'error, warning', //, info, profile, trace',
					//'showInFireBug' => true,
					//'categories'=>'system.*',
					'enabled' => YII_DEBUG,
				),

				// receive log messages by email
				array(
					'class' => 'CEmailLogRoute',
					'levels' => 'error, warning',
					'emails' => $params['adminEmail'],
					'enabled' => !empty($params['adminEmail']),
					'except' => array( // don't log this categories
						'exception.CHttpException.404', // 404 Page Not Found
						'tools.*',
						'queue_task.*',
						'images_generator.*',
						'queue.*',
						'tools.simplification_fail.*',
					),
				),
				array(
					'class' => 'CEmailLogRoute',
					'levels' => 'error, warning',
					'emails' => $params['adminEmail'] . (isset($params['designersEmail']) ? (',' . $params['designersEmail']) : ''),
					'enabled' => !empty($params['adminEmail']) || !empty($params['designersEmail']),
					'categories' => 'tools.simplification_fail',
					'except' => array( // don't log this categories
						'exception.CHttpException.404',
						'queue_task.*',
						'images_generator.*',
						'queue.*',
					),
				),
				array(
					'class' => 'application.extensions.log.CachedEmailToolsLogRoute',
					'subject' => 'Queue Log / ' . $params['title'],
					'levels' => 'error, warning',
					'emails' => $params['adminEmail'],
					'enabled' => true,
					'categories' => ['queue_task.*', 'images_generator.*', 'queue.*'],
				),
			),
		),
		'cache' => (extension_loaded('memcached') || extension_loaded('memcache'))
			? array(
				'class' => 'CMemCache',
				'useMemcached' => (bool)extension_loaded('memcached'),
				'servers' => array(
					array('host'=>'localhost', 'port'=>11211),
				),
			)
			: array(
				'class' => 'CFileCache',
				'directoryLevel' => 3,
				##'embedExpiry' => true,
			),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => $params,
);

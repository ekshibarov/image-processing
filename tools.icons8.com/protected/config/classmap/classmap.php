<?php

return array(
	// 'application.components.*',
	'ConsoleUser' => APP_PATH .'/protected/components/ConsoleUser.php',
	'WebLogRoute' => APP_PATH .'/protected/components/WebLogRoute.php',

	// 'application.helpers.*',
	'FileSystemHelper' => APP_PATH .'/protected/helpers/FileSystemHelper.php',

	'Platform' => APP_PATH . '/protected/models/Platform.php',
);
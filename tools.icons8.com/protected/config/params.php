<?php

// application parameters
return array(
	// this is displayed in the header section
	'title' => 'Icons8 tools',

	// this is used in error pages
	'adminEmail' => '',

	// На каком языке выдавать страницы документации и страницы тестов
	'language' => 'en',

	// URL сервиса
	'app_url' => 'http://tools.local-icons8.com',

	// URL api
	'api_url' => 'http://api.local-icons8.com',

	// URL основного сайта
	'main_app_url' => 'http://local-icons8.com',

	// Путь к папке с временными файлами и файловым кэшем
	'runtime_path' => APP_PATH . '/protected/runtime',

	// путь к хранилищу файлов
	'storage_path' => '/home/icons8/shared-icons/',

	// путь к папке с временными файлами
	'tmp_path' => '/tmp/changer',

	// путь к папке с резервными копиями удаляемых файлов
	'backup_path' => '/home/icons8/backup-icons/',

    /** Путь к файлу-семафору для установки признака "Требуется сборка архивов с файлами иконок" */
	'repack_semaphore_path' => '/tmp/refresh-zip',

	/** размер изображения для Twitter */
	'twitter_icon_size' => 100,

	// размеры иконок разного типа по разным платформам
	'platforms' => array(
		'Android' => array(
			'viewBox' => '24',
			'eps' => '24',
			'pdf' => true,
			'png' => array(24, 36, 48, 96, 32, 64, 128, 256, 512),
			'not-transparent-png' => array(256, 512),
			'svg.simplified' => true,
			'svg.crossed' => true,
		),
		'Android_L' => array(
            'viewBox' => '24',
			'eps' => '24',
			'png' => array(24, 36, 48, 96, 32, 64, 128, 256, 512),
			'not-transparent-png' => array(256, 512),
			'pdf' => true,
			'svg.simplified' => true,
			'svg.crossed' => true,
		),
		'iOS7' => array(
            'viewBox' => '50',
			'eps' => '50',
			'png' => array(25, 50, 75, 100, 32, 128, 256, 512),
			'not-transparent-png' => array(256, 512),
			'pdf' => true,
			'svg.simplified' => true,
			'svg.crossed' => true,
		),
		'windows8' => array(
            'viewBox' => '26',
			'eps' => '26',
			'pdf' => true,
			'png' => array(26, 32, 48, 64, 104, 128, 256, 512),
			'not-transparent-png' => array(256, 512),
			'svg.simplified' => true,
			'svg.crossed' => true,
		),
		'windows10' => array(
            'viewBox' => '32',
			'eps' => '32',
			'pdf' => true,
			'png' => array(16, 32, 48, 64, 96, 128, 256, 512),
			'not-transparent-png' => array(256, 512),
			'svg.simplified' => true,
			'svg.crossed' => false,
		),
		'MacOS' => array(
            'viewBox' => '36',
			'eps' => false, //'36',
			'pdf' => true,
			'png' => array(16, 18, 32, 36, 48, 64, 96, 128, 256, 512),
			'not-transparent-png' => array(256, 512),
			'svg.simplified' => false,
			'svg.crossed' => false,
		),
		'Responsive' => array(
            'viewBox' => '36',
			'eps' => false, // '36',
			'png' => false, // array(),
			'not-transparent-png' => array(256, 512),
			'pdf' => true,
			'svg.simplified' => false,
		),
		'Color' => array(
            'viewBox' => '48',
			'eps' => '48',
			'png' => array(24, 48, 96, 144, 192, 256, 512),
			'not-transparent-png' => array(),
			'pdf' => true,
			'svg.simplified' => false,
			'svg.crossed' => false,
		),
		'office' => array(
            'viewBox' => array(16, 30, 'main' => 40, 80),
            'viewBoxToPng' => array(
				16 => [16],
				24 => [80, 128, 160, 256, 512],
				30 => [30, 60],
				40 => [40, 120],
				80 => [80, 128, 160, 256, 512]
			),
			'eps' => '48',
			'png' => array(16, 30, 40, 60, 80, 120, 128, 160, 256, 512),
			'not-transparent-png' => array(),
			'pdf' => true,
			'svg.simplified' => false,
			'svg.crossed' => false,
		),
	),

	// стратегия сжатия PNG
	'strategy' => 'brute-force', // 'brute-force' / 'compatible'
	'multipass' => false, // true/false

	// пороговое значение метрики различий файлов SVG
	'compare_svg_metric_threshold' => 10,

	// пути ко всем нужным бинарникам
	'binaries' => array(
		'inkscape' => '/usr/bin/inkscape',
		'python' => '/usr/bin/python',
		'imgo' => 'bash ' . APP_PATH . '/imgo/imgo',
		'java' => '/usr/bin/java',
		'apache_fop' => 'bash ' . APP_PATH . '/apache-fop/fop.sh',
		'ps2eps' => '/usr/bin/ps2eps',
		'eps2eps' => '/usr/bin/eps2eps',
		'convert' => '/usr/bin/convert',
		'compare' => '/usr/bin/compare',
		'svgo' => '/usr/local/bin/svgo',
		'xvfb-run' => '/usr/bin/xvfb-run',
		'cairosvg' => '/usr/bin/cairosvg'
	),

	// карта классов - прямые пути к файлам с определениями классов
	// @see /api.icons8.com/protected/config/main.php line 20
	'classMap' => array(),
);

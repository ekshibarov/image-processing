#!/usr/bin/env bash

echo "... Stop amqp consumer"
cd ../amqp-consumer
sudo pm2 stop amqp-consumer-daemon.js

echo "... Delete 'test_message' queue"
sudo rabbitmqadmin delete queue name="test_message"
sudo rabbitmqadmin delete queue name="draw_eps"

echo "... Start amqp consumer"
sudo pm2 start amqp-consumer-daemon.js
